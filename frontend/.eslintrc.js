module.exports = {
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: "module",
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"],
  },
  env: {
    es6: true,
    browser: true,
  },
  plugins: ["@typescript-eslint"],
  ignorePatterns: ["src/types/*", "**/*spec.ts"],
  rules: {
    quotes: ["error", "double"],
    "@typescript-eslint/unbound-method": "off",
  },
};
