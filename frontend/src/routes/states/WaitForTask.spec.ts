import { render } from "@testing-library/svelte";
import { expect } from "chai";
import WaitForTask from "./WaitForTask.svelte";

describe("WaitForTask", () => {
  it("should render page", async () => {
    const { queryByText } = render(WaitForTask, {
      props: {
        leaveGame: () => {},
      },
    });

    expect(queryByText("Please wait for next steps...")).to.exist;
  });
});
