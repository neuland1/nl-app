import { render } from "@testing-library/svelte";
import { expect } from "chai";
import CombinationComp from "./Combination.svelte";
import type { Combination } from "../../types/proto/game";
import { Card_Color } from "../../types/proto/card";

const dummyCard = { color: Card_Color.COLOR_CLOVERS, value: 1 };

describe("Combinations", () => {
  const renderAndAssert = (combination: Combination, expectedText: string) => {
    const { queryByText } = render(CombinationComp, {
      props: {
        combination,
      },
    });

    expect(queryByText(expectedText)).to.exist;
  };

  it("should render high card", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "highCard",
          highCard: {
            kickers: [dummyCard, dummyCard, dummyCard, dummyCard],
            card: dummyCard,
          },
        },
      },
      "High Card:"
    );
  });

  it("should render pair", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "pair",
          pair: {
            cards: [dummyCard, dummyCard],
            kickers: [dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Pair:"
    );
  });

  it("should render three of a kind", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "threeKind",
          threeKind: {
            cards: [dummyCard, dummyCard, dummyCard],
            kickers: [dummyCard, dummyCard],
          },
        },
      },
      "Three of a Kind:"
    );
  });

  it("should render two pair", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "twoPair",
          twoPair: {
            aCards: [dummyCard, dummyCard],
            bCards: [dummyCard, dummyCard],
            kicker: dummyCard,
          },
        },
      },
      "Two Pair:"
    );
  });

  it("should render four of a kind", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "fourKind",
          fourKind: {
            cards: [dummyCard, dummyCard, dummyCard, dummyCard],
            kicker: dummyCard,
          },
        },
      },
      "Four of a Kind:"
    );
  });

  it("should render flush", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "flush",
          flush: {
            cards: [dummyCard, dummyCard, dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Flush:"
    );
  });

  it("should render straight", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "straight",
          straight: {
            cards: [dummyCard, dummyCard, dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Straight:"
    );
  });

  it("should render full house", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "fullHouse",
          fullHouse: {
            parents: [dummyCard, dummyCard],
            children: [dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Full House:"
    );
  });

  it("should render straight flush", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "straightFlush",
          straightFlush: {
            cards: [dummyCard, dummyCard, dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Straight Flush:"
    );
  });

  it("should render royal flush", async () => {
    renderAndAssert(
      {
        combination: {
          $case: "royalFlush",
          royalFlush: {
            cards: [dummyCard, dummyCard, dummyCard, dummyCard, dummyCard],
          },
        },
      },
      "Royal Flush:"
    );
  });
});
