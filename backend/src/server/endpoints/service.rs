use crate::server::app_context::AppContext;
use chrono::{DateTime, Utc};
use serde::Serialize;
use std::convert::Infallible;
use warp::hyper::StatusCode;

pub async fn handle_info_filter(
    start_date_time: &DateTime<Utc>,
    ctx: &AppContext,
) -> Result<impl warp::Reply, Infallible> {
    #[derive(Serialize)]
    #[serde(rename_all = "camelCase")]
    struct GetInfoResponse {
        version: String,
        start_date_time: DateTime<Utc>,
        // The image_tag field is important to determine if rolling updates with changed images have been applied successfully.
        // It is checked during the deployment process repeatedly.
        image_tag: String,
    }

    let version = env!("CARGO_PKG_VERSION");

    Ok(warp::reply::with_status(
        warp::reply::json(&GetInfoResponse {
            version: String::from(version),
            start_date_time: *start_date_time,
            image_tag: ctx.config().image_tag.clone(),
        }),
        StatusCode::OK,
    ))
}

pub async fn handle_statistics_filter(ctx: &AppContext) -> Result<impl warp::Reply, Infallible> {
    #[derive(Serialize)]
    #[serde(rename_all = "camelCase")]
    struct GetStatisticsResponse {
        total_games: usize,
        total_players: usize,
    }

    let (total_games, total_players) = tokio::join!(
        ctx.db().games().total_count(),
        ctx.db().players().total_count()
    );

    Ok(warp::reply::with_status(
        warp::reply::json(&GetStatisticsResponse {
            total_games: total_games.unwrap_or(0),
            total_players: total_players.unwrap_or(0),
        }),
        StatusCode::OK,
    ))
}

#[cfg(test)]
mod tests {
    use super::{handle_info_filter, handle_statistics_filter, AppContext};
    use chrono::Utc;

    #[tokio::test]
    async fn should_create_info() {
        let ctx = AppContext::init();
        let now = Utc::now();
        let reply = handle_info_filter(&now, &ctx).await;
        assert!(reply.is_ok());
    }

    #[tokio::test]
    async fn should_create_statistics() {
        let ctx = AppContext::init();
        let reply = handle_statistics_filter(&ctx).await;
        assert!(reply.is_ok());
    }
}
