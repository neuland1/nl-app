import { tick } from "svelte";
import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Notifications from "./Notifications.svelte";
import { notifications } from "./store";

describe("Notifications", () => {
  it("should render notifications", async () => {
    const { queryByText } = render(Notifications);

    notifications.addNotification({ message: "low severity", type: "info" });
    notifications.addNotification({
      message: "medium severity",
      type: "warning",
    });
    notifications.addNotification({ message: "high severity", type: "error" });

    await tick();

    expect(queryByText("low severity")).to.exist;
    expect(queryByText("medium severity")).to.exist;
    expect(queryByText("high severity")).to.exist;
  });

  it("should render not render disappered notifications", async () => {
    const { queryByText } = render(Notifications);

    notifications.addNotification({
      message: "disappear early",
      type: "info",
      timeoutInSeconds: 1,
    });

    await tick();
    expect(queryByText("disappear early")).to.exist;

    await new Promise((resolve) => setTimeout(resolve, 1600));
    await tick();

    expect(queryByText("disappear early")).not.to.exist;
  });
});
