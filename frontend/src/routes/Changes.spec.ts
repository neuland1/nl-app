import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Changes from "./Changes.svelte";

describe("Changes", () => {
  it("should render page", async () => {
    const { queryByText } = render(Changes);

    expect(queryByText("Changes")).to.exist;
  });
});
