import { writable, derived } from "svelte/store";
import type { Game } from "../types/proto/game";
import type { OwnPlayer, Player } from "../types/proto/player";
import { tweened } from "svelte/motion";
import { cubicOut } from "svelte/easing";

function createActiveGame() {
  const { subscribe, set } = writable<Game | null>(null);

  return {
    subscribe,
    update: (game: Game) => {
      void tweenedCredits.updatePot(game.pot);
      if (game.round) {
        void tweenedCredits.updateBet(game.round.bet);
      }
      set(game);
    },
    reset: () => set(null),
  };
}

function createPlayers() {
  const { subscribe, set, update } = writable<Map<string, Player>>(new Map());

  return {
    subscribe,
    update: (player: Player) =>
      update((current) => {
        current.set(player.id, player);
        return current;
      }),
    remove: (id: string) =>
      update((current) => {
        current.delete(id);
        return current;
      }),
    reset: () => set(new Map()),
  };
}

function createActivities() {
  const { subscribe, set, update } = writable<Map<string, boolean>>(new Map());

  return {
    subscribe,
    update: (id: string, isActive: boolean) =>
      update((current) => {
        current.set(id, isActive);
        return current;
      }),
    remove: (id: string) =>
      update((current) => {
        current.delete(id);
        return current;
      }),
    reset: () => set(new Map()),
  };
}

function createOwnPlayer() {
  const { subscribe, set } = writable<OwnPlayer | null>(null);

  return {
    subscribe,
    update: (player: OwnPlayer) => set(player),
    reset: () => set(null),
  };
}

function createCurrentRound() {
  return derived<
    [typeof activeGame, typeof ownPlayer],
    { prevPlayers: Array<string>; nextPlayers: Array<string> }
  >([activeGame, ownPlayer], ([newGame, ownPlayer], set) => {
    if (!newGame || !ownPlayer) {
      set({ nextPlayers: [], prevPlayers: [] });
      return;
    }
    const ownIndex = newGame.playerIds.findIndex((p) => p === ownPlayer.id);
    const nextPlayers = newGame.playerIds.filter((_, i) => i > ownIndex);
    const prevPlayers = newGame.playerIds.filter((_, i) => i < ownIndex);
    set({ nextPlayers, prevPlayers });
  });
}

function createTweenedCredits() {
  const { subscribe, update } = tweened<{ pot: number; bet: number }>(
    { pot: 0, bet: 0 },
    {
      duration: 1000,
      easing: cubicOut,
    }
  );

  return {
    subscribe,
    updatePot: (pot: number) => update(({ bet }) => ({ pot, bet })),
    updateBet: (bet: number) => update(({ pot }) => ({ pot, bet })),
  };
}

export const activeGame = createActiveGame();
export const ownPlayer = createOwnPlayer();
export const players = createPlayers();
export const activities = createActivities();
export const currentRound = createCurrentRound();
export const tweenedCredits = createTweenedCredits();
