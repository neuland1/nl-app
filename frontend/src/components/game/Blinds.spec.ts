import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Blinds from "./Blinds.svelte";

describe("Blinds", () => {
  it("should render big blind", async () => {
    const { queryByText } = render(Blinds, {
      props: {
        playerId: "abc",
        game: {
          smallBlindId: "abc",
          bigBlindId: "xyz",
        },
      },
    });

    expect(queryByText("S")).to.exist;
    expect(queryByText("B")).not.to.exist;
  });

  it("should render small blind", async () => {
    const { queryByText } = render(Blinds, {
      props: {
        playerId: "xyz",
        game: {
          smallBlindId: "abc",
          bigBlindId: "xyz",
        },
      },
    });

    expect(queryByText("S")).not.to.exist;
    expect(queryByText("B")).to.exist;
  });

  it("should render nothing", async () => {
    const { queryByText } = render(Blinds, {
      props: {
        playerId: "klm",
        game: {
          smallBlindId: "abc",
          bigBlindId: "xyz",
        },
      },
    });

    expect(queryByText("B")).not.to.exist;
    expect(queryByText("S")).not.to.exist;
  });
});
