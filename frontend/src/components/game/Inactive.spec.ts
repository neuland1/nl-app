import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Inactive from "./Inactive.svelte";

describe("Inactive", () => {
  it("should render inactive flag", async () => {
    const { queryByText } = render(Inactive, {
      props: {
        isActive: false,
      },
    });

    expect(queryByText("(Inactive)")).to.exist;
  });

  it("should render no inactive flag", async () => {
    const { queryByText } = render(Inactive, {
      props: {
        isActive: true,
      },
    });

    expect(queryByText("(Inactive)")).not.to.exist;
  });
});
