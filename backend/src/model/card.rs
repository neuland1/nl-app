use super::proto::{self};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub struct Card {
    color: CardColor,
    value: u32,
}

impl Card {
    pub fn new(color: CardColor, value: u32) -> Self {
        if value > 13 || value == 0 {
            panic!("Invalid card value provided");
        }

        Card { color, value }
    }

    pub fn value(&self) -> u32 {
        self.value
    }

    pub fn color(&self) -> &CardColor {
        &self.color
    }
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub enum CardColor {
    Hearts,
    Tiles,
    Clovers,
    Pikes,
}

impl CardColor {
    pub fn value(&self) -> u8 {
        match self {
            CardColor::Clovers => 0,
            CardColor::Tiles => 1,
            CardColor::Hearts => 2,
            CardColor::Pikes => 3,
        }
    }
}

impl From<proto::card::Card> for Card {
    fn from(proto_card: proto::card::Card) -> Self {
        Card {
            color: match proto_card.color {
                proto::card::Card_Color::COLOR_HEARTS => CardColor::Hearts,
                proto::card::Card_Color::COLOR_TILES => CardColor::Tiles,
                proto::card::Card_Color::COLOR_CLOVERS => CardColor::Clovers,
                proto::card::Card_Color::COLOR_PIKES => CardColor::Pikes,
                proto::card::Card_Color::COLOR_UNSPECIFIED => CardColor::Hearts,
            },
            value: proto_card.value,
        }
    }
}

impl Into<proto::card::Card> for Card {
    fn into(self) -> proto::card::Card {
        let mut card = proto::card::Card::new();
        card.set_color(match self.color {
            CardColor::Hearts => proto::card::Card_Color::COLOR_HEARTS,
            CardColor::Tiles => proto::card::Card_Color::COLOR_TILES,
            CardColor::Clovers => proto::card::Card_Color::COLOR_CLOVERS,
            CardColor::Pikes => proto::card::Card_Color::COLOR_PIKES,
        });
        card.set_value(self.value);
        card
    }
}

#[cfg(test)]
mod tests {
    use super::{Card, CardColor};
    use crate::model::proto::{self};

    #[test]
    #[should_panic(expected = "Invalid card value provided")]
    fn should_panic_for_invalid_value() {
        Card::new(CardColor::Pikes, 14);
    }

    #[test]
    #[should_panic(expected = "Invalid card value provided")]
    fn should_panic_for_zero_value() {
        Card::new(CardColor::Pikes, 0);
    }

    #[test]
    fn should_create_king_pikes() {
        let card = Card::new(CardColor::Pikes, 13);
        assert_eq!(card.color(), &CardColor::Pikes);
        assert_eq!(card.value(), 13);
    }

    #[test]
    fn should_create_ace_hearts() {
        let card = Card::new(CardColor::Hearts, 1);
        assert_eq!(card.color(), &CardColor::Hearts);
        assert_eq!(card.value(), 1);
    }

    #[test]
    fn should_serialized_and_deserialize_card_with_hearts() {
        let card = Card::new(CardColor::Hearts, 1);
        let serialized: proto::card::Card = card.clone().into();

        assert_eq!(Card::from(serialized), card);
    }

    #[test]
    fn should_serialized_and_deserialize_card_with_clovers() {
        let card = Card::new(CardColor::Clovers, 1);
        let serialized: proto::card::Card = card.clone().into();

        assert_eq!(Card::from(serialized), card);
    }

    #[test]
    fn should_serialized_and_deserialize_card_with_pikes() {
        let card = Card::new(CardColor::Pikes, 1);
        let serialized: proto::card::Card = card.clone().into();

        assert_eq!(Card::from(serialized), card);
    }

    #[test]
    fn should_serialized_and_deserialize_card_with_tiles() {
        let card = Card::new(CardColor::Tiles, 1);
        let serialized: proto::card::Card = card.clone().into();

        assert_eq!(Card::from(serialized), card);
    }
}
