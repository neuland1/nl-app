import { render } from "@testing-library/svelte";
import { expect } from "chai";
import GameLobby from "./GameLobby.svelte";

describe("GameLobby", () => {
  it("should render lobby", async () => {
    const { queryByText } = render(GameLobby);

    expect(queryByText("Game")).to.exist;
  });
});
