export const roadmap: Array<{
  priority: string;
  items: Array<string>;
}> = [
  {
    priority: "Medium",
    items: [
      "Allow buy in after missing credits or for new players",
      "Support spectators after game has started",
      "Add expiration date to user token",
      "Update token on user name updates",
    ],
  },
  {
    priority: "Low",
    items: [
      "Show realtime user video instead of only name",
      "Show table of combinations",
      "Kick users as admin",
      "Add timeout fallback for database queries",
    ],
  },
];
