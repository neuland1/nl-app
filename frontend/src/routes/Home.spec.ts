import { render } from "@testing-library/svelte";
import { expect } from "chai";
import sinon from "sinon";
import Home from "./Home.svelte";
import { jsonOk } from "../utils/test";

describe("Home", () => {
  beforeEach(() => {
    let stub = sinon.stub(window, "fetch");
    stub.onCall(0).returns(jsonOk({ totalGames: 123, totalPlayers: 567 }));
  });

  afterEach(() => {
    (window.fetch as any).restore();
  });

  it("should render home", async () => {
    const { queryByText } = render(Home);

    await new Promise((resolve) => setTimeout(resolve, 100));

    expect(queryByText("Lobby")).to.exist;
    expect(
      queryByText(
        "There are currently 123 active games and 567 active players."
      )
    ).to.exist;
  });
});
