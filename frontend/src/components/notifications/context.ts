import { getContext } from "svelte";
import type { NotificationMessage } from "./store";

const context: NotificationsContext = {
  subscribe: () => {
    // dummy
  },
  addNotification: () => {
    // dummy
  },
};

type NotificationsContext = {
  subscribe: () => void;
  addNotification: (message: NotificationMessage) => void;
};

export const getNotificationsContext = (): NotificationsContext =>
  getContext<NotificationsContext>(context);

export default context;
