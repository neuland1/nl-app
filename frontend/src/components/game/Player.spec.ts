import { render } from "@testing-library/svelte";
import { expect } from "chai";
import { Card_Color } from "../../types/proto/card";
import { Game, Game_State } from "../../types/proto/game";
import type { Player } from "../../types/proto/player";
import Comp from "./Player.svelte";

describe("Player", () => {
  it("should render player", async () => {
    const game: Game = {
      adminId: "a",
      bigBlindId: "a",
      smallBlindId: "b",
      blind: 10,
      playerIds: ["a", "b"],
      pot: 1,
      state: Game_State.STATE_STARTED,
      token: "a",
      round: {
        bet: 10,
        assignedCards: {
          a: {
            cards: [],
            count: 2,
          },
          b: {
            count: 2,
            cards: [
              { color: Card_Color.COLOR_PIKES, value: 1 },
              { color: Card_Color.COLOR_HEARTS, value: 2 },
            ],
          },
        },
        winners: [],
        foldPlayerIds: ["b"],
        lastRaiserId: "a",
        currentPlayerId: "a",
        openCards: [
          { color: Card_Color.COLOR_CLOVERS, value: 13 },
          { color: Card_Color.COLOR_HEARTS, value: 12 },
          { color: Card_Color.COLOR_PIKES, value: 11 },
          { color: Card_Color.COLOR_TILES, value: 10 },
          { color: Card_Color.COLOR_CLOVERS, value: 9 },
        ],
        bets: {
          ["a"]: 123,
        },
      },
    };
    const playerId = "a";
    const ownPlayerId = "a";
    const activities = new Map();
    activities.set("a", true);
    activities.set("b", true);
    const players = new Map<string, Player>();
    players.set("a", {
      credits: 999,
      id: "a",
      name: "a",
    });
    const { queryByText } = render(Comp, {
      props: {
        game,
        playerId,
        ownPlayerId,
        activities,
        players,
      },
    });

    expect(queryByText("a")).to.exist;
    expect(queryByText("999")).to.exist;
    expect(queryByText("123")).to.exist;
  });
});
