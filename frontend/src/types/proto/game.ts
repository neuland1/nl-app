/* eslint-disable */
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import * as Long from "long";
import { Card } from "./card";

export const protobufPackage = "";

export interface Cards {
  cards: Card[];
  count: number;
}

export interface Combination {
  combination?:
    | { $case: "highCard"; highCard: Combination_HighCard }
    | { $case: "pair"; pair: Combination_Pair }
    | { $case: "twoPair"; twoPair: Combination_TwoPair }
    | { $case: "threeKind"; threeKind: Combination_ThreeKind }
    | { $case: "straight"; straight: Combination_Straight }
    | { $case: "flush"; flush: Combination_Flush }
    | { $case: "fullHouse"; fullHouse: Combination_FullHouse }
    | { $case: "fourKind"; fourKind: Combination_FourKind }
    | { $case: "straightFlush"; straightFlush: Combination_StraightFlush }
    | { $case: "royalFlush"; royalFlush: Combination_RoyalFlush };
}

export interface Combination_HighCard {
  card?: Card;
  kickers: Card[];
}

export interface Combination_Pair {
  cards: Card[];
  kickers: Card[];
}

export interface Combination_TwoPair {
  aCards: Card[];
  bCards: Card[];
  kicker?: Card;
}

export interface Combination_ThreeKind {
  cards: Card[];
  kickers: Card[];
}

export interface Combination_Straight {
  cards: Card[];
}

export interface Combination_Flush {
  cards: Card[];
}

export interface Combination_FullHouse {
  parents: Card[];
  children: Card[];
}

export interface Combination_FourKind {
  cards: Card[];
  kicker?: Card;
}

export interface Combination_StraightFlush {
  cards: Card[];
}

export interface Combination_RoyalFlush {
  cards: Card[];
}

export interface Round {
  openCards: Card[];
  assignedCards: { [key: string]: Cards };
  currentPlayerId: string;
  bet: number;
  lastRaiserId: string;
  bets: { [key: string]: number };
  foldPlayerIds: string[];
  winners: string[];
  winningCombination?: Combination;
}

export interface Round_AssignedCardsEntry {
  key: string;
  value?: Cards;
}

export interface Round_BetsEntry {
  key: string;
  value: number;
}

export interface Game {
  token: string;
  adminId: string;
  state: Game_State;
  pot: number;
  round?: Round;
  smallBlindId: string;
  bigBlindId: string;
  blind: number;
  playerIds: string[];
}

export enum Game_State {
  STATE_UNSPECIFIED = 0,
  STATE_INITIALIZED = 1,
  STATE_STARTED = 2,
  STATE_ABANDONED = 3,
  STATE_FINISHED = 4,
  UNRECOGNIZED = -1,
}

export function game_StateFromJSON(object: any): Game_State {
  switch (object) {
    case 0:
    case "STATE_UNSPECIFIED":
      return Game_State.STATE_UNSPECIFIED;
    case 1:
    case "STATE_INITIALIZED":
      return Game_State.STATE_INITIALIZED;
    case 2:
    case "STATE_STARTED":
      return Game_State.STATE_STARTED;
    case 3:
    case "STATE_ABANDONED":
      return Game_State.STATE_ABANDONED;
    case 4:
    case "STATE_FINISHED":
      return Game_State.STATE_FINISHED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return Game_State.UNRECOGNIZED;
  }
}

export function game_StateToJSON(object: Game_State): string {
  switch (object) {
    case Game_State.STATE_UNSPECIFIED:
      return "STATE_UNSPECIFIED";
    case Game_State.STATE_INITIALIZED:
      return "STATE_INITIALIZED";
    case Game_State.STATE_STARTED:
      return "STATE_STARTED";
    case Game_State.STATE_ABANDONED:
      return "STATE_ABANDONED";
    case Game_State.STATE_FINISHED:
      return "STATE_FINISHED";
    default:
      return "UNKNOWN";
  }
}

const baseCards: object = { count: 0 };

export const Cards = {
  encode(message: Cards, writer: Writer = Writer.create()): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.count !== 0) {
      writer.uint32(16).uint32(message.count);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Cards {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCards } as Cards;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        case 2:
          message.count = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Cards {
    const message = { ...baseCards } as Cards;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    if (object.count !== undefined && object.count !== null) {
      message.count = Number(object.count);
    }
    return message;
  },

  toJSON(message: Cards): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    message.count !== undefined && (obj.count = message.count);
    return obj;
  },

  fromPartial(object: DeepPartial<Cards>): Cards {
    const message = { ...baseCards } as Cards;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    if (object.count !== undefined && object.count !== null) {
      message.count = object.count;
    }
    return message;
  },
};

const baseCombination: object = {};

export const Combination = {
  encode(message: Combination, writer: Writer = Writer.create()): Writer {
    if (message.combination?.$case === "highCard") {
      Combination_HighCard.encode(
        message.combination.highCard,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "pair") {
      Combination_Pair.encode(
        message.combination.pair,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "twoPair") {
      Combination_TwoPair.encode(
        message.combination.twoPair,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "threeKind") {
      Combination_ThreeKind.encode(
        message.combination.threeKind,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "straight") {
      Combination_Straight.encode(
        message.combination.straight,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "flush") {
      Combination_Flush.encode(
        message.combination.flush,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "fullHouse") {
      Combination_FullHouse.encode(
        message.combination.fullHouse,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "fourKind") {
      Combination_FourKind.encode(
        message.combination.fourKind,
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "straightFlush") {
      Combination_StraightFlush.encode(
        message.combination.straightFlush,
        writer.uint32(74).fork()
      ).ldelim();
    }
    if (message.combination?.$case === "royalFlush") {
      Combination_RoyalFlush.encode(
        message.combination.royalFlush,
        writer.uint32(82).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination } as Combination;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.combination = {
            $case: "highCard",
            highCard: Combination_HighCard.decode(reader, reader.uint32()),
          };
          break;
        case 2:
          message.combination = {
            $case: "pair",
            pair: Combination_Pair.decode(reader, reader.uint32()),
          };
          break;
        case 3:
          message.combination = {
            $case: "twoPair",
            twoPair: Combination_TwoPair.decode(reader, reader.uint32()),
          };
          break;
        case 4:
          message.combination = {
            $case: "threeKind",
            threeKind: Combination_ThreeKind.decode(reader, reader.uint32()),
          };
          break;
        case 5:
          message.combination = {
            $case: "straight",
            straight: Combination_Straight.decode(reader, reader.uint32()),
          };
          break;
        case 6:
          message.combination = {
            $case: "flush",
            flush: Combination_Flush.decode(reader, reader.uint32()),
          };
          break;
        case 7:
          message.combination = {
            $case: "fullHouse",
            fullHouse: Combination_FullHouse.decode(reader, reader.uint32()),
          };
          break;
        case 8:
          message.combination = {
            $case: "fourKind",
            fourKind: Combination_FourKind.decode(reader, reader.uint32()),
          };
          break;
        case 9:
          message.combination = {
            $case: "straightFlush",
            straightFlush: Combination_StraightFlush.decode(
              reader,
              reader.uint32()
            ),
          };
          break;
        case 10:
          message.combination = {
            $case: "royalFlush",
            royalFlush: Combination_RoyalFlush.decode(reader, reader.uint32()),
          };
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination {
    const message = { ...baseCombination } as Combination;
    if (object.highCard !== undefined && object.highCard !== null) {
      message.combination = {
        $case: "highCard",
        highCard: Combination_HighCard.fromJSON(object.highCard),
      };
    }
    if (object.pair !== undefined && object.pair !== null) {
      message.combination = {
        $case: "pair",
        pair: Combination_Pair.fromJSON(object.pair),
      };
    }
    if (object.twoPair !== undefined && object.twoPair !== null) {
      message.combination = {
        $case: "twoPair",
        twoPair: Combination_TwoPair.fromJSON(object.twoPair),
      };
    }
    if (object.threeKind !== undefined && object.threeKind !== null) {
      message.combination = {
        $case: "threeKind",
        threeKind: Combination_ThreeKind.fromJSON(object.threeKind),
      };
    }
    if (object.straight !== undefined && object.straight !== null) {
      message.combination = {
        $case: "straight",
        straight: Combination_Straight.fromJSON(object.straight),
      };
    }
    if (object.flush !== undefined && object.flush !== null) {
      message.combination = {
        $case: "flush",
        flush: Combination_Flush.fromJSON(object.flush),
      };
    }
    if (object.fullHouse !== undefined && object.fullHouse !== null) {
      message.combination = {
        $case: "fullHouse",
        fullHouse: Combination_FullHouse.fromJSON(object.fullHouse),
      };
    }
    if (object.fourKind !== undefined && object.fourKind !== null) {
      message.combination = {
        $case: "fourKind",
        fourKind: Combination_FourKind.fromJSON(object.fourKind),
      };
    }
    if (object.straightFlush !== undefined && object.straightFlush !== null) {
      message.combination = {
        $case: "straightFlush",
        straightFlush: Combination_StraightFlush.fromJSON(object.straightFlush),
      };
    }
    if (object.royalFlush !== undefined && object.royalFlush !== null) {
      message.combination = {
        $case: "royalFlush",
        royalFlush: Combination_RoyalFlush.fromJSON(object.royalFlush),
      };
    }
    return message;
  },

  toJSON(message: Combination): unknown {
    const obj: any = {};
    message.combination?.$case === "highCard" &&
      (obj.highCard = message.combination?.highCard
        ? Combination_HighCard.toJSON(message.combination?.highCard)
        : undefined);
    message.combination?.$case === "pair" &&
      (obj.pair = message.combination?.pair
        ? Combination_Pair.toJSON(message.combination?.pair)
        : undefined);
    message.combination?.$case === "twoPair" &&
      (obj.twoPair = message.combination?.twoPair
        ? Combination_TwoPair.toJSON(message.combination?.twoPair)
        : undefined);
    message.combination?.$case === "threeKind" &&
      (obj.threeKind = message.combination?.threeKind
        ? Combination_ThreeKind.toJSON(message.combination?.threeKind)
        : undefined);
    message.combination?.$case === "straight" &&
      (obj.straight = message.combination?.straight
        ? Combination_Straight.toJSON(message.combination?.straight)
        : undefined);
    message.combination?.$case === "flush" &&
      (obj.flush = message.combination?.flush
        ? Combination_Flush.toJSON(message.combination?.flush)
        : undefined);
    message.combination?.$case === "fullHouse" &&
      (obj.fullHouse = message.combination?.fullHouse
        ? Combination_FullHouse.toJSON(message.combination?.fullHouse)
        : undefined);
    message.combination?.$case === "fourKind" &&
      (obj.fourKind = message.combination?.fourKind
        ? Combination_FourKind.toJSON(message.combination?.fourKind)
        : undefined);
    message.combination?.$case === "straightFlush" &&
      (obj.straightFlush = message.combination?.straightFlush
        ? Combination_StraightFlush.toJSON(message.combination?.straightFlush)
        : undefined);
    message.combination?.$case === "royalFlush" &&
      (obj.royalFlush = message.combination?.royalFlush
        ? Combination_RoyalFlush.toJSON(message.combination?.royalFlush)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Combination>): Combination {
    const message = { ...baseCombination } as Combination;
    if (
      object.combination?.$case === "highCard" &&
      object.combination?.highCard !== undefined &&
      object.combination?.highCard !== null
    ) {
      message.combination = {
        $case: "highCard",
        highCard: Combination_HighCard.fromPartial(object.combination.highCard),
      };
    }
    if (
      object.combination?.$case === "pair" &&
      object.combination?.pair !== undefined &&
      object.combination?.pair !== null
    ) {
      message.combination = {
        $case: "pair",
        pair: Combination_Pair.fromPartial(object.combination.pair),
      };
    }
    if (
      object.combination?.$case === "twoPair" &&
      object.combination?.twoPair !== undefined &&
      object.combination?.twoPair !== null
    ) {
      message.combination = {
        $case: "twoPair",
        twoPair: Combination_TwoPair.fromPartial(object.combination.twoPair),
      };
    }
    if (
      object.combination?.$case === "threeKind" &&
      object.combination?.threeKind !== undefined &&
      object.combination?.threeKind !== null
    ) {
      message.combination = {
        $case: "threeKind",
        threeKind: Combination_ThreeKind.fromPartial(
          object.combination.threeKind
        ),
      };
    }
    if (
      object.combination?.$case === "straight" &&
      object.combination?.straight !== undefined &&
      object.combination?.straight !== null
    ) {
      message.combination = {
        $case: "straight",
        straight: Combination_Straight.fromPartial(object.combination.straight),
      };
    }
    if (
      object.combination?.$case === "flush" &&
      object.combination?.flush !== undefined &&
      object.combination?.flush !== null
    ) {
      message.combination = {
        $case: "flush",
        flush: Combination_Flush.fromPartial(object.combination.flush),
      };
    }
    if (
      object.combination?.$case === "fullHouse" &&
      object.combination?.fullHouse !== undefined &&
      object.combination?.fullHouse !== null
    ) {
      message.combination = {
        $case: "fullHouse",
        fullHouse: Combination_FullHouse.fromPartial(
          object.combination.fullHouse
        ),
      };
    }
    if (
      object.combination?.$case === "fourKind" &&
      object.combination?.fourKind !== undefined &&
      object.combination?.fourKind !== null
    ) {
      message.combination = {
        $case: "fourKind",
        fourKind: Combination_FourKind.fromPartial(object.combination.fourKind),
      };
    }
    if (
      object.combination?.$case === "straightFlush" &&
      object.combination?.straightFlush !== undefined &&
      object.combination?.straightFlush !== null
    ) {
      message.combination = {
        $case: "straightFlush",
        straightFlush: Combination_StraightFlush.fromPartial(
          object.combination.straightFlush
        ),
      };
    }
    if (
      object.combination?.$case === "royalFlush" &&
      object.combination?.royalFlush !== undefined &&
      object.combination?.royalFlush !== null
    ) {
      message.combination = {
        $case: "royalFlush",
        royalFlush: Combination_RoyalFlush.fromPartial(
          object.combination.royalFlush
        ),
      };
    }
    return message;
  },
};

const baseCombination_HighCard: object = {};

export const Combination_HighCard = {
  encode(
    message: Combination_HighCard,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.card !== undefined) {
      Card.encode(message.card, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.kickers) {
      Card.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_HighCard {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_HighCard } as Combination_HighCard;
    message.kickers = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.card = Card.decode(reader, reader.uint32());
          break;
        case 2:
          message.kickers.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_HighCard {
    const message = { ...baseCombination_HighCard } as Combination_HighCard;
    message.kickers = [];
    if (object.card !== undefined && object.card !== null) {
      message.card = Card.fromJSON(object.card);
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_HighCard): unknown {
    const obj: any = {};
    message.card !== undefined &&
      (obj.card = message.card ? Card.toJSON(message.card) : undefined);
    if (message.kickers) {
      obj.kickers = message.kickers.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.kickers = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_HighCard>): Combination_HighCard {
    const message = { ...baseCombination_HighCard } as Combination_HighCard;
    message.kickers = [];
    if (object.card !== undefined && object.card !== null) {
      message.card = Card.fromPartial(object.card);
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_Pair: object = {};

export const Combination_Pair = {
  encode(message: Combination_Pair, writer: Writer = Writer.create()): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.kickers) {
      Card.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_Pair {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_Pair } as Combination_Pair;
    message.cards = [];
    message.kickers = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        case 3:
          message.kickers.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_Pair {
    const message = { ...baseCombination_Pair } as Combination_Pair;
    message.cards = [];
    message.kickers = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_Pair): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    if (message.kickers) {
      obj.kickers = message.kickers.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.kickers = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_Pair>): Combination_Pair {
    const message = { ...baseCombination_Pair } as Combination_Pair;
    message.cards = [];
    message.kickers = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_TwoPair: object = {};

export const Combination_TwoPair = {
  encode(
    message: Combination_TwoPair,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.aCards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.bCards) {
      Card.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.kicker !== undefined) {
      Card.encode(message.kicker, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_TwoPair {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_TwoPair } as Combination_TwoPair;
    message.aCards = [];
    message.bCards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.aCards.push(Card.decode(reader, reader.uint32()));
          break;
        case 2:
          message.bCards.push(Card.decode(reader, reader.uint32()));
          break;
        case 3:
          message.kicker = Card.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_TwoPair {
    const message = { ...baseCombination_TwoPair } as Combination_TwoPair;
    message.aCards = [];
    message.bCards = [];
    if (object.aCards !== undefined && object.aCards !== null) {
      for (const e of object.aCards) {
        message.aCards.push(Card.fromJSON(e));
      }
    }
    if (object.bCards !== undefined && object.bCards !== null) {
      for (const e of object.bCards) {
        message.bCards.push(Card.fromJSON(e));
      }
    }
    if (object.kicker !== undefined && object.kicker !== null) {
      message.kicker = Card.fromJSON(object.kicker);
    }
    return message;
  },

  toJSON(message: Combination_TwoPair): unknown {
    const obj: any = {};
    if (message.aCards) {
      obj.aCards = message.aCards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.aCards = [];
    }
    if (message.bCards) {
      obj.bCards = message.bCards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.bCards = [];
    }
    message.kicker !== undefined &&
      (obj.kicker = message.kicker ? Card.toJSON(message.kicker) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_TwoPair>): Combination_TwoPair {
    const message = { ...baseCombination_TwoPair } as Combination_TwoPair;
    message.aCards = [];
    message.bCards = [];
    if (object.aCards !== undefined && object.aCards !== null) {
      for (const e of object.aCards) {
        message.aCards.push(Card.fromPartial(e));
      }
    }
    if (object.bCards !== undefined && object.bCards !== null) {
      for (const e of object.bCards) {
        message.bCards.push(Card.fromPartial(e));
      }
    }
    if (object.kicker !== undefined && object.kicker !== null) {
      message.kicker = Card.fromPartial(object.kicker);
    }
    return message;
  },
};

const baseCombination_ThreeKind: object = {};

export const Combination_ThreeKind = {
  encode(
    message: Combination_ThreeKind,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.kickers) {
      Card.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_ThreeKind {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_ThreeKind } as Combination_ThreeKind;
    message.cards = [];
    message.kickers = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        case 4:
          message.kickers.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_ThreeKind {
    const message = { ...baseCombination_ThreeKind } as Combination_ThreeKind;
    message.cards = [];
    message.kickers = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_ThreeKind): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    if (message.kickers) {
      obj.kickers = message.kickers.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.kickers = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<Combination_ThreeKind>
  ): Combination_ThreeKind {
    const message = { ...baseCombination_ThreeKind } as Combination_ThreeKind;
    message.cards = [];
    message.kickers = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    if (object.kickers !== undefined && object.kickers !== null) {
      for (const e of object.kickers) {
        message.kickers.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_Straight: object = {};

export const Combination_Straight = {
  encode(
    message: Combination_Straight,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_Straight {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_Straight } as Combination_Straight;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_Straight {
    const message = { ...baseCombination_Straight } as Combination_Straight;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_Straight): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_Straight>): Combination_Straight {
    const message = { ...baseCombination_Straight } as Combination_Straight;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_Flush: object = {};

export const Combination_Flush = {
  encode(message: Combination_Flush, writer: Writer = Writer.create()): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_Flush {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_Flush } as Combination_Flush;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_Flush {
    const message = { ...baseCombination_Flush } as Combination_Flush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_Flush): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_Flush>): Combination_Flush {
    const message = { ...baseCombination_Flush } as Combination_Flush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_FullHouse: object = {};

export const Combination_FullHouse = {
  encode(
    message: Combination_FullHouse,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.parents) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.children) {
      Card.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_FullHouse {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_FullHouse } as Combination_FullHouse;
    message.parents = [];
    message.children = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.parents.push(Card.decode(reader, reader.uint32()));
          break;
        case 2:
          message.children.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_FullHouse {
    const message = { ...baseCombination_FullHouse } as Combination_FullHouse;
    message.parents = [];
    message.children = [];
    if (object.parents !== undefined && object.parents !== null) {
      for (const e of object.parents) {
        message.parents.push(Card.fromJSON(e));
      }
    }
    if (object.children !== undefined && object.children !== null) {
      for (const e of object.children) {
        message.children.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_FullHouse): unknown {
    const obj: any = {};
    if (message.parents) {
      obj.parents = message.parents.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.parents = [];
    }
    if (message.children) {
      obj.children = message.children.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.children = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<Combination_FullHouse>
  ): Combination_FullHouse {
    const message = { ...baseCombination_FullHouse } as Combination_FullHouse;
    message.parents = [];
    message.children = [];
    if (object.parents !== undefined && object.parents !== null) {
      for (const e of object.parents) {
        message.parents.push(Card.fromPartial(e));
      }
    }
    if (object.children !== undefined && object.children !== null) {
      for (const e of object.children) {
        message.children.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_FourKind: object = {};

export const Combination_FourKind = {
  encode(
    message: Combination_FourKind,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.kicker !== undefined) {
      Card.encode(message.kicker, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_FourKind {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_FourKind } as Combination_FourKind;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        case 2:
          message.kicker = Card.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_FourKind {
    const message = { ...baseCombination_FourKind } as Combination_FourKind;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    if (object.kicker !== undefined && object.kicker !== null) {
      message.kicker = Card.fromJSON(object.kicker);
    }
    return message;
  },

  toJSON(message: Combination_FourKind): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    message.kicker !== undefined &&
      (obj.kicker = message.kicker ? Card.toJSON(message.kicker) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Combination_FourKind>): Combination_FourKind {
    const message = { ...baseCombination_FourKind } as Combination_FourKind;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    if (object.kicker !== undefined && object.kicker !== null) {
      message.kicker = Card.fromPartial(object.kicker);
    }
    return message;
  },
};

const baseCombination_StraightFlush: object = {};

export const Combination_StraightFlush = {
  encode(
    message: Combination_StraightFlush,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): Combination_StraightFlush {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCombination_StraightFlush,
    } as Combination_StraightFlush;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_StraightFlush {
    const message = {
      ...baseCombination_StraightFlush,
    } as Combination_StraightFlush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_StraightFlush): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<Combination_StraightFlush>
  ): Combination_StraightFlush {
    const message = {
      ...baseCombination_StraightFlush,
    } as Combination_StraightFlush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseCombination_RoyalFlush: object = {};

export const Combination_RoyalFlush = {
  encode(
    message: Combination_RoyalFlush,
    writer: Writer = Writer.create()
  ): Writer {
    for (const v of message.cards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Combination_RoyalFlush {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCombination_RoyalFlush } as Combination_RoyalFlush;
    message.cards = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.cards.push(Card.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Combination_RoyalFlush {
    const message = { ...baseCombination_RoyalFlush } as Combination_RoyalFlush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: Combination_RoyalFlush): unknown {
    const obj: any = {};
    if (message.cards) {
      obj.cards = message.cards.map((e) => (e ? Card.toJSON(e) : undefined));
    } else {
      obj.cards = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<Combination_RoyalFlush>
  ): Combination_RoyalFlush {
    const message = { ...baseCombination_RoyalFlush } as Combination_RoyalFlush;
    message.cards = [];
    if (object.cards !== undefined && object.cards !== null) {
      for (const e of object.cards) {
        message.cards.push(Card.fromPartial(e));
      }
    }
    return message;
  },
};

const baseRound: object = {
  currentPlayerId: "",
  bet: 0,
  lastRaiserId: "",
  foldPlayerIds: "",
  winners: "",
};

export const Round = {
  encode(message: Round, writer: Writer = Writer.create()): Writer {
    for (const v of message.openCards) {
      Card.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    Object.entries(message.assignedCards).forEach(([key, value]) => {
      Round_AssignedCardsEntry.encode(
        { key: key as any, value },
        writer.uint32(26).fork()
      ).ldelim();
    });
    if (message.currentPlayerId !== "") {
      writer.uint32(34).string(message.currentPlayerId);
    }
    if (message.bet !== 0) {
      writer.uint32(40).uint32(message.bet);
    }
    if (message.lastRaiserId !== "") {
      writer.uint32(50).string(message.lastRaiserId);
    }
    Object.entries(message.bets).forEach(([key, value]) => {
      Round_BetsEntry.encode(
        { key: key as any, value },
        writer.uint32(58).fork()
      ).ldelim();
    });
    for (const v of message.foldPlayerIds) {
      writer.uint32(66).string(v!);
    }
    for (const v of message.winners) {
      writer.uint32(74).string(v!);
    }
    if (message.winningCombination !== undefined) {
      Combination.encode(
        message.winningCombination,
        writer.uint32(90).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Round {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRound } as Round;
    message.openCards = [];
    message.assignedCards = {};
    message.bets = {};
    message.foldPlayerIds = [];
    message.winners = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.openCards.push(Card.decode(reader, reader.uint32()));
          break;
        case 3:
          const entry3 = Round_AssignedCardsEntry.decode(
            reader,
            reader.uint32()
          );
          if (entry3.value !== undefined) {
            message.assignedCards[entry3.key] = entry3.value;
          }
          break;
        case 4:
          message.currentPlayerId = reader.string();
          break;
        case 5:
          message.bet = reader.uint32();
          break;
        case 6:
          message.lastRaiserId = reader.string();
          break;
        case 7:
          const entry7 = Round_BetsEntry.decode(reader, reader.uint32());
          if (entry7.value !== undefined) {
            message.bets[entry7.key] = entry7.value;
          }
          break;
        case 8:
          message.foldPlayerIds.push(reader.string());
          break;
        case 9:
          message.winners.push(reader.string());
          break;
        case 11:
          message.winningCombination = Combination.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Round {
    const message = { ...baseRound } as Round;
    message.openCards = [];
    message.assignedCards = {};
    message.bets = {};
    message.foldPlayerIds = [];
    message.winners = [];
    if (object.openCards !== undefined && object.openCards !== null) {
      for (const e of object.openCards) {
        message.openCards.push(Card.fromJSON(e));
      }
    }
    if (object.assignedCards !== undefined && object.assignedCards !== null) {
      Object.entries(object.assignedCards).forEach(([key, value]) => {
        message.assignedCards[key] = Cards.fromJSON(value);
      });
    }
    if (
      object.currentPlayerId !== undefined &&
      object.currentPlayerId !== null
    ) {
      message.currentPlayerId = String(object.currentPlayerId);
    }
    if (object.bet !== undefined && object.bet !== null) {
      message.bet = Number(object.bet);
    }
    if (object.lastRaiserId !== undefined && object.lastRaiserId !== null) {
      message.lastRaiserId = String(object.lastRaiserId);
    }
    if (object.bets !== undefined && object.bets !== null) {
      Object.entries(object.bets).forEach(([key, value]) => {
        message.bets[key] = Number(value);
      });
    }
    if (object.foldPlayerIds !== undefined && object.foldPlayerIds !== null) {
      for (const e of object.foldPlayerIds) {
        message.foldPlayerIds.push(String(e));
      }
    }
    if (object.winners !== undefined && object.winners !== null) {
      for (const e of object.winners) {
        message.winners.push(String(e));
      }
    }
    if (
      object.winningCombination !== undefined &&
      object.winningCombination !== null
    ) {
      message.winningCombination = Combination.fromJSON(
        object.winningCombination
      );
    }
    return message;
  },

  toJSON(message: Round): unknown {
    const obj: any = {};
    if (message.openCards) {
      obj.openCards = message.openCards.map((e) =>
        e ? Card.toJSON(e) : undefined
      );
    } else {
      obj.openCards = [];
    }
    obj.assignedCards = {};
    if (message.assignedCards) {
      Object.entries(message.assignedCards).forEach(([k, v]) => {
        obj.assignedCards[k] = Cards.toJSON(v);
      });
    }
    message.currentPlayerId !== undefined &&
      (obj.currentPlayerId = message.currentPlayerId);
    message.bet !== undefined && (obj.bet = message.bet);
    message.lastRaiserId !== undefined &&
      (obj.lastRaiserId = message.lastRaiserId);
    obj.bets = {};
    if (message.bets) {
      Object.entries(message.bets).forEach(([k, v]) => {
        obj.bets[k] = v;
      });
    }
    if (message.foldPlayerIds) {
      obj.foldPlayerIds = message.foldPlayerIds.map((e) => e);
    } else {
      obj.foldPlayerIds = [];
    }
    if (message.winners) {
      obj.winners = message.winners.map((e) => e);
    } else {
      obj.winners = [];
    }
    message.winningCombination !== undefined &&
      (obj.winningCombination = message.winningCombination
        ? Combination.toJSON(message.winningCombination)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Round>): Round {
    const message = { ...baseRound } as Round;
    message.openCards = [];
    message.assignedCards = {};
    message.bets = {};
    message.foldPlayerIds = [];
    message.winners = [];
    if (object.openCards !== undefined && object.openCards !== null) {
      for (const e of object.openCards) {
        message.openCards.push(Card.fromPartial(e));
      }
    }
    if (object.assignedCards !== undefined && object.assignedCards !== null) {
      Object.entries(object.assignedCards).forEach(([key, value]) => {
        if (value !== undefined) {
          message.assignedCards[key] = Cards.fromPartial(value);
        }
      });
    }
    if (
      object.currentPlayerId !== undefined &&
      object.currentPlayerId !== null
    ) {
      message.currentPlayerId = object.currentPlayerId;
    }
    if (object.bet !== undefined && object.bet !== null) {
      message.bet = object.bet;
    }
    if (object.lastRaiserId !== undefined && object.lastRaiserId !== null) {
      message.lastRaiserId = object.lastRaiserId;
    }
    if (object.bets !== undefined && object.bets !== null) {
      Object.entries(object.bets).forEach(([key, value]) => {
        if (value !== undefined) {
          message.bets[key] = Number(value);
        }
      });
    }
    if (object.foldPlayerIds !== undefined && object.foldPlayerIds !== null) {
      for (const e of object.foldPlayerIds) {
        message.foldPlayerIds.push(e);
      }
    }
    if (object.winners !== undefined && object.winners !== null) {
      for (const e of object.winners) {
        message.winners.push(e);
      }
    }
    if (
      object.winningCombination !== undefined &&
      object.winningCombination !== null
    ) {
      message.winningCombination = Combination.fromPartial(
        object.winningCombination
      );
    }
    return message;
  },
};

const baseRound_AssignedCardsEntry: object = { key: "" };

export const Round_AssignedCardsEntry = {
  encode(
    message: Round_AssignedCardsEntry,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.key !== "") {
      writer.uint32(10).string(message.key);
    }
    if (message.value !== undefined) {
      Cards.encode(message.value, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): Round_AssignedCardsEntry {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseRound_AssignedCardsEntry,
    } as Round_AssignedCardsEntry;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.key = reader.string();
          break;
        case 2:
          message.value = Cards.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Round_AssignedCardsEntry {
    const message = {
      ...baseRound_AssignedCardsEntry,
    } as Round_AssignedCardsEntry;
    if (object.key !== undefined && object.key !== null) {
      message.key = String(object.key);
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = Cards.fromJSON(object.value);
    }
    return message;
  },

  toJSON(message: Round_AssignedCardsEntry): unknown {
    const obj: any = {};
    message.key !== undefined && (obj.key = message.key);
    message.value !== undefined &&
      (obj.value = message.value ? Cards.toJSON(message.value) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<Round_AssignedCardsEntry>
  ): Round_AssignedCardsEntry {
    const message = {
      ...baseRound_AssignedCardsEntry,
    } as Round_AssignedCardsEntry;
    if (object.key !== undefined && object.key !== null) {
      message.key = object.key;
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = Cards.fromPartial(object.value);
    }
    return message;
  },
};

const baseRound_BetsEntry: object = { key: "", value: 0 };

export const Round_BetsEntry = {
  encode(message: Round_BetsEntry, writer: Writer = Writer.create()): Writer {
    if (message.key !== "") {
      writer.uint32(10).string(message.key);
    }
    if (message.value !== 0) {
      writer.uint32(16).uint32(message.value);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Round_BetsEntry {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRound_BetsEntry } as Round_BetsEntry;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.key = reader.string();
          break;
        case 2:
          message.value = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Round_BetsEntry {
    const message = { ...baseRound_BetsEntry } as Round_BetsEntry;
    if (object.key !== undefined && object.key !== null) {
      message.key = String(object.key);
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = Number(object.value);
    }
    return message;
  },

  toJSON(message: Round_BetsEntry): unknown {
    const obj: any = {};
    message.key !== undefined && (obj.key = message.key);
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<Round_BetsEntry>): Round_BetsEntry {
    const message = { ...baseRound_BetsEntry } as Round_BetsEntry;
    if (object.key !== undefined && object.key !== null) {
      message.key = object.key;
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = object.value;
    }
    return message;
  },
};

const baseGame: object = {
  token: "",
  adminId: "",
  state: 0,
  pot: 0,
  smallBlindId: "",
  bigBlindId: "",
  blind: 0,
  playerIds: "",
};

export const Game = {
  encode(message: Game, writer: Writer = Writer.create()): Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    if (message.adminId !== "") {
      writer.uint32(18).string(message.adminId);
    }
    if (message.state !== 0) {
      writer.uint32(24).int32(message.state);
    }
    if (message.pot !== 0) {
      writer.uint32(32).uint32(message.pot);
    }
    if (message.round !== undefined) {
      Round.encode(message.round, writer.uint32(42).fork()).ldelim();
    }
    if (message.smallBlindId !== "") {
      writer.uint32(50).string(message.smallBlindId);
    }
    if (message.bigBlindId !== "") {
      writer.uint32(58).string(message.bigBlindId);
    }
    if (message.blind !== 0) {
      writer.uint32(64).uint32(message.blind);
    }
    for (const v of message.playerIds) {
      writer.uint32(74).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Game {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGame } as Game;
    message.playerIds = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.token = reader.string();
          break;
        case 2:
          message.adminId = reader.string();
          break;
        case 3:
          message.state = reader.int32() as any;
          break;
        case 4:
          message.pot = reader.uint32();
          break;
        case 5:
          message.round = Round.decode(reader, reader.uint32());
          break;
        case 6:
          message.smallBlindId = reader.string();
          break;
        case 7:
          message.bigBlindId = reader.string();
          break;
        case 8:
          message.blind = reader.uint32();
          break;
        case 9:
          message.playerIds.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Game {
    const message = { ...baseGame } as Game;
    message.playerIds = [];
    if (object.token !== undefined && object.token !== null) {
      message.token = String(object.token);
    }
    if (object.adminId !== undefined && object.adminId !== null) {
      message.adminId = String(object.adminId);
    }
    if (object.state !== undefined && object.state !== null) {
      message.state = game_StateFromJSON(object.state);
    }
    if (object.pot !== undefined && object.pot !== null) {
      message.pot = Number(object.pot);
    }
    if (object.round !== undefined && object.round !== null) {
      message.round = Round.fromJSON(object.round);
    }
    if (object.smallBlindId !== undefined && object.smallBlindId !== null) {
      message.smallBlindId = String(object.smallBlindId);
    }
    if (object.bigBlindId !== undefined && object.bigBlindId !== null) {
      message.bigBlindId = String(object.bigBlindId);
    }
    if (object.blind !== undefined && object.blind !== null) {
      message.blind = Number(object.blind);
    }
    if (object.playerIds !== undefined && object.playerIds !== null) {
      for (const e of object.playerIds) {
        message.playerIds.push(String(e));
      }
    }
    return message;
  },

  toJSON(message: Game): unknown {
    const obj: any = {};
    message.token !== undefined && (obj.token = message.token);
    message.adminId !== undefined && (obj.adminId = message.adminId);
    message.state !== undefined &&
      (obj.state = game_StateToJSON(message.state));
    message.pot !== undefined && (obj.pot = message.pot);
    message.round !== undefined &&
      (obj.round = message.round ? Round.toJSON(message.round) : undefined);
    message.smallBlindId !== undefined &&
      (obj.smallBlindId = message.smallBlindId);
    message.bigBlindId !== undefined && (obj.bigBlindId = message.bigBlindId);
    message.blind !== undefined && (obj.blind = message.blind);
    if (message.playerIds) {
      obj.playerIds = message.playerIds.map((e) => e);
    } else {
      obj.playerIds = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Game>): Game {
    const message = { ...baseGame } as Game;
    message.playerIds = [];
    if (object.token !== undefined && object.token !== null) {
      message.token = object.token;
    }
    if (object.adminId !== undefined && object.adminId !== null) {
      message.adminId = object.adminId;
    }
    if (object.state !== undefined && object.state !== null) {
      message.state = object.state;
    }
    if (object.pot !== undefined && object.pot !== null) {
      message.pot = object.pot;
    }
    if (object.round !== undefined && object.round !== null) {
      message.round = Round.fromPartial(object.round);
    }
    if (object.smallBlindId !== undefined && object.smallBlindId !== null) {
      message.smallBlindId = object.smallBlindId;
    }
    if (object.bigBlindId !== undefined && object.bigBlindId !== null) {
      message.bigBlindId = object.bigBlindId;
    }
    if (object.blind !== undefined && object.blind !== null) {
      message.blind = object.blind;
    }
    if (object.playerIds !== undefined && object.playerIds !== null) {
      for (const e of object.playerIds) {
        message.playerIds.push(e);
      }
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
