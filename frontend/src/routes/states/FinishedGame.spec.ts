import { render } from "@testing-library/svelte";
import { expect } from "chai";
import FinishedGame from "./FinishedGame.svelte";
import { Game_State } from "../../types/proto/game";
import { ownPlayer, players, activeGame, activities } from "../store";

describe("FinishedGame", () => {
  it("should render page", async () => {
    activeGame.update({
      adminId: "a",
      bigBlindId: "a",
      blind: 1,
      playerIds: ["a", "b"],
      pot: 1,
      smallBlindId: "b",
      state: Game_State.STATE_FINISHED,
      token: "abc",
    });
    players.update({ credits: 0, id: "a", name: "A" });
    players.update({ credits: 987, id: "b", name: "B" });
    ownPlayer.update({
      credits: 0,
      id: "a",
      name: "A",
      actions: [],
    });
    activities.update("a", false);
    activities.update("b", true);

    const { queryByText } = render(FinishedGame, {
      props: { leaveGame: () => {} },
    });

    expect(queryByText("B has won! Congratulations...")).to.exist;
  });
});
