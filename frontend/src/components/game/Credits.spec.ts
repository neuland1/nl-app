import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Credits from "./Credits.svelte";

describe("Credits", () => {
  it("should render credits", async () => {
    const { queryByText } = render(Credits, {
      props: {
        amount: 123,
      },
    });

    expect(queryByText("123")).to.exist;
    expect(queryByText("C")).to.exist;
  });
});
