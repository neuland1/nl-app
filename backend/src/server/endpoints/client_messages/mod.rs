mod auth_confirmed;
mod game_left;
mod game_started;
mod increase_blinds;
mod name_updated;
mod round_all_in;
mod round_called;
mod round_checked;
mod round_disclosed;
mod round_ended;
mod round_fold;
mod round_raised;
mod round_started;

pub use auth_confirmed::handle_auth_confirmation;
pub use game_left::handle_game_leave;
pub use game_started::handle_game_start;
pub use increase_blinds::handle_increase_blinds;
pub use name_updated::handle_name_update;
pub use round_all_in::handle_round_all_in;
pub use round_called::handle_round_call;
pub use round_checked::handle_round_checked;
pub use round_disclosed::handle_round_disclosed;
pub use round_ended::handle_round_ended;
pub use round_fold::handle_round_fold;
pub use round_raised::handle_round_raised;
pub use round_started::handle_round_started;

use crate::{
    combinations::{
        comparators::compare_hands,
        finder::{combine_hand_and_deck, determine_highest_combination},
    },
    model::{Action, Combination, Game, Player, Round, RoundState},
    server::app_context::AppContext,
};
use itertools::Itertools;
use log::info;
use std::{cmp::Ordering, collections::BTreeMap, convert::TryInto};

async fn move_to_next_player(
    ctx: &AppContext,
    game: &mut Game,
    current_player: &mut Player,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round is missing"))?;

    current_player.resolve_all_actions();
    ctx.db().players().persist(current_player).await?;

    // No other players left
    if round.active_players().len() == 1 {
        give_pot_to_only_player_and_start_next_round(game, ctx).await?;
        return Ok(());
    // ShowDown -> PostShowDown
    } else if round.state() == &RoundState::ShowDown && round.discloses_finished() {
        end_round(game, ctx).await?;
        return Ok(());
    // PostShowDown -> next Round
    } else if round.state() == &RoundState::PostShowDown {
        start_new_round(game, ctx).await?;
        return Ok(());
    }

    let next_player_id = round
        .move_to_next_player()?
        .ok_or_else(|| String::from("No next player known"))?;

    let mut next_player_has_raised = round
        .last_raise_player_id()
        .clone()
        .filter(|id| id == &next_player_id)
        .is_some();

    // check if all players have called, no raise is possible or player is already all in
    if next_player_has_raised
        || (round.can_check() && round.active_players().len() == round.checks().len())
        || (round.state() != &RoundState::ShowDown && round.all_ins().contains(&next_player_id))
    {
        round.move_to_next_state()?;
        next_player_has_raised = false;
    }

    assign_actions_to_next_player(game, next_player_has_raised, ctx).await?;

    Ok(())
}

async fn end_round(game: &mut Game, ctx: &AppContext) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round is missing"))?;
    round.move_bets_to_pot();

    determine_winners(round, ctx).await?;
    round.move_to_next_state()?;

    ctx.db().games().persist(game).await?;

    if let Some(admin_id) = game.admin_id() {
        let mut admin = ctx
            .db()
            .players()
            .get(admin_id)
            .await?
            .ok_or_else(|| String::from("Admin not found"))?;
        admin.assign_action(Action::EndRound);
        ctx.db().players().persist(&admin).await?;
    }

    Ok(())
}

fn determine_combinations(round: &mut Round) -> Vec<(String, Combination)> {
    round
        .disclosed_players()
        .iter()
        .map(|p| {
            let hand = round.assigned_cards().get(p).unwrap_or(&vec![]).clone();
            let cards = combine_hand_and_deck(&hand, round.open_cards());
            let combination = determine_highest_combination(&cards);

            (p.clone(), combination)
        })
        .sorted_by(|(_, a), (_, b)| compare_hands(&a.get_hand(), &b.get_hand()))
        .collect_vec()
}

async fn determine_winners(round: &mut Round, ctx: &AppContext) -> Result<(), String> {
    // determine best hands for each active player
    let mut combinations = determine_combinations(round);
    let mut total_pot = round.pot();

    while total_pot > 0 {
        // best combination is last combination in ordered list of combinations
        let (_, best_combination) = combinations
            .last()
            .ok_or_else(|| String::from("Should find at least one combination"))?
            .clone();

        // compare equal best combinations
        let players_with_same_combination = combinations
            .iter()
            .filter(|(_, c)| {
                compare_hands(&best_combination.get_hand(), &c.get_hand()) == Ordering::Equal
            })
            .cloned()
            .map(|(id, _)| {
                let invested = *round.total_invested().get(&id).unwrap_or(&0);
                (id, invested)
            })
            .collect::<BTreeMap<_, _>>();

        // distribute credits between winners
        let players_count: u32 = players_with_same_combination.len().try_into().unwrap();
        let (potential_pot, is_equal) = total_pot.overflowing_div(players_count);
        // TODO try to handle remaining unequal credits, discard for now
        if !is_equal {
            total_pot = players_count * potential_pot;
        }
        let mut added_credits = BTreeMap::new();
        for (player_id, invested) in &players_with_same_combination {
            // determine earned credits (all in receives maximum twice the invested amount, remaining players equal shares)
            let received_credits = if round.all_ins().contains(player_id) {
                std::cmp::min(potential_pot, invested * 2)
            } else {
                potential_pot
            };
            total_pot -= received_credits;

            added_credits.insert(String::from(player_id), received_credits);

            // remove combination, as player will receive credits
            combinations.retain(|(id, _)| id != player_id);
        }

        // if players went all in there might be credits left because of sidepot
        let players_without_all_in = players_with_same_combination
            .iter()
            .filter(|(id, _)| !round.all_ins().contains(*id))
            .map(|(id, _)| id.clone())
            .collect_vec();
        let players_without_all_in_count: u32 = players_without_all_in.len().try_into().unwrap();
        if total_pot > 0 && players_without_all_in_count > 0 {
            let (additional_credits, is_equal) =
                total_pot.overflowing_div(players_without_all_in_count);
            // TODO try to handle remaining unequal credits, discard for now
            if !is_equal {
                total_pot = players_without_all_in_count * additional_credits;
            }
            for player_id in &players_without_all_in {
                let mut received_credits = *added_credits.get(player_id).unwrap_or(&0);
                total_pot -= additional_credits;
                received_credits += additional_credits;
                added_credits.insert(String::from(player_id), received_credits);
            }
        }

        // give credits to players
        for player_id in players_with_same_combination.keys() {
            let mut player = ctx
                .db()
                .players()
                .get(player_id)
                .await?
                .ok_or_else(|| String::from("Winning player not found"))?;

            // determine earned credits (all in receives maximum twice the bet, remaining players equal shares)
            let received_credits = *added_credits.get(player_id).unwrap_or(&0);
            player.add_credits(received_credits);
            ctx.db().players().persist(&player).await?;

            info!(
                "Player {} received {} credits",
                player.id(),
                &received_credits
            );
        }

        round.set_winners_and_best_combination(
            &best_combination,
            &players_with_same_combination.keys().cloned().collect_vec(),
        );
    }

    Ok(())
}

async fn give_pot_to_only_player_and_start_next_round(
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round is missing"))?;
    let last_player_id = round
        .active_players()
        .get(0)
        .ok_or_else(|| String::from("Game is empty"))?
        .clone();
    round.move_bets_to_pot();
    let share = round.pot();

    let mut player = ctx
        .db()
        .players()
        .get(&last_player_id)
        .await?
        .ok_or_else(|| String::from("Winning player not found"))?;
    player.add_credits(share);
    ctx.db().players().persist(&player).await?;

    info!(
        "Player {} received all {} credits because he is the only left",
        player.id(),
        &share
    );

    start_new_round(game, ctx).await?;

    Ok(())
}

async fn assign_actions_to_next_player(
    game: &mut Game,
    next_player_has_raised: bool,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round is missing"))?;

    let mut next_player = ctx
        .db()
        .players()
        .get(round.current_player())
        .await?
        .ok_or_else(|| String::from("Next player not found"))?;

    if round.state() != &RoundState::ShowDown {
        if round.needed_credits_for_call(next_player.id()) < next_player.credits()
            || !round.all_ins().contains(next_player.id())
        {
            next_player.assign_action(Action::Call);
        }
        if !next_player_has_raised
            && round.blinds_have_been_done()
            && round.needed_credits_for_raise(round.bet() * 2, next_player.id())
                < next_player.credits()
            && !round.all_ins().contains(next_player.id())
        {
            next_player.assign_action(Action::Raise);
        }
        if round.blinds_have_been_done() && !round.all_ins().contains(next_player.id()) {
            next_player.assign_action(Action::Fold);
        }
        if (round.blinds_have_been_done() && round.can_check())
            || round.all_ins().contains(next_player.id())
        {
            next_player.assign_action(Action::Check);
        }
        if !round.all_ins().contains(next_player.id()) {
            next_player.assign_action(Action::AllIn);
        }
    } else {
        next_player.assign_action(Action::Fold);
        next_player.assign_action(Action::Disclose);
    }

    let (persist_player, persist_game) = tokio::join!(
        ctx.db().players().persist(&next_player),
        ctx.db().games().persist(game)
    );
    persist_player.and(persist_game)?;

    Ok(())
}

async fn start_new_round(game: &mut Game, ctx: &AppContext) -> Result<(), String> {
    check_for_failed_players(game, ctx).await?;

    if game.liquid_player_ids().len() < 2 {
        game.finish();
        ctx.db().games().persist(game).await?;

        return Ok(());
    }

    game.start_new_round();

    let mut players = ctx.db().players().get_batch(&game.player_ids()).await?;
    let players = players
        .values_mut()
        .map(|p| {
            p.resolve_all_actions();
            if p.id() == game.small_blind_id().clone().unwrap() {
                p.assign_action(Action::StartRound);
                p.assign_action(Action::IncreaseBlinds);
            }
            p.clone()
        })
        .collect_vec();
    let (persist_players, persist_game) = tokio::join!(
        ctx.db().players().persist_batch(&players),
        ctx.db().games().persist(&game)
    );
    persist_players.and(persist_game)?;

    Ok(())
}

async fn check_for_failed_players(game: &mut Game, ctx: &AppContext) -> Result<(), String> {
    let big_blind = game.blind() * 2;
    for player_id in game.player_ids().clone() {
        let player = ctx
            .db()
            .players()
            .get(&player_id)
            .await?
            .ok_or_else(|| String::from("Player not found"))?;
        if player.credits() < big_blind {
            game.fail_player(&player_id);
        }
    }
    ctx.db().games().persist(game).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::end_round;
    use crate::{
        model::{Card, CardColor, Game, Player},
        server::app_context::AppContext,
    };
    use std::collections::BTreeMap;

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_to_winner() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(900);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(900);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id())],
            &[
                Card::new(CardColor::Tiles, 9),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Pikes, 4),
                Card::new(CardColor::Pikes, 3),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Tiles, 13),
                        Card::new(CardColor::Tiles, 12),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Tiles, 13),
                        Card::new(CardColor::Hearts, 12),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 100),
                (String::from(player_a.id()), 100),
            ],
            &[String::from(admin.id()), String::from(player_a.id())],
            &[],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(
            credits_distribution.get(player_a.id()).unwrap().clone(),
            1100
        );
        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 900);
    }

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_to_only_disclosed_player() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(900);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(900);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id())],
            &[
                Card::new(CardColor::Tiles, 9),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Pikes, 4),
                Card::new(CardColor::Pikes, 3),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Tiles, 13),
                        Card::new(CardColor::Tiles, 12),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Tiles, 13),
                        Card::new(CardColor::Hearts, 12),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 100),
                (String::from(player_a.id()), 100),
            ],
            &[String::from(admin.id())],
            &[],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(
            credits_distribution.get(player_a.id()).unwrap().clone(),
            900
        );
        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 1100);
    }

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_between_both_winners() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(900);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(900);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id())],
            &[
                Card::new(CardColor::Tiles, 13),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Tiles, 12),
                Card::new(CardColor::Tiles, 9),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Hearts, 13),
                        Card::new(CardColor::Clovers, 12),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Clovers, 13),
                        Card::new(CardColor::Hearts, 12),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 100),
                (String::from(player_a.id()), 100),
            ],
            &[String::from(admin.id()), String::from(player_a.id())],
            &[],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(
            credits_distribution.get(player_a.id()).unwrap().clone(),
            1000
        );
        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 1000);
    }

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_between_winner_and_opponent_with_one_all_in() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(0);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(900);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id())],
            &[
                Card::new(CardColor::Tiles, 13),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Tiles, 12),
                Card::new(CardColor::Hearts, 3),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Hearts, 13),
                        Card::new(CardColor::Clovers, 12),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Tiles, 9),
                        Card::new(CardColor::Hearts, 12),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 300),
                (String::from(player_a.id()), 300),
                (String::from("discarded-a"), 200),
            ],
            &[String::from(admin.id()), String::from(player_a.id())],
            &[String::from(admin.id())],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(
            credits_distribution.get(player_a.id()).unwrap().clone(),
            1100
        );
        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 600);
    }

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_between_two_all_ins() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(0);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(0);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id())],
            &[
                Card::new(CardColor::Tiles, 13),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Tiles, 12),
                Card::new(CardColor::Hearts, 3),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Hearts, 13),
                        Card::new(CardColor::Clovers, 12),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Tiles, 9),
                        Card::new(CardColor::Hearts, 12),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 300),
                (String::from(player_a.id()), 300),
            ],
            &[String::from(admin.id()), String::from(player_a.id())],
            &[String::from(admin.id()), String::from(player_a.id())],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(credits_distribution.get(player_a.id()).unwrap().clone(), 0);
        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 600);
    }

    #[tokio::test]
    async fn should_end_round_and_distribute_credits_between_all_in_and_two_other_players() {
        let ctx = AppContext::init();
        let mut admin = Player::new("TOKEN");
        admin.add_credits(0);
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player failed");

        let mut player_a = Player::new("TOKEN");
        player_a.add_credits(500);
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player failed");

        let mut player_b = Player::new("TOKEN");
        player_b.add_credits(500);
        ctx.db()
            .players()
            .persist(&player_b)
            .await
            .expect("Persisting player failed");

        let credits_distribution = handle_end_round(
            admin.id(),
            &[String::from(player_a.id()), String::from(player_b.id())],
            &[
                Card::new(CardColor::Tiles, 13),
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Tiles, 12),
                Card::new(CardColor::Tiles, 9),
            ],
            &[
                (
                    String::from(player_a.id()),
                    vec![
                        Card::new(CardColor::Hearts, 13),
                        Card::new(CardColor::Clovers, 12),
                    ],
                ),
                (
                    String::from(player_b.id()),
                    vec![
                        Card::new(CardColor::Hearts, 10),
                        Card::new(CardColor::Clovers, 9),
                    ],
                ),
                (
                    String::from(admin.id()),
                    vec![
                        Card::new(CardColor::Hearts, 8),
                        Card::new(CardColor::Clovers, 7),
                    ],
                ),
            ],
            &[
                (String::from(admin.id()), 300),    // 600
                (String::from(player_a.id()), 300), // 600 + 50
                (String::from(player_b.id()), 300), // 600 + 50
                (String::from("other-c"), 250),
                (String::from("other-d"), 250),
                (String::from("other-e"), 250),
                (String::from("other-f"), 250),
            ],
            &[
                String::from(admin.id()),
                String::from(player_a.id()),
                String::from(player_b.id()),
            ],
            &[String::from(admin.id())],
            &ctx,
        )
        .await
        .unwrap();

        assert_eq!(credits_distribution.get(admin.id()).unwrap().clone(), 600);
        assert_eq!(
            credits_distribution.get(player_a.id()).unwrap().clone(),
            1149
        );
        assert_eq!(
            credits_distribution.get(player_b.id()).unwrap().clone(),
            1149
        );
    }

    async fn handle_end_round(
        admin_id: &str,
        player_ids: &[String],
        open_cards: &[Card],
        assigned_cards: &[(String, Vec<Card>)],
        investments: &[(String, u32)],
        disclosed_players: &[String],
        all_in_players: &[String],
        ctx: &AppContext,
    ) -> Result<BTreeMap<String, u32>, String> {
        let mut game = Game::new(admin_id, "TOKEN");
        for p in player_ids {
            game.add_player(p);
        }
        game.start();
        game.start_new_round();

        let round = game.round_mut().unwrap();

        for c in open_cards {
            round.open_cards_mut().push(c.clone());
        }
        for (player_id, cards) in assigned_cards {
            round
                .assigned_cards_mut()
                .insert(String::from(player_id), cards.clone());
        }
        for id in disclosed_players {
            round.disclosed_players_mut().insert(String::from(id));
        }
        let mut pot = 0;
        for (id, invested) in investments {
            pot += invested.clone();
            round
                .total_invested_mut()
                .insert(String::from(id), invested.clone());
            if all_in_players.contains(id) {
                round.all_ins_mut().insert(String::from(id));
            }
        }
        round.set_pot(pot);

        ctx.db()
            .games()
            .persist(&game)
            .await
            .expect("Persisting game failed");

        end_round(&mut game, &ctx)
            .await
            .expect("Ending round failed");

        let mut credits = BTreeMap::new();
        for p in player_ids {
            credits.insert(
                String::from(p),
                ctx.db().players().get(p).await?.unwrap().credits(),
            );
        }
        credits.insert(
            String::from(admin_id),
            ctx.db().players().get(admin_id).await?.unwrap().credits(),
        );

        Ok(credits)
    }
}
