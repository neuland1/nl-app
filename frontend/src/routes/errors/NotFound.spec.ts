import { render } from "@testing-library/svelte";
import { expect } from "chai";
import NotFound from "./NotFound.svelte";

describe("NotFound", () => {
  it("should render page", async () => {
    const { queryByText } = render(NotFound);

    expect(queryByText("Page not found :(")).to.exist;
  });
});
