import { writable } from "svelte/store";

export type NotificationMessage = {
  type: "info" | "error" | "warning";
  message: string;
  timeoutInSeconds?: number;
};

type NotificationsStore = Array<NotificationMessage & { id: string }>;

const createNotificationsStore = () => {
  const { subscribe, update } = writable<NotificationsStore>([]);

  return {
    subscribe,
    addNotification: (newNotification: NotificationMessage) => {
      const id = Math.random().toString(36).substring(7);
      const timeout = newNotification.timeoutInSeconds || 5;
      setTimeout(
        () =>
          update((notifications) => notifications.filter((n) => n.id !== id)),
        timeout * 1000
      );
      update((notifications) => [...notifications, { id, ...newNotification }]);
    },
  };
};

export const notifications = createNotificationsStore();
