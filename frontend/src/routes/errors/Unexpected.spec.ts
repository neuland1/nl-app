import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Unexpected from "./Unexpected.svelte";

describe("Unexpected", () => {
  it("should render page", async () => {
    const { queryByText } = render(Unexpected);

    expect(queryByText("Unexpected Error :(")).to.exist;
  });
});
