use std::iter::FromIterator;

use super::Action;
use crate::{
    db::Persist,
    model::proto::{self},
};
use chrono::{DateTime, Utc};
use log::warn;
use names::Generator;
use nanoid::nanoid;
use protobuf::RepeatedField;
use redis::{FromRedisValue, ToRedisArgs};
use serde::{Deserialize, Serialize};

fn generate_random_name() -> String {
    Generator::default().next().unwrap()
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Derivative)]
#[derivative(Debug)]
#[serde(rename_all = "camelCase")]
pub struct Player {
    id: String,
    name: String,
    game_token: String,
    #[derivative(Debug = "ignore")]
    user_token: String,
    creation_time: DateTime<Utc>,
    last_active_time: Option<DateTime<Utc>>,
    actions: Vec<Action>,
    credits: u32,
}

pub trait PrivateInto<T> {
    fn private_into(&self, player_id: &str) -> T;
}

impl Player {
    pub fn new(game_token: &str) -> Self {
        Player {
            id: nanoid!(),
            name: generate_random_name(),
            game_token: String::from(game_token),
            user_token: String::from(""),
            creation_time: Utc::now(),
            last_active_time: Some(Utc::now()),
            actions: vec![],
            credits: 0,
        }
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn set_name(&mut self, name: &str) {
        if !name.is_empty() {
            self.name = String::from(name);
        }
    }

    pub fn game_token(&self) -> &str {
        &self.game_token
    }

    pub fn user_token(&self) -> &str {
        &self.user_token
    }

    pub fn update_token(&mut self, new_token: &str) {
        self.user_token = String::from(new_token);
    }

    pub fn set_inactive(&mut self) {
        self.last_active_time = Some(Utc::now());
    }

    pub fn set_active(&mut self) {
        self.last_active_time = None;
    }

    pub fn last_active_time(&self) -> Option<DateTime<Utc>> {
        self.last_active_time
    }

    pub fn assign_action(&mut self, action: Action) {
        self.actions.push(action);
    }

    pub fn resolve_action(&mut self, action: Action) {
        if self.actions.iter().any(|t| *t == action) {
            self.actions = self
                .actions
                .iter()
                .filter(|a| **a != action)
                .cloned()
                .collect();
        } else {
            warn!(
                "Action {:?} not resolved, might have already been resolved before",
                action
            );
        }
    }

    pub fn resolve_all_actions(&mut self) {
        self.actions = vec![];
    }

    pub fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    pub fn substract_credits(&mut self, credits: u32) {
        self.credits -= credits;
    }

    pub fn add_credits(&mut self, credits: u32) {
        self.credits += credits;
    }

    pub fn credits(&self) -> u32 {
        self.credits
    }
}

impl Persist for Player {
    fn id(&self) -> &str {
        self.id()
    }
}

impl ToRedisArgs for Player {
    fn write_redis_args<W>(&self, out: &mut W)
    where
        W: ?Sized + redis::RedisWrite,
    {
        let bytes = bincode::serialize(&self).unwrap();
        out.write_arg(&bytes);
    }
}

impl FromRedisValue for Player {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<Self> {
        match v {
            redis::Value::Data(bytes) => bincode::deserialize(bytes).map_err(|err| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Deserializing player failed: {:?}", &err),
                )
                .into()
            }),
            res => {
                panic!("Deserialize failed: {:?}", res)
            }
        }
    }
}

impl Into<proto::player::Player> for Player {
    fn into(self) -> proto::player::Player {
        let mut player = proto::player::Player::new();
        player.set_id(self.id);
        player.set_name(self.name);
        player.set_credits(self.credits);
        player
    }
}

impl PrivateInto<proto::player::OwnPlayer> for Player {
    fn private_into(&self, player_id: &str) -> proto::player::OwnPlayer {
        if player_id != self.id() {
            panic!("Private information should be send only to same player");
        }
        let mut player = proto::player::OwnPlayer::new();
        player.set_id(String::from(self.id()));
        player.set_name(String::from(self.name()));
        player.set_actions(RepeatedField::from_iter(
            self.actions().iter().map(|a| a.clone().into()),
        ));
        player.set_credits(self.credits);
        player
    }
}

#[cfg(test)]
mod tests {
    use super::{Action, Player};

    #[tokio::test]
    async fn should_set_name() {
        let mut player = Player::new("TOKEN");
        player.set_name("abc");

        assert_eq!(player.name(), "abc");
    }

    #[tokio::test]
    async fn should_not_set_empty_name() {
        let mut player = Player::new("TOKEN");
        player.set_name("abc");
        player.set_name("");

        assert_eq!(player.name(), "abc");
    }

    #[tokio::test]
    async fn should_assign_action() {
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Call);

        assert_eq!(player.actions().len(), 1);
    }

    #[tokio::test]
    async fn should_resolve_action() {
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Call);
        player.resolve_action(Action::Call);

        assert_eq!(player.actions().len(), 0);
    }

    #[tokio::test]
    async fn should_keep_action() {
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Call);
        player.resolve_action(Action::Fold);

        assert_eq!(player.actions().len(), 1);
    }

    #[tokio::test]
    async fn should_resolve_all_actions() {
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Call);
        player.assign_action(Action::Fold);
        player.assign_action(Action::Raise);
        player.resolve_all_actions();

        assert_eq!(player.actions().len(), 0);
    }
}
