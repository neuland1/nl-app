/* eslint-disable */
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import * as Long from "long";
import { Action } from "./action";

export const protobufPackage = "";

export interface Player {
  id: string;
  name: string;
  credits: number;
}

export interface OwnPlayer {
  id: string;
  name: string;
  actions: Action[];
  credits: number;
}

const basePlayer: object = { id: "", name: "", credits: 0 };

export const Player = {
  encode(message: Player, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.credits !== 0) {
      writer.uint32(24).uint32(message.credits);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Player {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePlayer } as Player;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.credits = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Player {
    const message = { ...basePlayer } as Player;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    }
    if (object.credits !== undefined && object.credits !== null) {
      message.credits = Number(object.credits);
    }
    return message;
  },

  toJSON(message: Player): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.credits !== undefined && (obj.credits = message.credits);
    return obj;
  },

  fromPartial(object: DeepPartial<Player>): Player {
    const message = { ...basePlayer } as Player;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    if (object.credits !== undefined && object.credits !== null) {
      message.credits = object.credits;
    }
    return message;
  },
};

const baseOwnPlayer: object = { id: "", name: "", credits: 0 };

export const OwnPlayer = {
  encode(message: OwnPlayer, writer: Writer = Writer.create()): Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    for (const v of message.actions) {
      Action.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.credits !== 0) {
      writer.uint32(32).uint32(message.credits);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): OwnPlayer {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseOwnPlayer } as OwnPlayer;
    message.actions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.actions.push(Action.decode(reader, reader.uint32()));
          break;
        case 4:
          message.credits = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): OwnPlayer {
    const message = { ...baseOwnPlayer } as OwnPlayer;
    message.actions = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    }
    if (object.actions !== undefined && object.actions !== null) {
      for (const e of object.actions) {
        message.actions.push(Action.fromJSON(e));
      }
    }
    if (object.credits !== undefined && object.credits !== null) {
      message.credits = Number(object.credits);
    }
    return message;
  },

  toJSON(message: OwnPlayer): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    if (message.actions) {
      obj.actions = message.actions.map((e) =>
        e ? Action.toJSON(e) : undefined
      );
    } else {
      obj.actions = [];
    }
    message.credits !== undefined && (obj.credits = message.credits);
    return obj;
  },

  fromPartial(object: DeepPartial<OwnPlayer>): OwnPlayer {
    const message = { ...baseOwnPlayer } as OwnPlayer;
    message.actions = [];
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    if (object.actions !== undefined && object.actions !== null) {
      for (const e of object.actions) {
        message.actions.push(Action.fromPartial(e));
      }
    }
    if (object.credits !== undefined && object.credits !== null) {
      message.credits = object.credits;
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
