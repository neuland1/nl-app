# Watch
watch-fe:
	cd frontend && npm run watch
watch-be:
	cd backend && LOG_LEVEL=info cargo watch -x 'run --target-dir watch-target' -w 'src' -c -i 'src/model/proto'

# Test
test-fe:
	cd frontend && npm run test
test-be:
	cd backend && cargo tarpaulin --out Html --exclude-files src/model/proto/* --target-dir test-target --ignore-tests -- --test-threads 1
test:
	make test-fe
	make test-be
bench:
	cd backend && cargo bench

# Build
build-fe:
	cd frontend && npm run build
build-be:
	cd backend && cargo build

# Lint
lint-fe:
	cd frontend && npm run lint
lint-be:
	cd backend && cargo clippy --all-features --all --tests --examples -- -D clippy::all -D warnings
lint:
	make lint-be
	make lint-fe

# Update
update-fe:
	cd frontend && npm update
update-be:
	cd backend && cargo update
update:
	make update-be
	make update-fe

# Redis
start-redis:
	docker run --rm --name redis -p 6379:6379 -d redis:alpine
stop-redis:
	docker stop redis

# Schema
proto:
	protoc --proto_path=./schema message.proto --plugin=./frontend/node_modules/.bin/protoc-gen-ts_proto --ts_proto_opt=env=browser --ts_proto_opt=useOptionals=true --ts_proto_opt=oneof=unions --ts_proto_out=./frontend/src/types/proto message.proto
	protoc --proto_path=./schema --rust_out=./backend/src/model/proto message.proto
	protoc --proto_path=./schema --rust_out=./backend/src/model/proto game.proto
	protoc --proto_path=./schema --rust_out=./backend/src/model/proto player.proto
	protoc --proto_path=./schema --rust_out=./backend/src/model/proto card.proto
	protoc --proto_path=./schema --rust_out=./backend/src/model/proto action.proto

# Images
build-rust-image:
	cd ci/rust && docker build -t registry.gitlab.com/neuland1/nl-app:be-builder .
push-rust-image:
	docker push registry.gitlab.com/neuland1/nl-app:be-builder
run-rust-image:
	docker run --security-opt seccomp=unconfined -v /$(pwd)/backend:/tmp:Z -it registry.gitlab.com/neuland1/nl-app:be-builder