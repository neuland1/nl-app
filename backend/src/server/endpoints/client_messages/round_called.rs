use super::move_to_next_player;
use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_call(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round missing"))?;

    if !round.all_ins().contains(player.id()) {
        // big blind or subsequent calls
        let credits = if round.current_turn() == 2 && !round.blinds_have_been_done() {
            round.raise_credits(
                round.needed_credits_for_raise(round.bet() * 2, player.id()),
                player.id(),
            )
        } else {
            round.call_credits(player.id())
        };

        player.substract_credits(credits);
    }

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_round_call;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_call() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.add_credits(1000);
        player.assign_action(Action::Call);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();

        let result = handle_round_call(&mut player, &mut game, &ctx).await;
        assert!(result.is_ok());

        let game = ctx.db().games().get(game.token()).await.unwrap().unwrap();
        assert!(!game.round().unwrap().can_check());
    }
}
