use super::start_new_round;
use crate::{
    model::{Action, Player},
    server::app_context::AppContext,
};
use itertools::Itertools;

pub async fn handle_game_start(player: Option<Player>, ctx: &AppContext) -> Result<(), String> {
    if let Some(player) = player {
        let game = ctx.db().games().get(player.game_token()).await?;

        match game
            .filter(|game| game.admin_id().is_some())
            .filter(|game| player.id() == game.admin_id().as_ref().unwrap())
        {
            Some(mut game) => {
                game.start();

                let mut players = ctx.db().players().get_batch(&game.player_ids()).await?;
                let players = players
                    .values_mut()
                    .map(|p| {
                        p.resolve_action(Action::Settings);
                        // TODO make configurable
                        p.add_credits(5000);
                        p.clone()
                    })
                    .collect_vec();
                let (persist_players, persist_game) = tokio::join!(
                    ctx.db().players().persist_batch(&players),
                    ctx.db().games().persist(&game)
                );
                persist_players.and(persist_game)?;

                start_new_round(&mut game, ctx).await?;
                Ok(())
            }
            None => Err(String::from("Game not found")),
        }
    } else {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::handle_game_start;
    use crate::{
        model::{Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_start_game() {
        let ctx = AppContext::init();
        let player_a = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player_a)
            .await
            .expect("Persisting player has failed");
        let player_b = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player_b)
            .await
            .expect("Persisting player has failed");
        let mut game = Game::new(player_a.id(), "GAME");
        game.add_player(player_b.id());
        ctx.db()
            .games()
            .persist(&game)
            .await
            .expect("Persisting game failed");
        ctx.ws()
            .register_active_player(&player_a, &None, "peer_a")
            .await
            .expect("Registering player failed");
        ctx.ws()
            .register_active_player(&player_b, &None, "peer_b")
            .await
            .expect("Registering player failed");

        let reply = handle_game_start(Some(player_a), &ctx).await;
        assert!(reply.is_ok());
    }

    #[tokio::test]
    async fn should_not_start_unknown_game() {
        let ctx = AppContext::init();
        let player = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player has failed");
        ctx.ws()
            .register_active_player(&player, &None, "peer")
            .await
            .expect("Registering player failed");

        let reply = handle_game_start(Some(player), &ctx).await;
        assert!(reply.is_err());
    }

    #[tokio::test]
    async fn should_not_start_for_normal_player() {
        let ctx = AppContext::init();
        let player = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player has failed");
        let admin = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&admin)
            .await
            .expect("Persisting player has failed");
        let game = Game::new(admin.id(), "GAME");
        ctx.db()
            .games()
            .persist(&game)
            .await
            .expect("Persisting game failed");
        ctx.ws()
            .register_active_player(&player, &None, "participant_peer")
            .await
            .expect("Registering player failed");

        let reply = handle_game_start(Some(player), &ctx).await;
        assert!(reply.is_err());
    }
}
