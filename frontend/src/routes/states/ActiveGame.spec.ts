import { render } from "@testing-library/svelte";
import { expect } from "chai";
import ActiveGame from "./ActiveGame.svelte";
import { Game_State } from "../../types/proto/game";
import {
  Action_Call,
  Action_Check,
  Action_Fold,
  Action_Raise,
} from "../../types/proto/action";
import { Card_Color } from "../../types/proto/card";
import { ownPlayer, players, activeGame, activities } from "../store";

describe("ActiveGame", () => {
  it("should render page", async () => {
    activeGame.update({
      adminId: "a",
      bigBlindId: "a",
      blind: 1,
      playerIds: ["a", "b"],
      pot: 1,
      smallBlindId: "b",
      state: Game_State.STATE_STARTED,
      token: "abc",
      round: {
        bet: 10,
        assignedCards: {
          a: {
            cards: [],
            count: 2,
          },
          b: {
            count: 2,
            cards: [
              { color: Card_Color.COLOR_PIKES, value: 1 },
              { color: Card_Color.COLOR_HEARTS, value: 2 },
            ],
          },
        },
        winners: [],
        foldPlayerIds: ["b"],
        lastRaiserId: "a",
        currentPlayerId: "a",
        openCards: [
          { color: Card_Color.COLOR_CLOVERS, value: 13 },
          { color: Card_Color.COLOR_HEARTS, value: 12 },
          { color: Card_Color.COLOR_PIKES, value: 11 },
          { color: Card_Color.COLOR_TILES, value: 10 },
          { color: Card_Color.COLOR_CLOVERS, value: 9 },
        ],
        bets: {
          ["a"]: 123,
        },
      },
    });
    players.update({ credits: 123, id: "a", name: "A" });
    players.update({ credits: 987, id: "b", name: "B" });
    ownPlayer.update({
      credits: 123,
      id: "a",
      name: "A",
      actions: [
        { definition: { $case: "call", call: Action_Call } },
        { definition: { $case: "raise", raise: Action_Raise } },
        { definition: { $case: "fold", fold: Action_Fold } },
        { definition: { $case: "check", check: Action_Check } },
      ],
    });
    activities.update("a", false);
    activities.update("b", true);

    const { queryByText } = render(ActiveGame, {
      props: {
        sendWsMessage: () => {},
        leaveGame: () => {},
      },
    });

    expect(queryByText("Call")).to.exist;
    expect(queryByText("Raise")).to.exist;
    expect(queryByText("Fold")).to.exist;
    expect(queryByText("Check")).to.exist;
  });
});
