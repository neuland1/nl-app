import { getToken } from "./auth";
import { navigateTo } from "svelte-router-spa";

export const sendRequest = async <R>(
  url: string,
  method: "GET" | "POST" | "PUT",
  payload?: Record<string, string>
): Promise<R | null> => {
  try {
    const token = getToken();
    const res = await fetch(url, {
      headers: {
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
        ...(payload ? { "Content-Type": "application/json" } : {}),
      },
      body: JSON.stringify(payload),
      method,
    });
    if (!res.ok) {
      return null;
    }
    return (await res.json()) as R;
  } catch (err) {
    console.error(err);
    navigateTo("/errors/unexpected");
  }
  return null;
};
