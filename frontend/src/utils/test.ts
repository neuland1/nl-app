export const jsonOk = (
  body: Record<string, string | number>
): Promise<Response> => {
  const mockResponse = new window.Response(JSON.stringify(body), {
    status: 200,
    headers: {
      "Content-type": "application/json",
    },
  });

  return Promise.resolve(mockResponse);
};
