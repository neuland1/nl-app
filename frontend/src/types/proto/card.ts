/* eslint-disable */
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import * as Long from "long";

export const protobufPackage = "";

export interface Card {
  color: Card_Color;
  value: number;
}

export enum Card_Color {
  COLOR_UNSPECIFIED = 0,
  COLOR_HEARTS = 1,
  COLOR_TILES = 2,
  COLOR_CLOVERS = 3,
  COLOR_PIKES = 4,
  UNRECOGNIZED = -1,
}

export function card_ColorFromJSON(object: any): Card_Color {
  switch (object) {
    case 0:
    case "COLOR_UNSPECIFIED":
      return Card_Color.COLOR_UNSPECIFIED;
    case 1:
    case "COLOR_HEARTS":
      return Card_Color.COLOR_HEARTS;
    case 2:
    case "COLOR_TILES":
      return Card_Color.COLOR_TILES;
    case 3:
    case "COLOR_CLOVERS":
      return Card_Color.COLOR_CLOVERS;
    case 4:
    case "COLOR_PIKES":
      return Card_Color.COLOR_PIKES;
    case -1:
    case "UNRECOGNIZED":
    default:
      return Card_Color.UNRECOGNIZED;
  }
}

export function card_ColorToJSON(object: Card_Color): string {
  switch (object) {
    case Card_Color.COLOR_UNSPECIFIED:
      return "COLOR_UNSPECIFIED";
    case Card_Color.COLOR_HEARTS:
      return "COLOR_HEARTS";
    case Card_Color.COLOR_TILES:
      return "COLOR_TILES";
    case Card_Color.COLOR_CLOVERS:
      return "COLOR_CLOVERS";
    case Card_Color.COLOR_PIKES:
      return "COLOR_PIKES";
    default:
      return "UNKNOWN";
  }
}

const baseCard: object = { color: 0, value: 0 };

export const Card = {
  encode(message: Card, writer: Writer = Writer.create()): Writer {
    if (message.color !== 0) {
      writer.uint32(8).int32(message.color);
    }
    if (message.value !== 0) {
      writer.uint32(16).uint32(message.value);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Card {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCard } as Card;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.color = reader.int32() as any;
          break;
        case 2:
          message.value = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Card {
    const message = { ...baseCard } as Card;
    if (object.color !== undefined && object.color !== null) {
      message.color = card_ColorFromJSON(object.color);
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = Number(object.value);
    }
    return message;
  },

  toJSON(message: Card): unknown {
    const obj: any = {};
    message.color !== undefined &&
      (obj.color = card_ColorToJSON(message.color));
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<Card>): Card {
    const message = { ...baseCard } as Card;
    if (object.color !== undefined && object.color !== null) {
      message.color = object.color;
    }
    if (object.value !== undefined && object.value !== null) {
      message.value = object.value;
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
