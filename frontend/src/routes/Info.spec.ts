import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Info from "./Info.svelte";

describe("Info", () => {
  it("should render info", async () => {
    const { queryByText } = render(Info);

    expect(queryByText("Let's play")).to.exist;
  });
});
