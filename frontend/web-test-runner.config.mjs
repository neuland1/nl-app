import { defaultReporter } from "@web/test-runner";
import { junitReporter } from "@web/test-runner-junit-reporter";
import { puppeteerLauncher } from "@web/test-runner-puppeteer";
import snowpackRunner from "@snowpack/web-test-runner-plugin";

// Needed by "@snowpack/web-test-runner-plugin"
process.env.NODE_ENV = "test";

export default {
  plugins: [snowpackRunner()],
  files: ["./src/**/*.spec.ts"],
  coverage: true,
  coverageConfig: {
    exclude: ["**/*.spec.*", "**/*/_snowpack/**/*", "**/*/types/proto/**/*"],
  },
  reporters: [
    defaultReporter({ reportTestResults: true, reportTestProgress: true }),
    junitReporter({
      outputPath: "./junit.xml",
      reportLogs: true,
    }),
  ],
  browsers: [
    puppeteerLauncher({
      launchOptions: {
        headless: true,
        args: ["--no-sandbox"],
      },
    }),
  ],
};
