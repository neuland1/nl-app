use super::{card::CardColor, Card, Combination, PrivateInto};
use crate::model::proto::{self};
use itertools::Itertools;
use log::info;
use protobuf::RepeatedField;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{BTreeMap, BTreeSet, HashMap, VecDeque},
    convert::TryFrom,
    iter::FromIterator,
};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub enum RoundState {
    Start,
    PreFlop,
    Flop,
    River,
    Turn,
    ShowDown,
    PostShowDown,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Round {
    ordered_players: Vec<String>,
    fold_players: BTreeSet<String>,
    current_player: String,
    disclosed_players: BTreeSet<String>,
    assigned_cards: BTreeMap<String, Vec<Card>>,
    checks: BTreeMap<String, ()>,
    bets: BTreeMap<String, u32>,
    total_invested: BTreeMap<String, u32>,
    all_ins: BTreeSet<String>,
    open_cards: Vec<Card>,
    pot: u32,
    current_bet: u32,
    deck: VecDeque<Card>,
    current_turn: u32,
    last_raise_player_id: Option<String>,
    state: RoundState,
    small_blind: u32,
    small_blind_id: String,
    winners: Vec<String>,
    winning_combination: Option<Combination>,
}

impl Round {
    pub fn new(player_ids: Vec<String>, small_blind_id: &str, small_blind: u32) -> Self {
        if player_ids.len() > 8 {
            panic!("Rounds currently only support up to 8 players");
        }

        let ordered_players = create_ordered_players(small_blind_id, player_ids);

        Round {
            deck: Round::create_shuffled_deck(),
            assigned_cards: BTreeMap::new(),
            disclosed_players: BTreeSet::new(),
            checks: BTreeMap::new(),
            ordered_players,
            fold_players: BTreeSet::new(),
            bets: BTreeMap::new(),
            all_ins: BTreeSet::new(),
            total_invested: BTreeMap::new(),
            current_bet: small_blind,
            small_blind,
            small_blind_id: String::from(small_blind_id),
            current_player: String::from(small_blind_id),
            open_cards: Vec::new(),
            pot: 0,
            current_turn: 1,
            last_raise_player_id: None,
            state: RoundState::Start,
            winners: Vec::new(),
            winning_combination: None,
        }
    }

    fn create_shuffled_deck() -> VecDeque<Card> {
        let mut ordered_deck = vec![];
        for col in vec![
            CardColor::Pikes,
            CardColor::Hearts,
            CardColor::Clovers,
            CardColor::Tiles,
        ] {
            for i in 1..14 {
                ordered_deck.push(Card::new(col.clone(), i));
            }
        }

        let mut rng = thread_rng();
        let mut deck = VecDeque::new();
        while !ordered_deck.is_empty() {
            let next_index = rng.gen_range(0..ordered_deck.len());
            let card = ordered_deck.remove(next_index);
            deck.push_back(card);
        }

        deck
    }
    pub fn retrieve_card(&mut self) -> Option<Card> {
        self.deck.pop_front()
    }

    pub fn deck(&self) -> &VecDeque<Card> {
        &self.deck
    }

    pub fn open_cards(&self) -> &Vec<Card> {
        &self.open_cards
    }

    pub fn open_cards_mut(&mut self) -> &mut Vec<Card> {
        &mut self.open_cards
    }

    pub fn assigned_cards(&self) -> &BTreeMap<String, Vec<Card>> {
        &self.assigned_cards
    }

    pub fn assigned_cards_mut(&mut self) -> &mut BTreeMap<String, Vec<Card>> {
        &mut self.assigned_cards
    }

    pub fn disclose_cards(&mut self, player_id: &str) {
        self.disclosed_players.insert(String::from(player_id));
    }

    pub fn disclosed_players(&self) -> &BTreeSet<String> {
        &self.disclosed_players
    }

    pub fn disclosed_players_mut(&mut self) -> &mut BTreeSet<String> {
        &mut self.disclosed_players
    }

    pub fn discloses_finished(&self) -> bool {
        self.disclosed_players.len() == self.active_players().len()
    }

    pub fn active_players(&self) -> Vec<String> {
        self.ordered_players
            .iter()
            .filter(|p| !self.fold_players.contains(*p))
            .cloned()
            .collect()
    }

    pub fn current_player(&self) -> &str {
        &self.current_player
    }

    pub fn pot(&self) -> u32 {
        self.pot
    }

    pub fn set_pot(&mut self, pot: u32) {
        self.pot = pot;
    }

    pub fn open_next_card(&mut self) -> Result<(), String> {
        let new_card = self
            .deck
            .pop_front()
            .ok_or_else(|| String::from("Card missing"))?;
        self.open_cards.push(new_card);

        Ok(())
    }

    pub fn fold_player(&mut self, player_id: &str) {
        self.assigned_cards.remove(player_id);
        self.checks.remove(player_id);
        self.fold_players.insert(String::from(player_id));
    }

    pub fn has_finished(&self) -> bool {
        self.active_players().len() < 2 || self.state == RoundState::ShowDown
    }

    pub fn set_winners_and_best_combination(
        &mut self,
        combination: &Combination,
        winners: &[String],
    ) {
        // TODO consider saving all following, disclosed combinations as well to increase transparency of credits distribution
        self.winning_combination = Some(combination.clone());
        self.winners = winners.iter().cloned().collect_vec();
    }

    /// Sets the current player to the small blind or the next active player after him. It will return None in case no other player is left. In this case, the round should end.
    fn move_to_starting_player(&mut self) -> Result<(), String> {
        if self.active_players().len() == 1 {
            return Ok(());
        }

        // start show down with last raise or with player after small blind
        let next_player_index = self
            .ordered_players
            .iter()
            .position(|p| {
                p == &if self.state != RoundState::ShowDown {
                    self.small_blind_id.clone()
                } else {
                    self.last_raise_player_id
                        .clone()
                        .unwrap_or_else(|| String::from(&self.small_blind_id))
                }
            })
            .ok_or_else(|| String::from("Next player not found"))?;

        self.current_player = self
            .ordered_players
            .iter()
            .cycle()
            .skip(next_player_index)
            .find(|p| !self.fold_players.contains(*p))
            .cloned()
            .ok_or_else(|| String::from("No player found for starting a round"))?;
        self.current_turn = 1;

        Ok(())
    }

    /// Moves to the next player. It will return None in case no other player is left. In this case, the round should end.
    pub fn move_to_next_player(&mut self) -> Result<Option<String>, String> {
        if self.active_players().len() == 1 {
            self.current_player = self.active_players().get(0).unwrap().clone();
            return Ok(None);
        }

        self.current_turn += 1;
        let current_player_index = self
            .ordered_players
            .iter()
            .position(|p| p == &self.current_player)
            .ok_or_else(|| String::from("Current player not found"))?;

        let next_player_id = self
            .ordered_players
            .iter()
            .cycle()
            .skip(current_player_index + 1)
            .find(|p| !self.fold_players.contains(*p))
            .cloned()
            .ok_or_else(|| String::from("Next player not found"))?;

        self.current_player = next_player_id.clone();

        Ok(Some(next_player_id))
    }

    pub fn raise_credits(&mut self, amount: u32, player_id: &str) -> u32 {
        let needed_credits = self.needed_credits_for_raise(amount, player_id);
        self.current_bet = amount;
        self.add_bets_by_player(player_id, needed_credits);
        self.last_raise_player_id = Some(String::from(player_id));

        needed_credits
    }

    pub fn needed_credits_for_raise(&self, amount: u32, player_id: &str) -> u32 {
        amount - self.bets.get(player_id).unwrap_or(&0)
    }

    pub fn needed_credits_for_call(&self, player_id: &str) -> u32 {
        self.current_bet - self.bets.get(player_id).unwrap_or(&0)
    }

    pub fn call_credits(&mut self, player_id: &str) -> u32 {
        // TODO consider moving big blind logic here
        let needed_credits = self.needed_credits_for_call(player_id);
        self.add_bets_by_player(player_id, needed_credits);
        if self.last_raise_player_id.is_none() && self.blinds_have_been_done() {
            self.last_raise_player_id = Some(String::from(player_id));
        }

        needed_credits
    }

    pub fn all_in(&mut self, player_id: &str, player_credits: u32) {
        let complete_credits = *self.bets.get(player_id).unwrap_or(&0) + player_credits;

        // check if all in would increase current bet
        if self.current_bet < complete_credits {
            self.current_bet = complete_credits;
            self.last_raise_player_id = Some(String::from(player_id));
        }
        self.all_ins.insert(String::from(player_id));
        self.add_bets_by_player(player_id, player_credits);
    }

    pub fn all_ins(&self) -> &BTreeSet<String> {
        &self.all_ins
    }

    pub fn all_ins_mut(&mut self) -> &mut BTreeSet<String> {
        &mut self.all_ins
    }

    fn add_bets_by_player(&mut self, player_id: &str, credits: u32) {
        let mut existing_credits = *self.bets.get(player_id).unwrap_or(&0);
        existing_credits += credits;
        self.bets.insert(String::from(player_id), existing_credits);
        let mut invested_credits = *self.total_invested.get(player_id).unwrap_or(&0);
        invested_credits += credits;
        self.total_invested
            .insert(String::from(player_id), invested_credits);
    }

    pub fn bets(&self) -> &BTreeMap<String, u32> {
        &self.bets
    }

    pub fn bets_mut(&mut self) -> &mut BTreeMap<String, u32> {
        &mut self.bets
    }

    pub fn total_invested(&self) -> &BTreeMap<String, u32> {
        &self.total_invested
    }

    pub fn total_invested_mut(&mut self) -> &mut BTreeMap<String, u32> {
        &mut self.total_invested
    }

    pub fn bet(&self) -> u32 {
        self.current_bet
    }

    pub fn last_raise_player_id(&self) -> &Option<String> {
        &self.last_raise_player_id
    }

    pub fn current_turn(&self) -> u32 {
        self.current_turn
    }

    pub fn blinds_have_been_done(&self) -> bool {
        self.current_turn > 2 || self.state != RoundState::PreFlop
    }

    pub fn state(&self) -> &RoundState {
        &self.state
    }

    pub fn can_check(&self) -> bool {
        self.bets.is_empty()
    }

    pub fn checks(&self) -> &BTreeMap<String, ()> {
        &self.checks
    }

    pub fn check(&mut self, player_id: &str) {
        self.checks.insert(String::from(player_id), ());
    }

    pub fn move_to_next_state(&mut self) -> Result<(), String> {
        self.move_bets_to_pot();

        let new_state = match self.state {
            RoundState::Start => {
                for p in &self.active_players() {
                    self.assigned_cards.insert(
                        p.clone(),
                        vec![
                            self.deck.pop_front().unwrap(),
                            self.deck.pop_front().unwrap(),
                        ],
                    );
                }
                Ok(RoundState::PreFlop)
            }
            RoundState::PreFlop => {
                self.open_next_card()?;
                self.open_next_card()?;
                self.open_next_card()?;
                self.reset_current_bet();
                Ok(RoundState::Flop)
            }
            RoundState::Flop => {
                self.open_next_card()?;
                self.reset_current_bet();
                Ok(RoundState::River)
            }
            RoundState::River => {
                self.open_next_card()?;
                self.reset_current_bet();
                Ok(RoundState::Turn)
            }
            RoundState::Turn => Ok(RoundState::ShowDown),
            RoundState::ShowDown => {
                self.pot = 0;
                Ok(RoundState::PostShowDown)
            }
            RoundState::PostShowDown => Err(String::from("Round has ended")),
        }?;
        self.state = new_state;

        info!("Round moved to state {:?}", &self.state);

        self.move_to_starting_player()?;
        self.last_raise_player_id = None;
        self.checks = BTreeMap::new();

        Ok(())
    }

    fn reset_current_bet(&mut self) {
        self.current_bet = self.small_blind * 2;
    }

    pub fn move_bets_to_pot(&mut self) {
        self.pot += self.bets.iter().map(|(_, credits)| *credits).sum::<u32>();
        self.bets.clear();
    }
}

/// create list of ordered players starting with small blind
fn create_ordered_players(small_blind_id: &str, player_ids: Vec<String>) -> Vec<String> {
    let mut ordered_players = Vec::new();
    ordered_players.push(String::from(small_blind_id));
    let small_blind_pos = player_ids
        .iter()
        .position(|p| p == small_blind_id)
        .expect("Small blind missing in players");
    for p in player_ids
        .iter()
        .cycle()
        .skip(small_blind_pos + 1)
        .take_while(|p| *p != small_blind_id)
        .cloned()
    {
        ordered_players.push(p);
    }
    ordered_players
}

impl PrivateInto<proto::game::Round> for Round {
    fn private_into(&self, target_player_id: &str) -> proto::game::Round {
        let mut proto_round = proto::game::Round::new();
        proto_round.set_open_cards(RepeatedField::from_iter(
            self.open_cards().iter().map(|a| a.clone().into()),
        ));
        proto_round
            .set_fold_player_ids(RepeatedField::from_iter(self.fold_players.iter().cloned()));
        let mut assigned_cards = HashMap::new();
        for (player_id, player_cards) in self.assigned_cards() {
            let mut cards_wrapper = proto::game::Cards::new();
            cards_wrapper.set_count(u32::try_from(player_cards.len()).unwrap());
            if target_player_id == player_id || self.disclosed_players().contains(player_id) {
                cards_wrapper.set_cards(RepeatedField::from_iter(
                    player_cards.iter().map(|a| a.clone().into()),
                ));
            }
            assigned_cards.insert(String::from(player_id), cards_wrapper);
        }
        proto_round.set_assigned_cards(assigned_cards);
        proto_round.set_current_player_id(String::from(self.current_player()));
        let mut bets = HashMap::new();
        for (player_id, credits) in self.bets() {
            bets.insert(String::from(player_id), *credits);
        }
        proto_round.set_bets(bets);
        proto_round.set_bet(self.current_bet);
        if let Some(id) = self.last_raise_player_id() {
            proto_round.set_last_raiser_id(String::from(id));
        }
        if let Some(combination) = &self.winning_combination {
            proto_round.set_winning_combination(combination.clone().into());
        }
        proto_round.set_winners(RepeatedField::from_iter(self.winners.iter().cloned()));

        proto_round
    }
}

#[cfg(test)]
mod tests {
    use super::Round;

    #[tokio::test]
    async fn should_shuffle_deck() {
        let deck = Round::create_shuffled_deck();
        assert_eq!(deck.len(), 52);
    }

    #[tokio::test]
    async fn should_set_no_next_player() {
        let mut round = Round::new(vec![String::from("a")], "a", 10);
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 1);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert!(next_player.unwrap().is_none());
    }

    #[tokio::test]
    async fn should_set_next_player() {
        let mut round = Round::new(
            vec![String::from("a"), String::from("b"), String::from("c")],
            "a",
            10,
        );
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 3);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "b");
    }

    #[tokio::test]
    async fn should_set_next_player_with_two_players() {
        let mut round = Round::new(vec![String::from("a"), String::from("b")], "a", 10);
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 2);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "b");
    }

    #[tokio::test]
    async fn should_set_next_player_with_two_players_and_small_blind_second() {
        let mut round = Round::new(vec![String::from("a"), String::from("b")], "b", 10);
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 2);
        assert_eq!(round.active_players().get(0).unwrap(), "b");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "a");
        assert_eq!(round.current_player(), "a");
    }

    #[tokio::test]
    async fn should_set_next_player_in_cycle() {
        let mut round = Round::new(
            vec![String::from("b"), String::from("c"), String::from("a")],
            "a",
            10,
        );
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 3);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "b");
        assert_eq!(round.current_player(), "b");
    }

    #[tokio::test]
    async fn should_set_next_player_in_cycle_after_fold() {
        let mut round = Round::new(
            vec![String::from("a"), String::from("b"), String::from("c")],
            "a",
            10,
        );
        let _ = round.move_to_next_player();
        let _ = round.move_to_next_player();
        round.fold_player("c");
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 2);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert_eq!(round.current_player(), "a");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "a");
    }

    #[tokio::test]
    async fn should_set_next_player_after_fold() {
        let mut round = Round::new(
            vec![String::from("a"), String::from("b"), String::from("c")],
            "a",
            10,
        );
        let _ = round.move_to_next_player();
        round.fold_player("b");
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 2);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert_eq!(round.current_player(), "c");
        assert!(next_player.clone().unwrap().is_some());
        assert_eq!(next_player.unwrap().unwrap(), "c");
    }

    #[tokio::test]
    async fn should_not_set_next_player_after_folds() {
        let mut round = Round::new(
            vec![String::from("a"), String::from("b"), String::from("c")],
            "a",
            10,
        );
        let _ = round.move_to_next_player();
        round.fold_player("b");
        let _ = round.move_to_next_player();
        round.fold_player("c");
        let next_player = round.move_to_next_player();

        assert_eq!(round.active_players().len(), 1);
        assert_eq!(round.active_players().get(0).unwrap(), "a");
        assert_eq!(round.current_player(), "a");
        assert!(next_player.clone().unwrap().is_none());
    }
}
