use super::finder::determine_highest_combination;
use crate::model::{Card, Combination};
use std::cmp::Ordering;

/// Compares two cards. Aces have the highest value, two's the lowest. Equal card values are ordered by color.
pub fn compare_cards(a: &Card, b: &Card) -> Ordering {
    match a.value().cmp(&b.value()) {
        Ordering::Equal => a.color().value().cmp(&b.color().value()),
        unequal_orderings => {
            if a.value() == 1 {
                return Ordering::Greater;
            } else if b.value() == 1 {
                return Ordering::Less;
            }

            unequal_orderings
        }
    }
}

/// Compares two cards. Aces have the highest value, two's the lowest.
pub fn compare_cards_without_color(a: &Card, b: &Card) -> Ordering {
    if a.value() == 1 && b.value() == 1 {
        return Ordering::Equal;
    } else if a.value() == 1 {
        return Ordering::Greater;
    } else if b.value() == 1 {
        return Ordering::Less;
    }

    a.value().cmp(&b.value())
}

/// Compares two combinations.
pub fn compare_combinations(a: &Combination, b: &Combination) -> Ordering {
    match a.get_value().cmp(&b.get_value()) {
        Ordering::Equal => {
            match a {
                Combination::HighCard { card: card_a, .. } => {
                    if let Combination::HighCard { card: card_b, .. } = b {
                        return compare_cards_without_color(card_a, card_b);
                    }
                }
                Combination::Pair { cards: cards_a, .. } => {
                    if let Combination::Pair { cards: cards_b, .. } = b {
                        return compare_cards_without_color(&cards_a.0, &cards_b.0);
                    }
                }
                Combination::TwoPair {
                    a: a_pair_a,
                    b: a_pair_b,
                    ..
                } => {
                    if let Combination::TwoPair {
                        a: b_pair_a,
                        b: b_pair_b,
                        ..
                    } = b
                    {
                        return match compare_cards_without_color(&a_pair_b.0, &b_pair_b.0) {
                            Ordering::Equal => {
                                compare_cards_without_color(&a_pair_a.0, &b_pair_a.0)
                            }
                            unequal_orderings => unequal_orderings,
                        };
                    }
                }
                Combination::ThreeKind { cards: cards_a, .. } => {
                    if let Combination::ThreeKind { cards: cards_b, .. } = b {
                        return compare_cards_without_color(&cards_a.0, &cards_b.0);
                    }
                }
                Combination::Straight { cards: cards_a } => {
                    if let Combination::Straight { cards: cards_b } = b {
                        let a_has_ace = cards_a.4.value() == 1;
                        let b_has_ace = cards_b.4.value() == 1;
                        if a_has_ace && b_has_ace {
                            return cards_a.3.value().cmp(&cards_b.3.value());
                        } else if (a_has_ace && cards_a.3.value() == 13)
                            || (b_has_ace && cards_b.0.value() == 2)
                        {
                            return Ordering::Greater;
                        } else if (b_has_ace && cards_b.3.value() == 13)
                            || (a_has_ace && cards_a.0.value() == 2)
                        {
                            return Ordering::Less;
                        }
                        return compare_cards_without_color(&cards_a.4, &cards_b.4);
                    }
                }
                Combination::Flush { cards: cards_a } => {
                    if let Combination::Flush { cards: cards_b } = b {
                        return compare_all_cards_without_color(
                            &[
                                cards_a.0.clone(),
                                cards_a.1.clone(),
                                cards_a.2.clone(),
                                cards_a.3.clone(),
                                cards_a.4.clone(),
                            ],
                            &[
                                cards_b.0.clone(),
                                cards_b.1.clone(),
                                cards_b.2.clone(),
                                cards_b.3.clone(),
                                cards_b.4.clone(),
                            ],
                        );
                    }
                }
                Combination::FullHouse {
                    parents: parents_a,
                    children: children_a,
                } => {
                    if let Combination::FullHouse {
                        parents: parents_b,
                        children: children_b,
                    } = b
                    {
                        return match compare_cards_without_color(&children_a.0, &children_b.0) {
                            Ordering::Equal => {
                                compare_cards_without_color(&parents_a.0, &parents_b.0)
                            }
                            unequal_ordering => unequal_ordering,
                        };
                    }
                }
                Combination::FourKind { cards: cards_a, .. } => {
                    if let Combination::FourKind { cards: cards_b, .. } = b {
                        return compare_cards_without_color(&cards_a.0, &cards_b.0);
                    }
                }
                Combination::StraightFlush { cards: cards_a } => {
                    if let Combination::StraightFlush { cards: cards_b } = b {
                        let a_has_ace = cards_a.4.value() == 1;
                        let b_has_ace = cards_b.4.value() == 1;
                        if a_has_ace && b_has_ace {
                            return cards_a.3.value().cmp(&cards_b.3.value());
                        } else if (a_has_ace && cards_a.3.value() == 13)
                            || (b_has_ace && cards_b.0.value() == 2)
                        {
                            return Ordering::Greater;
                        } else if (b_has_ace && cards_b.3.value() == 13)
                            || (a_has_ace && cards_a.0.value() == 2)
                        {
                            return Ordering::Less;
                        }
                        return compare_cards_without_color(&cards_a.4, &cards_b.4);
                    }
                }
                Combination::RoyalFlush { .. } => {}
            }

            Ordering::Equal
        }
        unequal_orderings => unequal_orderings,
    }
}

pub fn compare_hands(hand_a: &[Card], hand_b: &[Card]) -> Ordering {
    if hand_a.len() != 5 || hand_b.len() != 5 {
        panic!("Hands need to have five cards");
    }

    let combination_a = determine_highest_combination(hand_a);
    let combination_b = determine_highest_combination(hand_b);

    match compare_combinations(&combination_a, &combination_b) {
        Ordering::Equal => {
            match combination_a {
                Combination::HighCard {
                    remaining: remaining_a,
                    ..
                } => {
                    // next highest card
                    if let Combination::HighCard {
                        remaining: remaining_b,
                        ..
                    } = combination_b
                    {
                        return compare_all_cards_without_color(
                            &[remaining_a.0, remaining_a.1, remaining_a.2, remaining_a.3],
                            &[remaining_b.0, remaining_b.1, remaining_b.2, remaining_b.3],
                        );
                    }
                    Ordering::Equal
                }
                Combination::Pair {
                    remaining: remaining_a,
                    ..
                } => {
                    // next highest card
                    if let Combination::Pair {
                        remaining: remaining_b,
                        ..
                    } = combination_b
                    {
                        return compare_all_cards_without_color(
                            &[remaining_a.0, remaining_a.1, remaining_a.2],
                            &[remaining_b.0, remaining_b.1, remaining_b.2],
                        );
                    }
                    Ordering::Equal
                }
                Combination::TwoPair {
                    remaining: remaining_a,
                    ..
                } => {
                    // next (remaining) highest card
                    if let Combination::TwoPair {
                        remaining: remaining_b,
                        ..
                    } = combination_b
                    {
                        return compare_cards_without_color(&remaining_a, &remaining_b);
                    }
                    Ordering::Equal
                }
                Combination::ThreeKind {
                    remaining: remaining_a,
                    ..
                } => {
                    // next highest card
                    if let Combination::ThreeKind {
                        remaining: remaining_b,
                        ..
                    } = combination_b
                    {
                        return compare_all_cards_without_color(
                            &[remaining_a.0, remaining_a.1],
                            &[remaining_b.0, remaining_b.1],
                        );
                    }
                    Ordering::Equal
                }
                Combination::FourKind {
                    remaining: remaining_a,
                    ..
                } => {
                    // next highest card
                    if let Combination::FourKind {
                        remaining: remaining_b,
                        ..
                    } = combination_b
                    {
                        return compare_cards_without_color(&remaining_a, &remaining_b);
                    }
                    Ordering::Equal
                }
                _ => {
                    // other combinations are unique and already compared without kickers
                    Ordering::Equal
                }
            }
        }
        unequal_ordering => unequal_ordering,
    }
}

fn compare_all_cards_without_color(cards_a: &[Card], cards_b: &[Card]) -> Ordering {
    if cards_a.len() != cards_b.len() {
        panic!("Parts should have same length");
    }

    for i in 0..cards_a.len() {
        let x = cards_a.len() - i - 1;
        let res = compare_cards_without_color(&cards_a[x], &cards_b[x]);
        if res != Ordering::Equal {
            return res;
        }
    }

    Ordering::Equal
}

#[cfg(test)]
mod tests {
    use super::compare_hands;
    use crate::model::{Card, CardColor};
    use std::cmp::Ordering;

    #[test]
    #[should_panic(expected = "Hands need to have five cards")]
    fn should_panic_for_invalid_hands() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![Card::new(CardColor::Clovers, 1)];
        compare_hands(&cards_a, &cards_b);
    }

    #[test]
    fn should_compare_equal_high_cards() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_high_cards_with_kicker() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 10),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_pairs() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_pairs_with_kicker() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Clovers, 6),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_triples() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_triples_with_kicker() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Clovers, 6),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_two_pairs() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_two_pairs_with_kicker() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 7),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_straights() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_straights_with_highest_card() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_flushs() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 11),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 11),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_flushs_with_highest_card() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 11),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 12),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_full_houses() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Pikes, 4),
            Card::new(CardColor::Hearts, 4),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Pikes, 4),
            Card::new(CardColor::Hearts, 4),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_equal_full_houses_with_higher_triplet() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 2),
            Card::new(CardColor::Pikes, 4),
            Card::new(CardColor::Hearts, 4),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Pikes, 4),
            Card::new(CardColor::Hearts, 4),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_full_houses_with_higher_pair() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 2),
            Card::new(CardColor::Pikes, 4),
            Card::new(CardColor::Hearts, 4),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 2),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Hearts, 7),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_four_of_a_kind() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_four_of_a_kind_with_kicker() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Tiles, 1),
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 11),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_straight_flushs() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }

    #[test]
    fn should_compare_straight_flushs_with_higher_card() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
            Card::new(CardColor::Clovers, 7),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_straight_flushs_with_higher_card_and_ace() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Clovers, 6),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_straight_flushs_with_aces() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 1),
            Card::new(CardColor::Clovers, 2),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Clovers, 4),
            Card::new(CardColor::Clovers, 5),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 10),
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Clovers, 12),
            Card::new(CardColor::Clovers, 13),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Less);
    }

    #[test]
    fn should_compare_equal_royal_flushs() {
        let cards_a = vec![
            Card::new(CardColor::Clovers, 10),
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Clovers, 12),
            Card::new(CardColor::Clovers, 13),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards_b = vec![
            Card::new(CardColor::Clovers, 10),
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Clovers, 12),
            Card::new(CardColor::Clovers, 13),
            Card::new(CardColor::Clovers, 1),
        ];
        let cards = compare_hands(&cards_a, &cards_b);
        assert_eq!(cards, Ordering::Equal);
    }
}
