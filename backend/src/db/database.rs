use super::{Command, Persist, ScanFunction};
use crate::config::AppConfig;
use log::{debug, error, info};
use redis::{cmd, Commands, RedisError};
use std::collections::HashMap;
use tokio::sync::{
    mpsc::{self},
    oneshot::{self},
};

pub struct Database<T: Persist> {
    path: String,
    connection: redis::Connection,
    receiver: mpsc::Receiver<Command<T>>,
}

impl<T: Persist> Database<T> {
    pub fn init(path: &str, config: &AppConfig) -> (Database<T>, mpsc::Sender<Command<T>>) {
        let client = redis::Client::open(format!("redis://{}/", &config.redis_host))
            .expect("Initializing redis client failed");
        let connection = client.get_connection().expect("Connection to redis failed");

        let (sender, receiver): (mpsc::Sender<Command<T>>, mpsc::Receiver<Command<T>>) =
            mpsc::channel(256);

        let repo = Database {
            connection,
            path: String::from(path),
            receiver,
        };

        (repo, sender)
    }

    /// A database connection is etablished by creating a database instance, which should should then be started in a separate thread.
    /// The communication between resources and the database is established with channels. Simply use a client to send messages to the database thread in an easy accesible way.
    /// Of course it's also possible to send messages through the channel directly without using the client.
    ///
    /// Example:
    /// ```
    /// use neuland::{model::Game, config::AppConfig, db::{Database, Client, Command}};
    /// use envconfig::Envconfig;
    /// let config = AppConfig::init_from_env().expect("Loading server config failed");
    /// let (mut repo, sender): (Database<Game>, tokio::sync::mpsc::Sender<Command<Game>>) = Database::init("test", &config);
    /// std::thread::spawn(move || {
    ///     repo.start_listening();
    /// });
    /// let client = Client::new(sender);
    /// ```
    pub async fn start_listening(&mut self) {
        info!("Database for \"{}\" ready", self.path);

        while let Some(cmd) = self.receiver.recv().await {
            debug!("Received query: {:?}", cmd);

            match cmd {
                Command::Get { key, data } => {
                    let res = self.get(&key);
                    self.send_result(res, data.responder);
                }
                Command::GetBatch { keys, data } => {
                    let res = self.get_batch(&keys);
                    self.send_result(res, data.responder);
                }
                Command::Persist { value, data } => {
                    let res = self.persist(value);
                    self.send_result(res, data.responder);
                }
                Command::PersistBatch { values, data } => {
                    let res = self.persist_batch(&values);
                    self.send_result(res, data.responder);
                }
                Command::Remove { key, data } => {
                    let res = self.remove(&key);
                    self.send_result(res, data.responder);
                }
                Command::RemoveBatch { keys, data } => {
                    let res = self.remove_batch(&keys);
                    self.send_result(res, data.responder);
                }
                Command::Count { data } => {
                    let res = self.total_count();
                    self.send_result(res, data.responder);
                }
                Command::Scan {
                    scan_function,
                    data,
                } => {
                    let res = self.scan(scan_function);
                    self.send_result(res, data.responder);
                }
                Command::Purge { data } => {
                    self.purge();
                    self.send_result(Ok(()), data.responder);
                }
            }
        }
    }

    fn persist(&mut self, elem: T) -> Result<(), RedisError> {
        self.connection
            .set(self.build_full_key(elem.id()), elem.clone())
    }

    fn persist_batch(&mut self, values: &[T]) -> Result<(), RedisError> {
        for elem in values {
            self.persist(elem.clone())?;
        }

        Ok(())
    }

    fn remove(&mut self, key: &str) -> Result<(), RedisError> {
        self.connection.del(self.build_full_key(key))
    }

    fn remove_batch(&mut self, keys: &[String]) -> Result<(), RedisError> {
        for key in keys {
            self.connection.del(self.build_full_key(key))?;
        }

        Ok(())
    }

    fn get(&mut self, key: &str) -> Result<Option<T>, RedisError> {
        let full_key = self.build_full_key(key);
        self.get_with_full_key(&full_key)
    }

    fn get_with_full_key(&mut self, full_key: &str) -> Result<Option<T>, RedisError> {
        match self.connection.exists(full_key)? {
            true => {
                let res = self.connection.get(full_key)?;
                Ok(Some(res))
            }
            false => Ok(None),
        }
    }

    fn get_batch(&mut self, keys: &[String]) -> Result<HashMap<String, T>, RedisError> {
        let mut result = HashMap::default();

        for key in keys {
            if let Some(res) = self.get(key)? {
                result.insert(String::from(key), res);
            }
        }

        Ok(result)
    }

    fn scan(&mut self, scan_function: ScanFunction<T>) -> Result<Vec<String>, RedisError> {
        let mut results = vec![];
        // TODO consider alternatives as KEYS is a full scan
        let full_keys: Vec<String> = self.connection.keys(self.build_full_key("*"))?;
        for full_key in full_keys.iter() {
            if let Some(item) = self
                .get_with_full_key(full_key)?
                .filter(|item| scan_function(item))
            {
                results.push(String::from(item.id()));
            }
        }

        Ok(results)
    }

    fn total_count(&mut self) -> Result<usize, RedisError> {
        // TODO consider alternatives as KEYS is a full scan
        let keys: Vec<String> = self.connection.keys(self.build_full_key("*"))?;
        Ok(keys.len())
    }

    #[inline]
    fn send_result<R>(&self, data: R, sender: oneshot::Sender<R>) {
        if sender.send(data).is_err() {
            error!("Sending result to client has failed");
        }
    }

    fn purge(&mut self) {
        info!("Purged database \"{}\"", self.path);

        cmd("FLUSHDB").execute(&mut self.connection);
    }

    /// Builds a redis keys with the pattern `path:id`, for example `games:12NH_199MASdZ`. This method can also be used for patterns like `games:*`.
    fn build_full_key(&self, key: &str) -> String {
        format!("{}:{}", &self.path, key)
    }
}
