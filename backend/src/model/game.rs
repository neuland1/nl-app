use std::iter::FromIterator;

use super::{player::PrivateInto, Round};
use crate::{
    db::Persist,
    model::proto::{self},
};
use chrono::{DateTime, Utc};
use itertools::Itertools;
use log::info;
use protobuf::RepeatedField;
use redis::{FromRedisValue, ToRedisArgs};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub enum GameState {
    Initialized,
    Abandoned,
    Started,
    Finished,
}

impl Into<proto::game::Game_State> for GameState {
    fn into(self) -> proto::game::Game_State {
        match self {
            GameState::Initialized => proto::game::Game_State::STATE_INITIALIZED,
            GameState::Abandoned => proto::game::Game_State::STATE_ABANDONED,
            GameState::Started => proto::game::Game_State::STATE_STARTED,
            GameState::Finished => proto::game::Game_State::STATE_FINISHED,
        }
    }
}

/// This struct defines a game session. Each valid game needs to have an admin who is responsible for defining game settings.
/// The admin is also a player but currently not added redundantly to player_ids as well as admin_id.
///
/// Please note that each method here only mutates the struct state but still needs to be persisted to the database separately.
///
/// Example:
/// ```no_run
/// use neuland::{model::Game, config::AppConfig, db::{Database, Client, Command}};
/// use envconfig::Envconfig;
/// let config = AppConfig::init_from_env().expect("Loading server config failed");
/// let (mut repo, sender): (Database<Game>, tokio::sync::mpsc::Sender<Command<Game>>) = Database::init("test", &config);
/// std::thread::spawn(move || {
///     repo.start_listening();
/// });
/// std::thread::spawn(move || {
///     async move {
///         let client = Client::new(sender.clone());
///         let mut game = Game::new("admin", "GAME");
///         game.start();
///         let _ = client.persist(&game).await;
///     }
/// });
/// ```
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Game {
    token: String,
    creation_time: DateTime<Utc>,
    last_action_time: DateTime<Utc>,
    admin_id: Option<String>,
    player_ids: Vec<String>,
    failed_player_ids: Vec<String>,
    state: GameState,
    blind: u32,
    small_blind_id: Option<String>,
    big_blind_id: Option<String>,
    round: Option<Round>,
    round_index: u32,
}

impl Game {
    pub fn new(admin_id: &str, token: &str) -> Self {
        if token.to_uppercase() != token {
            panic!("Only uppercase characters allowed");
        }
        Game {
            token: String::from(token).to_uppercase(),
            creation_time: Utc::now(),
            last_action_time: Utc::now(),
            admin_id: Some(String::from(admin_id)),
            player_ids: vec![String::from(admin_id)],
            failed_player_ids: Vec::new(),
            state: GameState::Initialized,
            blind: 10,
            small_blind_id: None,
            big_blind_id: None,
            round: None,
            round_index: 0,
        }
    }

    pub fn token(&self) -> &str {
        &self.token
    }

    pub fn admin_id(&self) -> &Option<String> {
        &self.admin_id
    }

    pub fn big_blind_id(&self) -> &Option<String> {
        &self.big_blind_id
    }

    pub fn small_blind_id(&self) -> &Option<String> {
        &self.small_blind_id
    }

    pub fn player_ids(&self) -> &Vec<String> {
        &self.player_ids
    }

    pub fn blind(&self) -> u32 {
        self.blind
    }

    pub fn liquid_player_ids(&self) -> Vec<String> {
        self.player_ids
            .iter()
            .cloned()
            .filter(|id| !self.failed_player_ids.contains(id))
            .collect_vec()
    }

    pub fn last_action_time(&self) -> &DateTime<Utc> {
        &self.last_action_time
    }

    pub fn state(&self) -> &GameState {
        &self.state
    }

    pub fn add_player(&mut self, player_id: &str) -> bool {
        if self.player_ids.len() == 8 {
            return false;
        }
        if self.admin_id.is_none() {
            self.admin_id = Some(String::from(player_id));
        }
        self.player_ids.push(String::from(player_id));
        self.state = GameState::Initialized;
        true
    }

    pub fn remove_player(&mut self, player_id: &str) {
        self.player_ids.retain(|p| p != player_id);
        if self
            .admin_id
            .to_owned()
            .filter(|id| id == player_id)
            .is_some()
        {
            if let Some(next_player_id) = self.player_ids.get(0).map(String::from) {
                self.admin_id = Some(String::from(&next_player_id));
            } else {
                self.admin_id = None;
                self.state = GameState::Abandoned
            }
        }
    }

    pub fn fail_player(&mut self, player_id: &str) {
        self.failed_player_ids.push(String::from(player_id));

        info!("Player {} has lost", player_id);
    }

    pub fn start(&mut self) {
        self.state = GameState::Started;
    }

    pub fn finish(&mut self) {
        self.state = GameState::Finished;
    }

    pub fn start_new_round(&mut self) {
        self.move_blinds_roles();
        self.round = Some(Round::new(
            self.liquid_player_ids(),
            &self.small_blind_id.clone().unwrap(),
            self.blind,
        ));
        self.round_index += 1;
        info!(
            "Game {} started new round {}",
            &self.token, self.round_index
        );
    }

    pub fn restart_round(&mut self) {
        self.round = Some(Round::new(
            self.liquid_player_ids(),
            &self.small_blind_id.clone().unwrap(),
            self.blind,
        ));
        info!(
            "Game {} restarted same round {}",
            &self.token, self.round_index
        );
    }

    fn move_blinds_roles(&mut self) {
        let players_count = self.liquid_player_ids().len();
        if players_count < 2 {
            return;
        }

        let current_small_blind_pos = self
            .small_blind_id
            .clone()
            .and_then(|id| self.liquid_player_ids().iter().position(|r| r == &id));
        match current_small_blind_pos {
            Some(small_blind_pos) => {
                if small_blind_pos >= players_count - 1 {
                    self.small_blind_id = Some(self.liquid_player_ids().get(0).unwrap().clone());
                    self.big_blind_id = Some(self.liquid_player_ids().get(1).unwrap().clone());
                } else {
                    self.small_blind_id = Some(
                        self.liquid_player_ids()
                            .get(small_blind_pos + 1)
                            .unwrap()
                            .clone(),
                    );
                    if small_blind_pos + 1 >= players_count - 1 {
                        self.big_blind_id = Some(self.liquid_player_ids().get(0).unwrap().clone());
                    } else {
                        self.big_blind_id = Some(
                            self.liquid_player_ids()
                                .get(small_blind_pos + 2)
                                .unwrap()
                                .clone(),
                        );
                    }
                }
            }
            None => {
                self.small_blind_id = Some(self.liquid_player_ids().get(0).unwrap().clone());
                self.big_blind_id = Some(self.liquid_player_ids().get(1).unwrap().clone());
            }
        }
    }

    pub fn set_blinds(&mut self, new_small_blind: u32) -> Result<(), String> {
        if new_small_blind < self.blind {
            return Err(String::from("Small blind is not high enough"));
        }
        self.blind = new_small_blind;

        Ok(())
    }

    pub fn round_mut(&mut self) -> Option<&mut Round> {
        self.round.as_mut()
    }

    pub fn round(&self) -> Option<&Round> {
        self.round.as_ref()
    }
}

impl Persist for Game {
    fn id(&self) -> &str {
        self.token()
    }
}

impl ToRedisArgs for Game {
    fn write_redis_args<W>(&self, out: &mut W)
    where
        W: ?Sized + redis::RedisWrite,
    {
        let bytes = bincode::serialize(&self).unwrap();
        out.write_arg(&bytes);
    }
}

impl FromRedisValue for Game {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<Self> {
        match v {
            redis::Value::Data(bytes) => bincode::deserialize(bytes).map_err(|err| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Deserializing game failed: {:?}", &err),
                )
                .into()
            }),
            res => {
                panic!("Deserialize failed: {:?}", res)
            }
        }
    }
}

fn create_proto_game(game: &Game, target_player_id: &str) -> proto::game::Game {
    let mut proto_game = proto::game::Game::new();
    proto_game.set_token(String::from(game.token()));
    if let Some(id) = game.admin_id() {
        proto_game.set_admin_id(String::from(id));
    }
    proto_game.set_state(game.state().clone().into());
    if let Some(big_blind_id) = game.big_blind_id() {
        proto_game.set_big_blind_id(big_blind_id.clone());
    }
    if let Some(small_blind_id) = game.small_blind_id() {
        proto_game.set_small_blind_id(small_blind_id.clone());
    }
    proto_game.set_blind(game.blind);
    if let Some(active_round) = game.round() {
        proto_game.set_round(active_round.clone().private_into(target_player_id));
    }
    proto_game.set_player_ids(RepeatedField::from_iter(game.player_ids().iter().cloned()));
    proto_game.set_pot(game.round().map_or(0, |r| r.pot()));

    proto_game
}

impl PrivateInto<proto::game::Game> for Game {
    fn private_into(&self, player_id: &str) -> proto::game::Game {
        create_proto_game(self, player_id)
    }
}

#[cfg(test)]
mod tests {
    use super::Game;

    #[tokio::test]
    async fn should_set_initial_blinds() {
        let mut game = Game::new("a", "TOKEN");
        game.add_player("b");
        game.move_blinds_roles();

        assert_eq!(game.small_blind_id().clone().unwrap(), "a");
        assert_eq!(game.big_blind_id().clone().unwrap(), "b");
    }

    #[tokio::test]
    async fn should_set_second_blinds() {
        let mut game = Game::new("a", "TOKEN");
        game.add_player("b");
        game.move_blinds_roles();
        game.move_blinds_roles();

        assert_eq!(game.small_blind_id().clone().unwrap(), "b");
        assert_eq!(game.big_blind_id().clone().unwrap(), "a");
    }

    #[tokio::test]
    async fn should_set_blinds_back() {
        let mut game = Game::new("a", "TOKEN");
        game.add_player("b");
        game.add_player("c");
        game.add_player("d");
        game.move_blinds_roles();
        game.move_blinds_roles();
        game.move_blinds_roles();
        game.move_blinds_roles();

        assert_eq!(game.small_blind_id().clone().unwrap(), "d");
        assert_eq!(game.big_blind_id().clone().unwrap(), "a");
    }
}
