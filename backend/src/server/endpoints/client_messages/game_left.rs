use crate::{model::Player, server::app_context::AppContext};

pub async fn handle_game_leave(player: Option<Player>, ctx: &AppContext) -> Result<(), String> {
    let player = player.ok_or_else(|| String::from("Player not found"))?;
    let mut game = ctx
        .db()
        .games()
        .get(player.game_token())
        .await?
        .ok_or_else(|| String::from("Game not found"))?;

    game.remove_player(player.id());
    if let Some(round) = game.round_mut() {
        round.fold_player(player.id());
        // TODO check if he is also the last player and should end
    }

    ctx.db().games().persist(&game).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_game_leave;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_leave() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.add_credits(1000);
        player.assign_action(Action::Check);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();

        assert_eq!(game.round().unwrap().active_players().len(), 2);

        let result = handle_game_leave(Some(player), &ctx).await;
        assert!(result.is_ok());

        let game = ctx.db().games().get(game.token()).await.unwrap().unwrap();
        assert_eq!(game.round().unwrap().active_players().len(), 1);
    }
}
