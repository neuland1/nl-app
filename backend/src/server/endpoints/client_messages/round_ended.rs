use super::move_to_next_player;
use crate::{
    model::{Action, Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_ended(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    player.resolve_action(Action::EndRound);

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}
