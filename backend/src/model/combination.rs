use crate::model::{
    proto::{self},
    Card,
};
use protobuf::RepeatedField;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum Combination {
    HighCard {
        card: Card,
        remaining: (Card, Card, Card, Card),
    },
    Pair {
        cards: (Card, Card),
        remaining: (Card, Card, Card),
    },
    TwoPair {
        a: (Card, Card),
        b: (Card, Card),
        remaining: Card,
    },
    ThreeKind {
        cards: (Card, Card, Card),
        remaining: (Card, Card),
    },
    Straight {
        cards: (Card, Card, Card, Card, Card),
    },
    Flush {
        cards: (Card, Card, Card, Card, Card),
    },
    FullHouse {
        parents: (Card, Card),
        children: (Card, Card, Card),
    },
    FourKind {
        cards: (Card, Card, Card, Card),
        remaining: Card,
    },
    StraightFlush {
        cards: (Card, Card, Card, Card, Card),
    },
    RoyalFlush {
        cards: (Card, Card, Card, Card, Card),
    },
}

impl Combination {
    pub fn get_value(&self) -> usize {
        match self {
            Combination::HighCard { .. } => 1,
            Combination::Pair { .. } => 2,
            Combination::TwoPair { .. } => 3,
            Combination::ThreeKind { .. } => 4,
            Combination::Straight { .. } => 5,
            Combination::Flush { .. } => 6,
            Combination::FullHouse { .. } => 7,
            Combination::FourKind { .. } => 8,
            Combination::StraightFlush { .. } => 9,
            Combination::RoyalFlush { .. } => 10,
        }
    }

    pub fn get_hand(&self) -> Vec<Card> {
        match self {
            Combination::HighCard { card, remaining } => vec![
                card.clone(),
                remaining.0.clone(),
                remaining.1.clone(),
                remaining.2.clone(),
                remaining.3.clone(),
            ],
            Combination::Pair { cards, remaining } => vec![
                cards.0.clone(),
                cards.1.clone(),
                remaining.0.clone(),
                remaining.1.clone(),
                remaining.2.clone(),
            ],
            Combination::TwoPair { a, b, remaining } => {
                vec![
                    a.0.clone(),
                    a.1.clone(),
                    b.0.clone(),
                    b.1.clone(),
                    remaining.clone(),
                ]
            }
            Combination::ThreeKind { cards, remaining } => vec![
                cards.0.clone(),
                cards.1.clone(),
                cards.2.clone(),
                remaining.0.clone(),
                remaining.1.clone(),
            ],
            Combination::Straight { cards } => {
                vec![
                    cards.0.clone(),
                    cards.1.clone(),
                    cards.2.clone(),
                    cards.3.clone(),
                    cards.4.clone(),
                ]
            }
            Combination::Flush { cards } => vec![
                cards.0.clone(),
                cards.1.clone(),
                cards.2.clone(),
                cards.3.clone(),
                cards.4.clone(),
            ],
            Combination::FullHouse { parents, children } => {
                vec![
                    parents.0.clone(),
                    parents.1.clone(),
                    children.0.clone(),
                    children.1.clone(),
                    children.2.clone(),
                ]
            }
            Combination::FourKind { cards, remaining } => {
                vec![
                    cards.0.clone(),
                    cards.1.clone(),
                    cards.2.clone(),
                    cards.3.clone(),
                    remaining.clone(),
                ]
            }
            Combination::StraightFlush { cards } => {
                vec![
                    cards.0.clone(),
                    cards.1.clone(),
                    cards.2.clone(),
                    cards.3.clone(),
                    cards.4.clone(),
                ]
            }
            Combination::RoyalFlush { cards } => vec![
                cards.0.clone(),
                cards.1.clone(),
                cards.2.clone(),
                cards.3.clone(),
                cards.4.clone(),
            ],
        }
    }
}

impl Into<proto::game::Combination> for Combination {
    fn into(self) -> proto::game::Combination {
        let mut proto_combination = proto::game::Combination::new();
        match self {
            Combination::HighCard { card, remaining } => {
                let mut high_card = proto::game::Combination_HighCard::new();
                high_card.set_card(card.into());
                high_card.set_kickers(RepeatedField::from_slice(&[
                    remaining.0.into(),
                    remaining.1.into(),
                    remaining.2.into(),
                    remaining.3.into(),
                ]));
                proto_combination.set_high_card(high_card);
            }
            Combination::Pair { cards, remaining } => {
                let mut pair = proto::game::Combination_Pair::new();
                pair.set_cards(RepeatedField::from_slice(&[cards.0.into(), cards.1.into()]));
                pair.set_kickers(RepeatedField::from_slice(&[
                    remaining.0.into(),
                    remaining.1.into(),
                    remaining.2.into(),
                ]));
                proto_combination.set_pair(pair);
            }
            Combination::TwoPair { a, b, remaining } => {
                let mut two_pair = proto::game::Combination_TwoPair::new();
                two_pair.set_a_cards(RepeatedField::from_slice(&[a.0.into(), a.1.into()]));
                two_pair.set_b_cards(RepeatedField::from_slice(&[b.0.into(), b.1.into()]));
                two_pair.set_kicker(remaining.into());
                proto_combination.set_two_pair(two_pair);
            }
            Combination::ThreeKind { cards, remaining } => {
                let mut three_kind = proto::game::Combination_ThreeKind::new();
                three_kind.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                ]));
                three_kind.set_kickers(RepeatedField::from_slice(&[
                    remaining.0.into(),
                    remaining.1.into(),
                ]));
                proto_combination.set_three_kind(three_kind);
            }
            Combination::Straight { cards } => {
                let mut straight = proto::game::Combination_Straight::new();
                straight.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                    cards.3.into(),
                    cards.4.into(),
                ]));
                proto_combination.set_straight(straight);
            }
            Combination::Flush { cards } => {
                let mut flush = proto::game::Combination_Flush::new();
                flush.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                    cards.3.into(),
                    cards.4.into(),
                ]));
                proto_combination.set_flush(flush);
            }
            Combination::FullHouse { children, parents } => {
                let mut full_house = proto::game::Combination_FullHouse::new();
                full_house.set_parents(RepeatedField::from_slice(&[
                    parents.0.into(),
                    parents.1.into(),
                ]));
                full_house.set_children(RepeatedField::from_slice(&[
                    children.0.into(),
                    children.1.into(),
                    children.2.into(),
                ]));
                proto_combination.set_full_house(full_house);
            }
            Combination::FourKind { cards, remaining } => {
                let mut four_kind = proto::game::Combination_FourKind::new();
                four_kind.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                    cards.3.into(),
                ]));
                four_kind.set_kicker(remaining.into());
                proto_combination.set_four_kind(four_kind);
            }
            Combination::StraightFlush { cards } => {
                let mut straight_flush = proto::game::Combination_StraightFlush::new();
                straight_flush.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                    cards.3.into(),
                    cards.4.into(),
                ]));
                proto_combination.set_straight_flush(straight_flush);
            }
            Combination::RoyalFlush { cards } => {
                let mut royal_flush = proto::game::Combination_RoyalFlush::new();
                royal_flush.set_cards(RepeatedField::from_slice(&[
                    cards.0.into(),
                    cards.1.into(),
                    cards.2.into(),
                    cards.3.into(),
                    cards.4.into(),
                ]));
                proto_combination.set_royal_flush(royal_flush);
            }
        };
        proto_combination
    }
}

#[cfg(test)]
mod tests {
    use super::{Card, Combination};
    use crate::model::{
        proto::{self},
        CardColor,
    };

    fn get_dummy_card() -> Card {
        Card::new(CardColor::Clovers, 2)
    }

    #[test]
    fn should_get_hand_for_high_card() {
        let combination = Combination::HighCard {
            card: Card::new(CardColor::Clovers, 1),
            remaining: (
                Card::new(CardColor::Pikes, 2),
                Card::new(CardColor::Hearts, 3),
                Card::new(CardColor::Tiles, 5),
                Card::new(CardColor::Hearts, 8),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_pair() {
        let combination = Combination::Pair {
            cards: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 1),
            ),
            remaining: (
                Card::new(CardColor::Pikes, 2),
                Card::new(CardColor::Hearts, 3),
                Card::new(CardColor::Tiles, 5),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_two_pair() {
        let combination = Combination::TwoPair {
            a: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 1),
            ),
            b: (
                Card::new(CardColor::Clovers, 2),
                Card::new(CardColor::Pikes, 2),
            ),
            remaining: Card::new(CardColor::Tiles, 5),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_three_of_a_kind() {
        let combination = Combination::ThreeKind {
            cards: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 1),
                Card::new(CardColor::Tiles, 1),
            ),
            remaining: (
                Card::new(CardColor::Pikes, 2),
                Card::new(CardColor::Hearts, 3),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_straight() {
        let combination = Combination::Straight {
            cards: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 2),
                Card::new(CardColor::Tiles, 3),
                Card::new(CardColor::Tiles, 4),
                Card::new(CardColor::Tiles, 5),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_flush() {
        let combination = Combination::Flush {
            cards: (
                Card::new(CardColor::Tiles, 1),
                Card::new(CardColor::Tiles, 2),
                Card::new(CardColor::Tiles, 3),
                Card::new(CardColor::Tiles, 7),
                Card::new(CardColor::Tiles, 5),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_full_house() {
        let combination = Combination::FullHouse {
            parents: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 1),
            ),
            children: (
                Card::new(CardColor::Pikes, 2),
                Card::new(CardColor::Hearts, 2),
                Card::new(CardColor::Tiles, 2),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_four_kind() {
        let combination = Combination::FourKind {
            cards: (
                Card::new(CardColor::Clovers, 1),
                Card::new(CardColor::Pikes, 1),
                Card::new(CardColor::Tiles, 1),
                Card::new(CardColor::Hearts, 1),
            ),
            remaining: Card::new(CardColor::Tiles, 5),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_straight_flush() {
        let combination = Combination::StraightFlush {
            cards: (
                Card::new(CardColor::Tiles, 1),
                Card::new(CardColor::Tiles, 2),
                Card::new(CardColor::Tiles, 3),
                Card::new(CardColor::Tiles, 4),
                Card::new(CardColor::Tiles, 5),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_get_hand_for_royal_flush() {
        let combination = Combination::RoyalFlush {
            cards: (
                Card::new(CardColor::Tiles, 10),
                Card::new(CardColor::Tiles, 11),
                Card::new(CardColor::Tiles, 12),
                Card::new(CardColor::Tiles, 13),
                Card::new(CardColor::Tiles, 1),
            ),
        };
        assert_eq!(combination.get_hand().len(), 5);
    }

    #[test]
    fn should_serialize_high_card() {
        let combination = Combination::HighCard {
            card: get_dummy_card(),
            remaining: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_high_card());
    }

    #[test]
    fn should_serialize_pair() {
        let combination = Combination::Pair {
            cards: (get_dummy_card(), get_dummy_card()),
            remaining: (get_dummy_card(), get_dummy_card(), get_dummy_card()),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_pair());
    }

    #[test]
    fn should_serialize_three_of_a_kind() {
        let combination = Combination::ThreeKind {
            cards: (get_dummy_card(), get_dummy_card(), get_dummy_card()),
            remaining: (get_dummy_card(), get_dummy_card()),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_three_kind());
    }

    #[test]
    fn should_serialize_two_pair() {
        let combination = Combination::TwoPair {
            a: (get_dummy_card(), get_dummy_card()),
            b: (get_dummy_card(), get_dummy_card()),
            remaining: get_dummy_card(),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_two_pair());
    }

    #[test]
    fn should_serialize_straight() {
        let combination = Combination::Straight {
            cards: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_straight());
    }

    #[test]
    fn should_serialize_flush() {
        let combination = Combination::Flush {
            cards: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_flush());
    }

    #[test]
    fn should_serialize_full_house() {
        let combination = Combination::FullHouse {
            parents: (get_dummy_card(), get_dummy_card()),
            children: (get_dummy_card(), get_dummy_card(), get_dummy_card()),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_full_house());
    }

    #[test]
    fn should_serialize_four_of_a_kind() {
        let combination = Combination::FourKind {
            cards: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
            remaining: get_dummy_card(),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_four_kind());
    }

    #[test]
    fn should_serialize_straight_flush() {
        let combination = Combination::StraightFlush {
            cards: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_straight_flush());
    }

    #[test]
    fn should_serialize_royal_flush() {
        let combination = Combination::RoyalFlush {
            cards: (
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
                get_dummy_card(),
            ),
        };

        let serialized: proto::game::Combination = combination.clone().into();
        assert!(serialized.has_royal_flush());
    }
}
