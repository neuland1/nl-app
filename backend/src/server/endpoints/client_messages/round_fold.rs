use super::move_to_next_player;
use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_fold(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("No active round"))?;
    if !round.blinds_have_been_done() {
        return Err(format!(
            "Player {player} tried to fold but is big or small blind",
            player = player.id()
        ));
    }

    round.fold_player(player.id());

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_round_fold;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_fold_and_end_round() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Fold);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        game.round_mut()
            .unwrap()
            .move_to_next_player()
            .expect("Move failed");
        game.round_mut()
            .unwrap()
            .move_to_next_player()
            .expect("Move failed");
        ctx.db().games().persist(&game).await.unwrap();

        let result = handle_round_fold(&mut player, &mut game, &ctx).await;
        assert!(result.is_ok());
    }

    #[tokio::test]
    async fn should_handle_fold_and_move_to_next_player() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Fold);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();
        let player_c = Player::new("TOKEN");
        ctx.db().players().persist(&player_c).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.add_player(player_c.id());
        game.start();
        game.start_new_round();
        game.round_mut()
            .unwrap()
            .move_to_next_player()
            .expect("Move failed");
        game.round_mut()
            .unwrap()
            .move_to_next_player()
            .expect("Move failed");
        ctx.db().games().persist(&game).await.unwrap();

        let result = handle_round_fold(&mut player, &mut game, &ctx).await;
        assert!(result.is_ok());

        let updated_game = ctx.db().games().get(game.token()).await.unwrap();
        assert_eq!(
            updated_game.unwrap().round().unwrap().current_player(),
            player_b.id()
        );
    }
}
