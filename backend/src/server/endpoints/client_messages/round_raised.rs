use super::move_to_next_player;
use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_raised(
    amount: u32,
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round missing"))?;

    if amount < round.bet() * 2 {
        return Err(String::from("Raise too low"));
    }

    let added_missing_credits = round.raise_credits(amount, player.id());
    player.substract_credits(added_missing_credits);

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_round_raised;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_raise() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Raise);
        player.add_credits(1000);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();

        let result = handle_round_raised(100, &mut player, &mut game, &ctx).await;
        assert!(result.is_ok());
    }

    #[tokio::test]
    async fn should_handle_raise_with_too_low_raise_amount() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Raise);
        let mut game = Game::new(player.id(), "TOKEN");
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();
        ctx.db().players().persist(&player).await.unwrap();

        let result = handle_round_raised(1, &mut player, &mut game, &ctx).await;
        assert!(result.is_err());
    }
}
