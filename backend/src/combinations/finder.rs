use super::comparators::compare_cards;
use crate::model::{Card, Combination};
use itertools::Itertools;

pub fn combine_hand_and_deck(hand: &[Card], deck: &[Card]) -> Vec<Card> {
    if hand.len() != 2 || deck.len() != 5 {
        panic!("Player should have two and deck should have five cards");
    }

    let mut cards = vec![];
    cards.extend_from_slice(hand);
    cards.extend_from_slice(deck);

    let cards = cards
        .iter()
        .cloned()
        .sorted_by(super::comparators::compare_cards)
        .collect();

    cards
}

pub fn determine_highest_combination(cards: &[Card]) -> Combination {
    // create possible permutations with five out of all seven cards
    let all_hands = generate_hand_combinations(cards);

    let best_combination = all_hands
        .iter()
        .map(|h| self::determine_highest_combination_of_hand(h))
        .max_by(super::comparators::compare_combinations)
        .unwrap();

    best_combination
}

pub fn determine_highest_combination_of_hand(hand: &[Card]) -> Combination {
    if hand.len() != 5 {
        panic!("Hand should have exactly five cards");
    }

    let is_flush = self::is_flush(hand);
    let is_royal = self::is_royal(hand);

    // Royal Flush
    if is_flush && is_royal {
        return Combination::RoyalFlush {
            cards: hand
                .iter()
                .cloned()
                .collect_tuple::<(_, _, _, _, _)>()
                .unwrap(),
        };
    }

    let is_straight = self::is_straight(hand);

    // Straight Flush
    if is_straight && is_flush {
        return Combination::StraightFlush {
            cards: hand
                .iter()
                .cloned()
                .collect_tuple::<(_, _, _, _, _)>()
                .unwrap(),
        };
    }

    let value_groups = self::get_cards_grouped_by_value(hand);

    // Four of a kind
    if let Some(four_of_a_kind) = self::get_four_of_a_kind(&value_groups) {
        let (_, cards) = four_of_a_kind;
        let cards: (Card, Card, Card, Card) = cards.into_iter().collect_tuple().unwrap();
        let remaining = self::get_remaining_cards(
            hand,
            &[
                cards.0.clone(),
                cards.1.clone(),
                cards.2.clone(),
                cards.3.clone(),
            ],
        )
        .first()
        .unwrap()
        .clone();
        return Combination::FourKind { cards, remaining };
    }

    let three_of_a_kind = self::get_three_of_a_kind(&value_groups);
    let two_pairs = self::get_two_pairs(&value_groups);

    // Full House
    if three_of_a_kind.is_some() && two_pairs.len() == 1 {
        let children = three_of_a_kind
            .and_then(|elem| elem.1.iter().cloned().collect_tuple::<(_, _, _)>())
            .unwrap();
        let parents = two_pairs
            .get(0)
            .and_then(|elem| elem.1.iter().cloned().collect_tuple::<(_, _)>())
            .unwrap();
        return Combination::FullHouse { children, parents };
    }

    // Flush
    if is_flush {
        return Combination::Flush {
            cards: hand
                .iter()
                .cloned()
                .collect_tuple::<(_, _, _, _, _)>()
                .unwrap(),
        };
    }

    // Straight
    if is_straight {
        return Combination::Straight {
            cards: hand
                .iter()
                .cloned()
                .collect_tuple::<(_, _, _, _, _)>()
                .unwrap(),
        };
    }

    // Three of a kind
    if three_of_a_kind.is_some() && two_pairs.is_empty() {
        let cards = three_of_a_kind
            .and_then(|elem| elem.1.iter().cloned().collect_tuple::<(_, _, _)>())
            .unwrap();
        let remaining =
            self::get_remaining_cards(hand, &[cards.0.clone(), cards.1.clone(), cards.2.clone()])
                .iter()
                .cloned()
                .collect_tuple()
                .unwrap();
        return Combination::ThreeKind { cards, remaining };
    }

    // Two Pair
    if two_pairs.len() == 2 {
        let (a, b) = two_pairs
            .iter()
            .sorted_by(|(_, a_cards), (_, b_cards)| {
                compare_cards(&a_cards.get(0).unwrap(), &b_cards.get(0).unwrap())
            })
            .map(|(_, t)| t.iter().cloned().collect_tuple::<(_, _)>().unwrap())
            .collect_tuple::<(_, _)>()
            .unwrap();
        let remaining =
            self::get_remaining_cards(hand, &[a.0.clone(), a.1.clone(), b.0.clone(), b.1.clone()])
                .first()
                .unwrap()
                .clone();
        return Combination::TwoPair { a, b, remaining };
    }

    // Pair
    if two_pairs.len() == 1 {
        let cards = two_pairs
            .get(0)
            .and_then(|elem| elem.1.iter().cloned().collect_tuple::<(_, _)>())
            .unwrap();
        let remaining = self::get_remaining_cards(hand, &[cards.0.clone(), cards.1.clone()])
            .iter()
            .cloned()
            .collect_tuple()
            .unwrap();
        return Combination::Pair { cards, remaining };
    }

    // High Card
    let card = self::get_high_card(hand);
    Combination::HighCard {
        remaining: self::get_remaining_cards(hand, &[card.clone()])
            .iter()
            .cloned()
            .collect_tuple()
            .unwrap(),
        card,
    }
}

fn is_flush(hand: &[Card]) -> bool {
    hand.iter()
        .cloned()
        .group_by(|c| c.color().value())
        .into_iter()
        .any(|(_, b)| b.count() == 5)
}

fn is_royal(hand: &[Card]) -> bool {
    let values = hand.iter().map(|c| c.value()).dedup().collect_vec();
    if values.len() != 5 {
        return false;
    }

    values
        .iter()
        .all(|i| i == &1 || i == &13 || i == &12 || i == &11 || i == &10)
}

fn is_straight(hand: &[Card]) -> bool {
    let plain_values = hand
        .iter()
        .map(|c| c.value())
        .collect_tuple::<(_, _, _, _, _)>()
        .unwrap();

    plain_values.0 + 1 == plain_values.1
        && plain_values.1 + 1 == plain_values.2
        && plain_values.2 + 1 == plain_values.3
        // as Ace is defined as 1 we need to check both possibilities
        && ((plain_values.4 == 1 && (plain_values.0 == 2 || plain_values.3 == 13)) || plain_values.3 + 1 == plain_values.4)
}

fn get_cards_grouped_by_value(hand: &[Card]) -> Vec<(u32, Vec<Card>)> {
    hand.iter()
        .group_by(|c| c.value())
        .into_iter()
        .map(|(value, group)| (value, group.cloned().collect_vec()))
        .collect()
}

fn get_four_of_a_kind(value_groups: &[(u32, Vec<Card>)]) -> Option<(u32, Vec<Card>)> {
    value_groups
        .iter()
        .find(|(_, group)| group.len() == 4)
        .cloned()
}

fn get_three_of_a_kind(value_groups: &[(u32, Vec<Card>)]) -> Option<(u32, Vec<Card>)> {
    value_groups
        .iter()
        .find(|(_, group)| group.len() == 3)
        .cloned()
}

fn get_two_pairs(value_groups: &[(u32, Vec<Card>)]) -> Vec<(u32, Vec<Card>)> {
    value_groups
        .iter()
        .filter(|(_, group)| group.len() == 2)
        .cloned()
        .collect()
}

fn get_high_card(hand: &[Card]) -> Card {
    hand.iter()
        .cloned()
        .max_by(super::comparators::compare_cards)
        .unwrap()
}

fn get_remaining_cards(hand: &[Card], selected_cards: &[Card]) -> Vec<Card> {
    hand.iter()
        .cloned()
        .filter(|c| selected_cards.iter().find(|s| *s == c).is_none())
        .sorted_by(super::comparators::compare_cards)
        .collect()
}

/// Generates all possible combinations for a hand and the deck. They should be 21 in total. The cards are sorted by value and color.
fn generate_hand_combinations(cards: &[Card]) -> Vec<Vec<Card>> {
    cards
        .iter()
        .cloned()
        .combinations(5)
        .map(|c| {
            c.iter()
                .cloned()
                .sorted_by(super::comparators::compare_cards)
                .collect()
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::{
        combine_hand_and_deck, determine_highest_combination, generate_hand_combinations,
        Combination,
    };
    use crate::model::{Card, CardColor};

    #[test]
    #[should_panic(expected = "Player should have two and deck should have five cards")]
    fn should_panic_for_no_cards() {
        let hand = vec![];
        let deck = vec![];

        let cards = combine_hand_and_deck(&hand, &deck);
        determine_highest_combination(&cards);
    }

    #[test]
    #[should_panic(expected = "Player should have two and deck should have five cards")]
    fn should_panic_for_missing_cards() {
        let hand = vec![Card::new(CardColor::Pikes, 2)];
        let deck = vec![
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Hearts, 11),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        determine_highest_combination(&cards);
    }

    #[test]
    fn should_return_all_combinations() {
        let hand = vec![
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 1),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Hearts, 10),
            Card::new(CardColor::Hearts, 9),
            Card::new(CardColor::Hearts, 8),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let all_hands = generate_hand_combinations(&cards);

        assert_eq!(all_hands.len(), 21);
    }

    #[test]
    fn should_find_high_card_with_ace() {
        let hand = vec![
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 1),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Hearts, 10),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Tiles, 8),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::HighCard { card, remaining } => {
                assert_eq!(card.value(), 1);
                assert_eq!(card.color(), &CardColor::Hearts);
                assert_eq!(remaining.3.value(), 10);
                assert_eq!(remaining.2.value(), 9);
                assert_eq!(remaining.1.value(), 8);
                assert_eq!(remaining.0.value(), 7);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_high_card_without_ace() {
        let hand = vec![
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 4),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Hearts, 10),
            Card::new(CardColor::Clovers, 9),
            Card::new(CardColor::Tiles, 3),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::HighCard { card, remaining } => {
                assert_eq!(card.value(), 10);
                assert_eq!(card.color(), &CardColor::Hearts);
                assert_eq!(remaining.3.value(), 9);
                assert_eq!(remaining.2.value(), 7);
                assert_eq!(remaining.1.value(), 5);
                assert_eq!(remaining.0.value(), 4);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_pair() {
        let hand = vec![
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Clovers, 5),
        ];
        let deck = vec![
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Hearts, 5),
            Card::new(CardColor::Hearts, 9),
            Card::new(CardColor::Tiles, 8),
            Card::new(CardColor::Pikes, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::Pair {
                cards: (a, b),
                remaining,
            } => {
                assert_eq!(a.value(), 5);
                assert_eq!(a.color(), &CardColor::Clovers);
                assert_eq!(b.value(), 5);
                assert_eq!(b.color(), &CardColor::Hearts);
                assert_eq!(remaining.2.value(), 1);
                assert_eq!(remaining.1.value(), 9);
                assert_eq!(remaining.0.value(), 8);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_highest_two_pairs() {
        let hand = vec![
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 2),
        ];
        let deck = vec![
            Card::new(CardColor::Hearts, 1),
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 2),
            Card::new(CardColor::Pikes, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::TwoPair {
                a: (a_1, a_2),
                b: (b_1, b_2),
                remaining,
            } => {
                assert_eq!(a_1.value(), 3);
                assert_eq!(a_1.color(), &CardColor::Hearts);
                assert_eq!(a_2.value(), 3);
                assert_eq!(a_2.color(), &CardColor::Pikes);
                assert_eq!(b_1.value(), 1);
                assert_eq!(b_1.color(), &CardColor::Hearts);
                assert_eq!(b_2.value(), 1);
                assert_eq!(b_2.color(), &CardColor::Pikes);
                assert_eq!(remaining.value(), 7);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_two_pair() {
        let hand = vec![
            Card::new(CardColor::Pikes, 2),
            Card::new(CardColor::Hearts, 9),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 5),
            Card::new(CardColor::Hearts, 5),
            Card::new(CardColor::Pikes, 9),
            Card::new(CardColor::Hearts, 8),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::TwoPair {
                a: (a_1, a_2),
                b: (b_1, b_2),
                remaining,
            } => {
                assert_eq!(a_1.value(), 5);
                assert_eq!(a_1.color(), &CardColor::Clovers);
                assert_eq!(a_2.value(), 5);
                assert_eq!(a_2.color(), &CardColor::Hearts);
                assert_eq!(b_1.value(), 9);
                assert_eq!(b_1.color(), &CardColor::Hearts);
                assert_eq!(b_2.value(), 9);
                assert_eq!(b_2.color(), &CardColor::Pikes);
                assert_eq!(remaining.value(), 8);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_three_of_a_kind() {
        let hand = vec![
            Card::new(CardColor::Pikes, 11),
            Card::new(CardColor::Tiles, 1),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Hearts, 5),
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Tiles, 11),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::ThreeKind {
                cards: (a, b, c),
                remaining,
            } => {
                assert_eq!(a.value(), 11);
                assert_eq!(a.color(), &CardColor::Clovers);
                assert_eq!(b.value(), 11);
                assert_eq!(b.color(), &CardColor::Tiles);
                assert_eq!(c.value(), 11);
                assert_eq!(c.color(), &CardColor::Pikes);
                assert_eq!(remaining.1.value(), 1);
                assert_eq!(remaining.0.value(), 7);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_full_house() {
        let hand = vec![
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Tiles, 11),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Hearts, 5),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Tiles, 3),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::FullHouse {
                children: (c1, c2, c3),
                parents: (p1, p2),
            } => {
                assert_eq!(c1.value(), 3);
                assert_eq!(c1.color(), &CardColor::Tiles);
                assert_eq!(c2.value(), 3);
                assert_eq!(c2.color(), &CardColor::Hearts);
                assert_eq!(c3.value(), 3);
                assert_eq!(c3.color(), &CardColor::Pikes);

                assert_eq!(p1.value(), 11);
                assert_eq!(p1.color(), &CardColor::Clovers);
                assert_eq!(p2.value(), 11);
                assert_eq!(p2.color(), &CardColor::Tiles);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_four_of_a_kind() {
        let hand = vec![
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Tiles, 3),
        ];
        let deck = vec![
            Card::new(CardColor::Clovers, 11),
            Card::new(CardColor::Hearts, 5),
            Card::new(CardColor::Hearts, 3),
            Card::new(CardColor::Clovers, 3),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::FourKind {
                cards: (a, b, c, d),
                remaining,
            } => {
                assert_eq!(a.value(), 3);
                assert_eq!(a.color(), &CardColor::Clovers);
                assert_eq!(b.value(), 3);
                assert_eq!(b.color(), &CardColor::Tiles);
                assert_eq!(c.value(), 3);
                assert_eq!(c.color(), &CardColor::Hearts);
                assert_eq!(d.value(), 3);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(remaining.value(), 11);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_royal_flush() {
        let hand = vec![
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Pikes, 12),
        ];
        let deck = vec![
            Card::new(CardColor::Pikes, 13),
            Card::new(CardColor::Pikes, 11),
            Card::new(CardColor::Pikes, 10),
            Card::new(CardColor::Pikes, 5),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::RoyalFlush {
                cards: (a, b, c, d, e),
            } => {
                assert_eq!(a.value(), 10);
                assert_eq!(a.color(), &CardColor::Pikes);
                assert_eq!(b.value(), 11);
                assert_eq!(b.color(), &CardColor::Pikes);
                assert_eq!(c.value(), 12);
                assert_eq!(c.color(), &CardColor::Pikes);
                assert_eq!(d.value(), 13);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(e.value(), 1);
                assert_eq!(e.color(), &CardColor::Pikes);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_straight_flush() {
        let hand = vec![
            Card::new(CardColor::Pikes, 9),
            Card::new(CardColor::Pikes, 12),
        ];
        let deck = vec![
            Card::new(CardColor::Pikes, 13),
            Card::new(CardColor::Pikes, 11),
            Card::new(CardColor::Pikes, 10),
            Card::new(CardColor::Pikes, 5),
            Card::new(CardColor::Hearts, 7),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::StraightFlush {
                cards: (a, b, c, d, e),
            } => {
                assert_eq!(a.value(), 9);
                assert_eq!(a.color(), &CardColor::Pikes);
                assert_eq!(b.value(), 10);
                assert_eq!(b.color(), &CardColor::Pikes);
                assert_eq!(c.value(), 11);
                assert_eq!(c.color(), &CardColor::Pikes);
                assert_eq!(d.value(), 12);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(e.value(), 13);
                assert_eq!(e.color(), &CardColor::Pikes);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_flush() {
        let hand = vec![
            Card::new(CardColor::Pikes, 10),
            Card::new(CardColor::Pikes, 2),
        ];
        let deck = vec![
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Hearts, 11),
            Card::new(CardColor::Pikes, 6),
            Card::new(CardColor::Pikes, 11),
            Card::new(CardColor::Hearts, 8),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::Flush {
                cards: (a, b, c, d, e),
            } => {
                assert_eq!(a.value(), 2);
                assert_eq!(a.color(), &CardColor::Pikes);
                assert_eq!(b.value(), 3);
                assert_eq!(b.color(), &CardColor::Pikes);
                assert_eq!(c.value(), 6);
                assert_eq!(c.color(), &CardColor::Pikes);
                assert_eq!(d.value(), 10);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(e.value(), 11);
                assert_eq!(e.color(), &CardColor::Pikes);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_straight_with_ace_as_lowest_card() {
        let hand = vec![
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 2),
        ];
        let deck = vec![
            Card::new(CardColor::Pikes, 3),
            Card::new(CardColor::Hearts, 4),
            Card::new(CardColor::Pikes, 7),
            Card::new(CardColor::Pikes, 5),
            Card::new(CardColor::Hearts, 8),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::Straight {
                cards: (a, b, c, d, e),
            } => {
                assert_eq!(a.value(), 2);
                assert_eq!(a.color(), &CardColor::Clovers);
                assert_eq!(b.value(), 3);
                assert_eq!(b.color(), &CardColor::Pikes);
                assert_eq!(c.value(), 4);
                assert_eq!(c.color(), &CardColor::Hearts);
                assert_eq!(d.value(), 5);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(e.value(), 1);
                assert_eq!(e.color(), &CardColor::Pikes);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }

    #[test]
    fn should_find_straight_with_ace_as_highest_card() {
        let hand = vec![
            Card::new(CardColor::Pikes, 1),
            Card::new(CardColor::Clovers, 12),
        ];
        let deck = vec![
            Card::new(CardColor::Pikes, 13),
            Card::new(CardColor::Hearts, 11),
            Card::new(CardColor::Pikes, 10),
            Card::new(CardColor::Pikes, 8),
            Card::new(CardColor::Hearts, 9),
        ];

        let cards = combine_hand_and_deck(&hand, &deck);
        let res = determine_highest_combination(&cards);

        match res {
            Combination::Straight {
                cards: (a, b, c, d, e),
            } => {
                assert_eq!(a.value(), 10);
                assert_eq!(a.color(), &CardColor::Pikes);
                assert_eq!(b.value(), 11);
                assert_eq!(b.color(), &CardColor::Hearts);
                assert_eq!(c.value(), 12);
                assert_eq!(c.color(), &CardColor::Clovers);
                assert_eq!(d.value(), 13);
                assert_eq!(d.color(), &CardColor::Pikes);
                assert_eq!(e.value(), 1);
                assert_eq!(e.color(), &CardColor::Pikes);
            }
            combination => {
                panic!(format!("Unexpected answer: {:?}", combination))
            }
        }
    }
}
