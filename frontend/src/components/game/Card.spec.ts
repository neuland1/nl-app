import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Card from "./Card.svelte";
import { Card as ProtoCard, Card_Color } from "../../types/proto/card";

describe("Card", () => {
  it("should render ace", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_CLOVERS, value: 1 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("A")).to.exist;
  });

  it("should render king", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_CLOVERS, value: 13 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("K")).to.exist;
  });

  it("should render queen", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_CLOVERS, value: 12 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("Q")).to.exist;
  });

  it("should render jack", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_CLOVERS, value: 11 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("J")).to.exist;
  });

  it("should render clovers", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_CLOVERS, value: 1 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("♣")).to.exist;
  });
  it("should render hearts", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_HEARTS, value: 1 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("♡")).to.exist;
  });
  it("should render pikes", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_PIKES, value: 1 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("♠")).to.exist;
  });
  it("should render tiles", async () => {
    let card: ProtoCard = { color: Card_Color.COLOR_TILES, value: 1 };
    const { queryByText } = render(Card, {
      props: {
        card,
      },
    });

    expect(queryByText("♢")).to.exist;
  });
});
