import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Settings from "./Settings.svelte";
import { Game_State } from "../../types/proto/game";
import { ownPlayer, players, activeGame, activities } from "../store";

describe("Settings", () => {
  it("should render page", async () => {
    activeGame.update({
      adminId: "a",
      bigBlindId: "a",
      blind: 1,
      playerIds: ["a", "b"],
      pot: 1,
      smallBlindId: "b",
      state: Game_State.STATE_STARTED,
      token: "abc",
    });
    players.update({ credits: 123, id: "a", name: "A" });
    players.update({ credits: 987, id: "b", name: "B" });
    ownPlayer.update({
      credits: 123,
      id: "a",
      name: "A",
      actions: [],
    });
    activities.update("a", false);
    activities.update("b", true);

    const { queryByText } = render(Settings, {
      props: { sendWsMessage: () => {}, leaveGame: () => {} },
    });

    expect(queryByText("2/8 Players")).to.exist;
    expect(queryByText("Start")).to.exist;
  });
});
