export const changelog: Array<{
  version: string;
  name: string;
  changes: Array<string>;
}> = [
  {
    version: "0.5.1",
    name: "Schnittlauch",
    changes: ["Improved developer experience", "Small UI improvements"],
  },
  {
    version: "0.5.0",
    name: "Cranberry",
    changes: [
      "Simplified automated deployments",
      "Implemented hopefully correct split pot with side pots",
    ],
  },
  {
    version: "0.4.3",
    name: "Walnuss",
    changes: ["Added simple imprint and privacy notice"],
  },
  {
    version: "0.4.2",
    name: "Granatapfel",
    changes: ["Updated depedencies", "Security improvements for deployment"],
  },
  {
    version: "0.4.1",
    name: "Erdbeere",
    changes: [
      "Support more settings in lobby",
      "Fixed unrecognized kicker cards after show down",
    ],
  },
  {
    version: "0.4.0",
    name: "Apfel",
    changes: [
      "Respect kicker cards for highest hand",
      "Fixed blinds increase",
      "Fixed wrong order of straights with aces",
      "Fixed invalid order of full houses",
      "UI fixes",
      "Mark admin in active game",
    ],
  },
  {
    version: "0.3.6",
    name: "Kaffee",
    changes: [
      "Raise with self defined values higher then double bet",
      "Added security question for more risky actions like All In",
      "Fixed missing raise as first player",
      "Support game reentry from start page",
      "Possibility to increase blinds after each round",
      "UX improvements",
    ],
  },
  {
    version: "0.3.5",
    name: "Wiersing",
    changes: ["Support pasteable attend links"],
  },
  {
    version: "0.3.4",
    name: "Rosenkohl",
    changes: ["Use persistent volumes to safe game state"],
  },
  {
    version: "0.3.3",
    name: "Brokkoli",
    changes: ["Improved UI and added separate pages for Imprint and Privacy"],
  },
  {
    version: "0.3.2",
    name: "Weißkohl",
    changes: [
      "Fixed stuck inactive players",
      "Fixed incompatibility with older browsers",
    ],
  },
  {
    version: "0.3.1",
    name: "Kohlrabi",
    changes: ["Fixed release build and deployment"],
  },
  {
    version: "0.3.0",
    name: "Radieschen",
    changes: ["Added show down", "Small UI improvements"],
  },
  {
    version: "0.2.14",
    name: "Schneeglöckchen",
    changes: [
      "Added basic animations and improved user interface",
      "Finished basic game flow",
    ],
  },
  {
    version: "0.2.13",
    name: "Kresse",
    changes: ["First named release"],
  },
];
