export type AttendGameResponse = {
  game: string;
  token: string;
};
