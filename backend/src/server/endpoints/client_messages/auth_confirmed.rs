use crate::{
    model::{
        proto::{self},
        PrivateInto,
    },
    server::{app_context::AppContext, auth::extract_verified_player},
};
use log::{debug, warn};

pub async fn handle_auth_confirmation(
    token: &str,
    peer_id: &str,
    ctx: &AppContext,
) -> Result<(), String> {
    match extract_verified_player(&token, ctx).await {
        Some(mut new_player) => {
            new_player.set_active();
            ctx.db().players().persist(&new_player).await?;
            let game = ctx.db().games().get(new_player.game_token()).await?;
            ctx.ws()
                .register_active_player(&new_player, &game, peer_id)
                .await?;

            if let Some(game) = ctx.db().games().get(new_player.game_token()).await? {
                // inform new player about game
                let mut game_updated_msg = proto::message::Server_GameUpdated::new();
                game_updated_msg.set_game(game.clone().private_into(new_player.id()));
                let mut msg = proto::message::Server::new();
                msg.set_game_updated(game_updated_msg);
                ctx.ws()
                    .send_message(String::from(new_player.id()), msg)
                    .await?;

                // inform new player about existing players
                for other_player_id in game.player_ids() {
                    if let Some(other_player) = ctx
                        .db()
                        .players()
                        .get(other_player_id)
                        .await?
                        .filter(|p| p.id() != new_player.id())
                    {
                        // send general infos about player
                        let mut entered_msg = proto::message::Server_PlayerEntered::new();
                        entered_msg.set_player(other_player.clone().into());
                        let mut msg = proto::message::Server::new();
                        msg.set_player_entered(entered_msg);
                        ctx.ws()
                            .send_message(String::from(new_player.id()), msg)
                            .await?;

                        // send message if player is inactive
                        if other_player.last_active_time().is_some() {
                            let mut left_conn_msg = proto::message::Server_PlayerLostConn::new();
                            left_conn_msg.set_player_id(String::from(other_player.id()));
                            let mut msg = proto::message::Server::new();
                            msg.set_player_lost_conn(left_conn_msg);
                            ctx.ws()
                                .send_message(String::from(new_player.id()), msg)
                                .await?;
                        }
                    } else {
                        warn!(
                            "Player \"{}\" needed for informing about new player not found",
                            other_player_id
                        );
                    }
                }
            }
            Ok(())
        }
        None => {
            let mut msg = proto::message::Server::new();
            msg.set_game_declined(proto::message::Server_GameDeclined::new());
            ctx.ws().send_message(String::from(peer_id), msg).await?;
            debug!("Unauthorized user tried to access game {}", token);
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::handle_auth_confirmation;
    use crate::{
        config::AppConfig,
        model::{Action, Game, Player},
        server::{
            app_context::{AppContext, DbClients},
            auth::generate_jwt_token,
            ws::{WsClient, WsCommand},
        },
    };
    use envconfig::Envconfig;
    use log::error;
    use tokio::sync::mpsc;

    #[tokio::test]
    async fn should_handle_auth_message_with_action() {
        let ctx = AppContext::init();
        let mut player = Player::new("GAME");
        player.assign_action(Action::Settings);
        let game = Game::new(player.id(), "GAME");
        ctx.db()
            .games()
            .persist(&game)
            .await
            .expect("Persisting game has failed");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player has failed");
        let token = generate_jwt_token(&player, &ctx.config().auth_secret);
        ctx.ws()
            .register_active_player(&player, &None, "peer-id")
            .await
            .expect("Registering players connection failed");

        let reply = handle_auth_confirmation(&token, "peer-id", &ctx).await;
        assert!(reply.is_ok());
    }

    #[tokio::test]
    async fn should_handle_auth_message_without_action() {
        let ctx = AppContext::init();
        let player = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player has failed");
        let token = generate_jwt_token(&player, &ctx.config().auth_secret);
        ctx.ws()
            .register_active_player(&player, &None, "peer-id")
            .await
            .expect("Registering players connection failed");

        let reply = handle_auth_confirmation(&token, "peer-id", &ctx).await;
        assert!(reply.is_ok());
    }

    #[tokio::test]
    async fn should_handle_auth_message_with_invalid_token() {
        let (change_sender, mut change_receiver): (
            mpsc::Sender<WsCommand>,
            mpsc::Receiver<WsCommand>,
        ) = mpsc::channel(256);
        let config = AppConfig::init_from_env().expect("Loading server config failed");
        let ctx = AppContext {
            config: config.clone(),
            db: DbClients::init(&config),
            ws: WsClient {
                sender: change_sender,
            },
        };
        let player = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player has failed");

        let reply = handle_auth_confirmation("invalid", "peer-id", &ctx).await;
        assert!(reply.is_ok());

        let sent_msg = change_receiver.recv().await;

        if let Some(command) = sent_msg {
            match command {
                WsCommand::SendMessage { msg, .. } => {
                    assert!(msg.has_game_declined());
                }
                _ => {
                    error!("Unexpected type: {:?}", command);
                }
            }
        } else {
            panic!("Received no command");
        }
    }
}
