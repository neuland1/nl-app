use crate::{db::Client, model::Game, server::app_context::AppContext};
use chrono::{Duration, Utc};
use log::{debug, error, info};

/// Removes inactive games if there is no player present for more than ten minutes.
pub fn cleanup_games(ctx: &'static AppContext) -> impl Fn() {
    move || {
        tokio::task::spawn(async move {
            if let Err(err) = execute_cleanup_games(ctx.db().games(), Duration::minutes(10)).await {
                error!("Cleanup of games has failed: {}", &err);
            }
        });
    }
}

async fn execute_cleanup_games(client: &Client<Game>, duration: Duration) -> Result<bool, String> {
    let inactive_games = client.scan(Box::new(is_inactive_game(duration))).await?;

    let inactive_count = inactive_games.len();
    if inactive_count == 0 {
        debug!("Removed no inactive games");
        Ok(false)
    } else {
        client.remove_batch(&inactive_games).await?;
        info!("Removed {} inactive games", inactive_count);
        Ok(true)
    }
}

// Game is inactive if no admin is present and last activity happened more than ten minutes ago
fn is_inactive_game(duration: Duration) -> impl Fn(&Game) -> bool {
    let threshold = Utc::now().checked_sub_signed(duration).unwrap();
    move |game: &Game| match game.admin_id() {
        Some(_) => false,
        None => game.last_action_time().lt(&threshold),
    }
}

#[cfg(test)]
mod tests {
    use super::execute_cleanup_games;
    use crate::{
        config::AppConfig,
        db::{Client, Database},
        model::Game,
    };
    use chrono::Duration;
    use envconfig::Envconfig;

    fn init_client() -> Client<Game> {
        let config = AppConfig::init_from_env().expect("Loading server config failed");
        let (mut repo, sender) = Database::init("games", &config);
        tokio::task::spawn(async move {
            repo.start_listening().await;
        });

        Client::new(sender)
    }

    #[tokio::test]
    async fn should_cleanup_games() {
        let client = init_client();
        let mut game = Game::new("admin", "TOKEN");
        game.remove_player("admin");
        client.persist(&game).await.expect("Game persist failed");
        assert!(client
            .get("TOKEN")
            .await
            .unwrap()
            .unwrap()
            .admin_id()
            .is_none());
        assert!(client.get("TOKEN").await.unwrap().is_some());

        let res = execute_cleanup_games(&client, Duration::nanoseconds(1)).await;
        assert!(res.is_ok());
        assert!(res.unwrap());
        assert!(client.get("TOKEN").await.unwrap().is_none());
    }

    #[tokio::test]
    async fn should_not_cleanup_games_with_admin() {
        let client = init_client();
        let game = Game::new("admin", "TOKEN");
        client.persist(&game).await.expect("Game persist failed");
        assert!(client
            .get("TOKEN")
            .await
            .unwrap()
            .unwrap()
            .admin_id()
            .is_some());

        let res = execute_cleanup_games(&client, Duration::nanoseconds(1)).await;
        assert!(res.is_ok());
        assert!(!res.unwrap());
        assert!(client.get("TOKEN").await.unwrap().is_some());
    }

    #[tokio::test]
    async fn should_not_cleanup_games_with_recent_action() {
        let client = init_client();
        let mut game = Game::new("admin", "TOKEN");
        game.remove_player("admin");
        client.persist(&game).await.expect("Game persist failed");
        assert!(client
            .get("TOKEN")
            .await
            .unwrap()
            .unwrap()
            .admin_id()
            .is_none());

        let res = execute_cleanup_games(&client, Duration::minutes(5)).await;
        assert!(res.is_ok());
        assert!(!res.unwrap());
        assert!(client.get("TOKEN").await.unwrap().is_some());
    }
}
