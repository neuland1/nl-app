use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};
use log::info;

pub async fn handle_increase_blinds(
    new_small_blind: u32,
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    game.set_blinds(new_small_blind)?;
    game.restart_round();

    ctx.db().games().persist(game).await?;

    info!(
        "{} increased the blinds to {} in game {}",
        player.id(),
        new_small_blind,
        game.token()
    );

    Ok(())
}
