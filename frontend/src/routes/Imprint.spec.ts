import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Imprint from "./Imprint.svelte";

describe("Imprint", () => {
  it("should render page", async () => {
    const { queryByText } = render(Imprint);

    expect(queryByText("Imprint")).to.exist;
  });
});
