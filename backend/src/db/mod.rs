mod change_listener;
mod client;
mod database;

pub use self::change_listener::ChangeListener;
pub use self::client::Client;
pub use self::database::Database;
use redis::{FromRedisValue, RedisError, ToRedisArgs};
use std::{
    clone::Clone,
    collections::HashMap,
    fmt::{self, Debug},
};
use tokio::sync::oneshot;

pub trait Persist: Clone + Debug + Send + ToRedisArgs + FromRedisValue {
    fn id(&self) -> &str;
}

#[derive(Debug, Clone)]
pub struct QueryError {
    message: String,
}

impl QueryError {
    pub fn new(message: &str) -> Self {
        QueryError {
            message: String::from(message),
        }
    }

    pub fn from_redis_err(error: RedisError) -> Self {
        QueryError {
            message: error.to_string(),
        }
    }
}

impl fmt::Display for QueryError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid first item to double")
    }
}

impl From<QueryError> for String {
    fn from(e: QueryError) -> Self {
        format!("QueryError: {error}", error = e)
    }
}

#[derive(Derivative)]
#[derivative(Debug)]
pub struct CommandData<R: Debug> {
    id: String,
    #[derivative(Debug = "ignore")]
    responder: oneshot::Sender<R>,
}

pub type ScanFunction<T> = Box<dyn Fn(&T) -> bool + Send + Sync>;

#[derive(Derivative)]
#[derivative(Debug)]
pub enum Command<T: Persist> {
    Get {
        key: String,
        data: CommandData<Result<Option<T>, RedisError>>,
    },
    GetBatch {
        keys: Vec<String>,
        data: CommandData<Result<HashMap<String, T>, RedisError>>,
    },
    Scan {
        #[derivative(Debug = "ignore")]
        scan_function: ScanFunction<T>,
        data: CommandData<Result<Vec<String>, RedisError>>,
    },
    Persist {
        value: T,
        data: CommandData<Result<(), RedisError>>,
    },
    PersistBatch {
        values: Vec<T>,
        data: CommandData<Result<(), RedisError>>,
    },
    Remove {
        key: String,
        data: CommandData<Result<(), RedisError>>,
    },
    Purge {
        data: CommandData<Result<(), RedisError>>,
    },
    RemoveBatch {
        keys: Vec<String>,
        data: CommandData<Result<(), RedisError>>,
    },
    Count {
        data: CommandData<Result<usize, RedisError>>,
    },
}
