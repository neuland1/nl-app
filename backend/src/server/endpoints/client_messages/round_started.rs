use crate::{
    model::{Action, Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_started(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round missing"))?;

    player.resolve_action(Action::StartRound);
    player.resolve_action(Action::IncreaseBlinds);

    if round.needed_credits_for_call(player.id()) < player.credits() {
        player.assign_action(Action::Call);
    } else {
        player.assign_action(Action::AllIn);
    }

    round.move_to_next_state()?;

    let (persist_player, persist_game) = tokio::join!(
        ctx.db().players().persist(player),
        ctx.db().games().persist(game)
    );
    persist_player.and(persist_game)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_round_started;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_round_start() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.add_credits(1000);
        player.assign_action(Action::StartRound);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();

        let result = handle_round_started(&mut player, &mut game, &ctx).await;
        assert!(result.is_ok());

        let player = ctx.db().players().get(player.id()).await.unwrap().unwrap();
        assert!(player.actions().contains(&Action::Call));
    }
}
