import { render } from "@testing-library/svelte";
import { expect } from "chai";
import Privacy from "./Privacy.svelte";

describe("Privacy", () => {
  it("should render page", async () => {
    const { queryByText } = render(Privacy);

    expect(queryByText("Privacy")).to.exist;
  });
});
