const httpProxy = require("http-proxy");
const proxy = httpProxy.createServer({ target: "http://localhost:3333" });

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: { url: "/", static: true, resolve: false },
    src: { url: "/dist" },
  },
  plugins: [
    "@snowpack/plugin-svelte",
    "@snowpack/plugin-dotenv",
    "@snowpack/plugin-typescript",
    "@snowpack/plugin-postcss",
  ],
  routes: [],
  optimize: {
    bundle: true,
    manifest: false,
    treeshake: true,
    minify: true,
    target: "es2018",
  },
  routes: [
    {
      src: "/api/.*",
      dest: (req, res) => proxy.web(req, res),
    },
    { match: "routes", src: ".*", dest: "/index.html" },
  ],
  packageOptions: {},
  devOptions: {},
  buildOptions: {},
};
