pub mod app_context;
mod auth;
mod endpoints;
mod logger;
mod reply;
mod ws;

use self::{
    app_context::AppContext,
    endpoints::{
        active_game::handle_ws_filter,
        games::{attend_game_filter, create_game_filter},
        service::handle_info_filter,
    },
    logger::log_request,
    reply::handle_rejection,
};
use chrono::Utc;
use endpoints::service::handle_statistics_filter;
use warp::Filter;

pub async fn run_server(ctx: &'static AppContext) {
    let game_route =
        warp::path("games").and(
            // PUT /api/games
            warp::put()
                .and_then(move || async move { create_game_filter(ctx).await })
                .or(
                    // POST /api/games/:token/attend
                    warp::post().and(warp::path!(String / "attend")).and_then(
                        move |game_token: String| async move {
                            attend_game_filter(&game_token, ctx).await
                        },
                    ),
                ),
        );

    let start_date_time = Utc::now();
    let api_route = warp::path("api").and(
        game_route
            // WS /api/active-game
            .or(warp::path("active-game")
                .and(warp::ws())
                .map(move |ws| handle_ws_filter(ws, ctx)))
            .or(warp::path("service").and(
                // GET /api/service/info
                warp::get()
                    .and(warp::path("info").and_then(move || async move {
                        handle_info_filter(&start_date_time, ctx).await
                    }))
                    // GET /api/service/statistics
                    .or(warp::path("statistics")
                        .and_then(move || async move { handle_statistics_filter(ctx).await })),
            )),
    );

    let routes = api_route
        .recover(handle_rejection)
        .with(warp::log::custom(|info| {
            log_request(&info);
        }));

    warp::serve(routes)
        .run(([0, 0, 0, 0], ctx.config().port))
        .await;
}
