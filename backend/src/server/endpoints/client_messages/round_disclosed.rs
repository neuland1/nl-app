use super::move_to_next_player;
use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_disclosed(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("No active round"))?;

    round.disclose_cards(player.id());

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::handle_round_disclosed;
    use crate::{
        model::{Action, Game, Player},
        server::app_context::AppContext,
    };

    #[tokio::test]
    async fn should_handle_disclose() {
        let ctx = AppContext::init();
        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Disclose);
        ctx.db().players().persist(&player).await.unwrap();
        let player_b = Player::new("TOKEN");
        ctx.db().players().persist(&player_b).await.unwrap();

        let mut game = Game::new(player.id(), "TOKEN");
        game.add_player(player_b.id());
        game.start();
        game.start_new_round();
        ctx.db().games().persist(&game).await.unwrap();
        assert!(!game
            .round()
            .unwrap()
            .disclosed_players()
            .contains(player.id()));

        let result = handle_round_disclosed(&mut player, &mut game, &ctx).await;
        assert!(result.is_ok());

        let game = ctx.db().games().get(game.token()).await.unwrap().unwrap();
        assert!(game
            .round()
            .unwrap()
            .disclosed_players()
            .contains(player.id()));
    }
}
