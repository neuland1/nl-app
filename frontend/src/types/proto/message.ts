/* eslint-disable */
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import * as Long from "long";
import { Player, OwnPlayer } from "./player";
import { Game } from "./game";
import { Card } from "./card";

export const protobufPackage = "";

export interface Client {
  message?:
    | { $case: "authConfirmed"; authConfirmed: Client_AuthConfirmed }
    | { $case: "nameUpdated"; nameUpdated: Client_NameUpdated }
    | { $case: "gameStarted"; gameStarted: Client_GameStarted }
    | { $case: "gameLeft"; gameLeft: Client_GameLeft }
    | { $case: "roundCalled"; roundCalled: Client_RoundCalled }
    | { $case: "roundRaised"; roundRaised: Client_RoundRaised }
    | { $case: "roundFold"; roundFold: Client_RoundFold }
    | { $case: "roundStarted"; roundStarted: Client_RoundStarted }
    | { $case: "roundChecked"; roundChecked: Client_RoundChecked }
    | { $case: "roundDisclosed"; roundDisclosed: Client_RoundDisclosed }
    | { $case: "roundEnded"; roundEnded: Client_RoundEnded }
    | { $case: "roundAllIn"; roundAllIn: Client_RoundAllIn }
    | { $case: "increasedBlinds"; increasedBlinds: Client_IncreasedBlinds };
}

export interface Client_AuthConfirmed {
  token: string;
}

export interface Client_NameUpdated {
  name: string;
}

export interface Client_GameStarted {}

export interface Client_GameLeft {}

export interface Client_RoundCalled {}

export interface Client_RoundRaised {
  amount: number;
}

export interface Client_RoundFold {}

export interface Client_RoundStarted {}

export interface Client_RoundChecked {}

export interface Client_RoundDisclosed {}

export interface Client_RoundEnded {}

export interface Client_RoundAllIn {}

export interface Client_IncreasedBlinds {
  smallBlind: number;
}

export interface Server {
  message?:
    | { $case: "playerUpdated"; playerUpdated: Server_PlayerUpdated }
    | { $case: "gameUpdated"; gameUpdated: Server_GameUpdated }
    | { $case: "selfUpdated"; selfUpdated: Server_SelfUpdated }
    | { $case: "playerEntered"; playerEntered: Server_PlayerEntered }
    | { $case: "playerLostConn"; playerLostConn: Server_PlayerLostConn }
    | { $case: "gameDeclined"; gameDeclined: Server_GameDeclined };
}

export interface Server_PlayerUpdated {
  player?: Player;
}

export interface Server_SelfUpdated {
  player?: OwnPlayer;
}

export interface Server_GameUpdated {
  game?: Game;
}

export interface Server_PlayerEntered {
  player?: Player;
}

export interface Server_PlayerLostConn {
  playerId: string;
}

export interface Server_GameDeclined {}

export interface Server_CardReceived {
  card?: Card;
}

const baseClient: object = {};

export const Client = {
  encode(message: Client, writer: Writer = Writer.create()): Writer {
    if (message.message?.$case === "authConfirmed") {
      Client_AuthConfirmed.encode(
        message.message.authConfirmed,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.message?.$case === "nameUpdated") {
      Client_NameUpdated.encode(
        message.message.nameUpdated,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.message?.$case === "gameStarted") {
      Client_GameStarted.encode(
        message.message.gameStarted,
        writer.uint32(82).fork()
      ).ldelim();
    }
    if (message.message?.$case === "gameLeft") {
      Client_GameLeft.encode(
        message.message.gameLeft,
        writer.uint32(90).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundCalled") {
      Client_RoundCalled.encode(
        message.message.roundCalled,
        writer.uint32(162).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundRaised") {
      Client_RoundRaised.encode(
        message.message.roundRaised,
        writer.uint32(170).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundFold") {
      Client_RoundFold.encode(
        message.message.roundFold,
        writer.uint32(178).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundStarted") {
      Client_RoundStarted.encode(
        message.message.roundStarted,
        writer.uint32(186).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundChecked") {
      Client_RoundChecked.encode(
        message.message.roundChecked,
        writer.uint32(194).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundDisclosed") {
      Client_RoundDisclosed.encode(
        message.message.roundDisclosed,
        writer.uint32(202).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundEnded") {
      Client_RoundEnded.encode(
        message.message.roundEnded,
        writer.uint32(210).fork()
      ).ldelim();
    }
    if (message.message?.$case === "roundAllIn") {
      Client_RoundAllIn.encode(
        message.message.roundAllIn,
        writer.uint32(218).fork()
      ).ldelim();
    }
    if (message.message?.$case === "increasedBlinds") {
      Client_IncreasedBlinds.encode(
        message.message.increasedBlinds,
        writer.uint32(226).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient } as Client;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.message = {
            $case: "authConfirmed",
            authConfirmed: Client_AuthConfirmed.decode(reader, reader.uint32()),
          };
          break;
        case 2:
          message.message = {
            $case: "nameUpdated",
            nameUpdated: Client_NameUpdated.decode(reader, reader.uint32()),
          };
          break;
        case 10:
          message.message = {
            $case: "gameStarted",
            gameStarted: Client_GameStarted.decode(reader, reader.uint32()),
          };
          break;
        case 11:
          message.message = {
            $case: "gameLeft",
            gameLeft: Client_GameLeft.decode(reader, reader.uint32()),
          };
          break;
        case 20:
          message.message = {
            $case: "roundCalled",
            roundCalled: Client_RoundCalled.decode(reader, reader.uint32()),
          };
          break;
        case 21:
          message.message = {
            $case: "roundRaised",
            roundRaised: Client_RoundRaised.decode(reader, reader.uint32()),
          };
          break;
        case 22:
          message.message = {
            $case: "roundFold",
            roundFold: Client_RoundFold.decode(reader, reader.uint32()),
          };
          break;
        case 23:
          message.message = {
            $case: "roundStarted",
            roundStarted: Client_RoundStarted.decode(reader, reader.uint32()),
          };
          break;
        case 24:
          message.message = {
            $case: "roundChecked",
            roundChecked: Client_RoundChecked.decode(reader, reader.uint32()),
          };
          break;
        case 25:
          message.message = {
            $case: "roundDisclosed",
            roundDisclosed: Client_RoundDisclosed.decode(
              reader,
              reader.uint32()
            ),
          };
          break;
        case 26:
          message.message = {
            $case: "roundEnded",
            roundEnded: Client_RoundEnded.decode(reader, reader.uint32()),
          };
          break;
        case 27:
          message.message = {
            $case: "roundAllIn",
            roundAllIn: Client_RoundAllIn.decode(reader, reader.uint32()),
          };
          break;
        case 28:
          message.message = {
            $case: "increasedBlinds",
            increasedBlinds: Client_IncreasedBlinds.decode(
              reader,
              reader.uint32()
            ),
          };
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Client {
    const message = { ...baseClient } as Client;
    if (object.authConfirmed !== undefined && object.authConfirmed !== null) {
      message.message = {
        $case: "authConfirmed",
        authConfirmed: Client_AuthConfirmed.fromJSON(object.authConfirmed),
      };
    }
    if (object.nameUpdated !== undefined && object.nameUpdated !== null) {
      message.message = {
        $case: "nameUpdated",
        nameUpdated: Client_NameUpdated.fromJSON(object.nameUpdated),
      };
    }
    if (object.gameStarted !== undefined && object.gameStarted !== null) {
      message.message = {
        $case: "gameStarted",
        gameStarted: Client_GameStarted.fromJSON(object.gameStarted),
      };
    }
    if (object.gameLeft !== undefined && object.gameLeft !== null) {
      message.message = {
        $case: "gameLeft",
        gameLeft: Client_GameLeft.fromJSON(object.gameLeft),
      };
    }
    if (object.roundCalled !== undefined && object.roundCalled !== null) {
      message.message = {
        $case: "roundCalled",
        roundCalled: Client_RoundCalled.fromJSON(object.roundCalled),
      };
    }
    if (object.roundRaised !== undefined && object.roundRaised !== null) {
      message.message = {
        $case: "roundRaised",
        roundRaised: Client_RoundRaised.fromJSON(object.roundRaised),
      };
    }
    if (object.roundFold !== undefined && object.roundFold !== null) {
      message.message = {
        $case: "roundFold",
        roundFold: Client_RoundFold.fromJSON(object.roundFold),
      };
    }
    if (object.roundStarted !== undefined && object.roundStarted !== null) {
      message.message = {
        $case: "roundStarted",
        roundStarted: Client_RoundStarted.fromJSON(object.roundStarted),
      };
    }
    if (object.roundChecked !== undefined && object.roundChecked !== null) {
      message.message = {
        $case: "roundChecked",
        roundChecked: Client_RoundChecked.fromJSON(object.roundChecked),
      };
    }
    if (object.roundDisclosed !== undefined && object.roundDisclosed !== null) {
      message.message = {
        $case: "roundDisclosed",
        roundDisclosed: Client_RoundDisclosed.fromJSON(object.roundDisclosed),
      };
    }
    if (object.roundEnded !== undefined && object.roundEnded !== null) {
      message.message = {
        $case: "roundEnded",
        roundEnded: Client_RoundEnded.fromJSON(object.roundEnded),
      };
    }
    if (object.roundAllIn !== undefined && object.roundAllIn !== null) {
      message.message = {
        $case: "roundAllIn",
        roundAllIn: Client_RoundAllIn.fromJSON(object.roundAllIn),
      };
    }
    if (
      object.increasedBlinds !== undefined &&
      object.increasedBlinds !== null
    ) {
      message.message = {
        $case: "increasedBlinds",
        increasedBlinds: Client_IncreasedBlinds.fromJSON(
          object.increasedBlinds
        ),
      };
    }
    return message;
  },

  toJSON(message: Client): unknown {
    const obj: any = {};
    message.message?.$case === "authConfirmed" &&
      (obj.authConfirmed = message.message?.authConfirmed
        ? Client_AuthConfirmed.toJSON(message.message?.authConfirmed)
        : undefined);
    message.message?.$case === "nameUpdated" &&
      (obj.nameUpdated = message.message?.nameUpdated
        ? Client_NameUpdated.toJSON(message.message?.nameUpdated)
        : undefined);
    message.message?.$case === "gameStarted" &&
      (obj.gameStarted = message.message?.gameStarted
        ? Client_GameStarted.toJSON(message.message?.gameStarted)
        : undefined);
    message.message?.$case === "gameLeft" &&
      (obj.gameLeft = message.message?.gameLeft
        ? Client_GameLeft.toJSON(message.message?.gameLeft)
        : undefined);
    message.message?.$case === "roundCalled" &&
      (obj.roundCalled = message.message?.roundCalled
        ? Client_RoundCalled.toJSON(message.message?.roundCalled)
        : undefined);
    message.message?.$case === "roundRaised" &&
      (obj.roundRaised = message.message?.roundRaised
        ? Client_RoundRaised.toJSON(message.message?.roundRaised)
        : undefined);
    message.message?.$case === "roundFold" &&
      (obj.roundFold = message.message?.roundFold
        ? Client_RoundFold.toJSON(message.message?.roundFold)
        : undefined);
    message.message?.$case === "roundStarted" &&
      (obj.roundStarted = message.message?.roundStarted
        ? Client_RoundStarted.toJSON(message.message?.roundStarted)
        : undefined);
    message.message?.$case === "roundChecked" &&
      (obj.roundChecked = message.message?.roundChecked
        ? Client_RoundChecked.toJSON(message.message?.roundChecked)
        : undefined);
    message.message?.$case === "roundDisclosed" &&
      (obj.roundDisclosed = message.message?.roundDisclosed
        ? Client_RoundDisclosed.toJSON(message.message?.roundDisclosed)
        : undefined);
    message.message?.$case === "roundEnded" &&
      (obj.roundEnded = message.message?.roundEnded
        ? Client_RoundEnded.toJSON(message.message?.roundEnded)
        : undefined);
    message.message?.$case === "roundAllIn" &&
      (obj.roundAllIn = message.message?.roundAllIn
        ? Client_RoundAllIn.toJSON(message.message?.roundAllIn)
        : undefined);
    message.message?.$case === "increasedBlinds" &&
      (obj.increasedBlinds = message.message?.increasedBlinds
        ? Client_IncreasedBlinds.toJSON(message.message?.increasedBlinds)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Client>): Client {
    const message = { ...baseClient } as Client;
    if (
      object.message?.$case === "authConfirmed" &&
      object.message?.authConfirmed !== undefined &&
      object.message?.authConfirmed !== null
    ) {
      message.message = {
        $case: "authConfirmed",
        authConfirmed: Client_AuthConfirmed.fromPartial(
          object.message.authConfirmed
        ),
      };
    }
    if (
      object.message?.$case === "nameUpdated" &&
      object.message?.nameUpdated !== undefined &&
      object.message?.nameUpdated !== null
    ) {
      message.message = {
        $case: "nameUpdated",
        nameUpdated: Client_NameUpdated.fromPartial(object.message.nameUpdated),
      };
    }
    if (
      object.message?.$case === "gameStarted" &&
      object.message?.gameStarted !== undefined &&
      object.message?.gameStarted !== null
    ) {
      message.message = {
        $case: "gameStarted",
        gameStarted: Client_GameStarted.fromPartial(object.message.gameStarted),
      };
    }
    if (
      object.message?.$case === "gameLeft" &&
      object.message?.gameLeft !== undefined &&
      object.message?.gameLeft !== null
    ) {
      message.message = {
        $case: "gameLeft",
        gameLeft: Client_GameLeft.fromPartial(object.message.gameLeft),
      };
    }
    if (
      object.message?.$case === "roundCalled" &&
      object.message?.roundCalled !== undefined &&
      object.message?.roundCalled !== null
    ) {
      message.message = {
        $case: "roundCalled",
        roundCalled: Client_RoundCalled.fromPartial(object.message.roundCalled),
      };
    }
    if (
      object.message?.$case === "roundRaised" &&
      object.message?.roundRaised !== undefined &&
      object.message?.roundRaised !== null
    ) {
      message.message = {
        $case: "roundRaised",
        roundRaised: Client_RoundRaised.fromPartial(object.message.roundRaised),
      };
    }
    if (
      object.message?.$case === "roundFold" &&
      object.message?.roundFold !== undefined &&
      object.message?.roundFold !== null
    ) {
      message.message = {
        $case: "roundFold",
        roundFold: Client_RoundFold.fromPartial(object.message.roundFold),
      };
    }
    if (
      object.message?.$case === "roundStarted" &&
      object.message?.roundStarted !== undefined &&
      object.message?.roundStarted !== null
    ) {
      message.message = {
        $case: "roundStarted",
        roundStarted: Client_RoundStarted.fromPartial(
          object.message.roundStarted
        ),
      };
    }
    if (
      object.message?.$case === "roundChecked" &&
      object.message?.roundChecked !== undefined &&
      object.message?.roundChecked !== null
    ) {
      message.message = {
        $case: "roundChecked",
        roundChecked: Client_RoundChecked.fromPartial(
          object.message.roundChecked
        ),
      };
    }
    if (
      object.message?.$case === "roundDisclosed" &&
      object.message?.roundDisclosed !== undefined &&
      object.message?.roundDisclosed !== null
    ) {
      message.message = {
        $case: "roundDisclosed",
        roundDisclosed: Client_RoundDisclosed.fromPartial(
          object.message.roundDisclosed
        ),
      };
    }
    if (
      object.message?.$case === "roundEnded" &&
      object.message?.roundEnded !== undefined &&
      object.message?.roundEnded !== null
    ) {
      message.message = {
        $case: "roundEnded",
        roundEnded: Client_RoundEnded.fromPartial(object.message.roundEnded),
      };
    }
    if (
      object.message?.$case === "roundAllIn" &&
      object.message?.roundAllIn !== undefined &&
      object.message?.roundAllIn !== null
    ) {
      message.message = {
        $case: "roundAllIn",
        roundAllIn: Client_RoundAllIn.fromPartial(object.message.roundAllIn),
      };
    }
    if (
      object.message?.$case === "increasedBlinds" &&
      object.message?.increasedBlinds !== undefined &&
      object.message?.increasedBlinds !== null
    ) {
      message.message = {
        $case: "increasedBlinds",
        increasedBlinds: Client_IncreasedBlinds.fromPartial(
          object.message.increasedBlinds
        ),
      };
    }
    return message;
  },
};

const baseClient_AuthConfirmed: object = { token: "" };

export const Client_AuthConfirmed = {
  encode(
    message: Client_AuthConfirmed,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_AuthConfirmed {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_AuthConfirmed } as Client_AuthConfirmed;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.token = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Client_AuthConfirmed {
    const message = { ...baseClient_AuthConfirmed } as Client_AuthConfirmed;
    if (object.token !== undefined && object.token !== null) {
      message.token = String(object.token);
    }
    return message;
  },

  toJSON(message: Client_AuthConfirmed): unknown {
    const obj: any = {};
    message.token !== undefined && (obj.token = message.token);
    return obj;
  },

  fromPartial(object: DeepPartial<Client_AuthConfirmed>): Client_AuthConfirmed {
    const message = { ...baseClient_AuthConfirmed } as Client_AuthConfirmed;
    if (object.token !== undefined && object.token !== null) {
      message.token = object.token;
    }
    return message;
  },
};

const baseClient_NameUpdated: object = { name: "" };

export const Client_NameUpdated = {
  encode(
    message: Client_NameUpdated,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.name !== "") {
      writer.uint32(10).string(message.name);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_NameUpdated {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_NameUpdated } as Client_NameUpdated;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Client_NameUpdated {
    const message = { ...baseClient_NameUpdated } as Client_NameUpdated;
    if (object.name !== undefined && object.name !== null) {
      message.name = String(object.name);
    }
    return message;
  },

  toJSON(message: Client_NameUpdated): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<Client_NameUpdated>): Client_NameUpdated {
    const message = { ...baseClient_NameUpdated } as Client_NameUpdated;
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    return message;
  },
};

const baseClient_GameStarted: object = {};

export const Client_GameStarted = {
  encode(_: Client_GameStarted, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_GameStarted {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_GameStarted } as Client_GameStarted;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_GameStarted {
    const message = { ...baseClient_GameStarted } as Client_GameStarted;
    return message;
  },

  toJSON(_: Client_GameStarted): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_GameStarted>): Client_GameStarted {
    const message = { ...baseClient_GameStarted } as Client_GameStarted;
    return message;
  },
};

const baseClient_GameLeft: object = {};

export const Client_GameLeft = {
  encode(_: Client_GameLeft, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_GameLeft {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_GameLeft } as Client_GameLeft;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_GameLeft {
    const message = { ...baseClient_GameLeft } as Client_GameLeft;
    return message;
  },

  toJSON(_: Client_GameLeft): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_GameLeft>): Client_GameLeft {
    const message = { ...baseClient_GameLeft } as Client_GameLeft;
    return message;
  },
};

const baseClient_RoundCalled: object = {};

export const Client_RoundCalled = {
  encode(_: Client_RoundCalled, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundCalled {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundCalled } as Client_RoundCalled;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundCalled {
    const message = { ...baseClient_RoundCalled } as Client_RoundCalled;
    return message;
  },

  toJSON(_: Client_RoundCalled): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundCalled>): Client_RoundCalled {
    const message = { ...baseClient_RoundCalled } as Client_RoundCalled;
    return message;
  },
};

const baseClient_RoundRaised: object = { amount: 0 };

export const Client_RoundRaised = {
  encode(
    message: Client_RoundRaised,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.amount !== 0) {
      writer.uint32(8).uint32(message.amount);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundRaised {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundRaised } as Client_RoundRaised;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.amount = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Client_RoundRaised {
    const message = { ...baseClient_RoundRaised } as Client_RoundRaised;
    if (object.amount !== undefined && object.amount !== null) {
      message.amount = Number(object.amount);
    }
    return message;
  },

  toJSON(message: Client_RoundRaised): unknown {
    const obj: any = {};
    message.amount !== undefined && (obj.amount = message.amount);
    return obj;
  },

  fromPartial(object: DeepPartial<Client_RoundRaised>): Client_RoundRaised {
    const message = { ...baseClient_RoundRaised } as Client_RoundRaised;
    if (object.amount !== undefined && object.amount !== null) {
      message.amount = object.amount;
    }
    return message;
  },
};

const baseClient_RoundFold: object = {};

export const Client_RoundFold = {
  encode(_: Client_RoundFold, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundFold {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundFold } as Client_RoundFold;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundFold {
    const message = { ...baseClient_RoundFold } as Client_RoundFold;
    return message;
  },

  toJSON(_: Client_RoundFold): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundFold>): Client_RoundFold {
    const message = { ...baseClient_RoundFold } as Client_RoundFold;
    return message;
  },
};

const baseClient_RoundStarted: object = {};

export const Client_RoundStarted = {
  encode(_: Client_RoundStarted, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundStarted {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundStarted } as Client_RoundStarted;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundStarted {
    const message = { ...baseClient_RoundStarted } as Client_RoundStarted;
    return message;
  },

  toJSON(_: Client_RoundStarted): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundStarted>): Client_RoundStarted {
    const message = { ...baseClient_RoundStarted } as Client_RoundStarted;
    return message;
  },
};

const baseClient_RoundChecked: object = {};

export const Client_RoundChecked = {
  encode(_: Client_RoundChecked, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundChecked {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundChecked } as Client_RoundChecked;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundChecked {
    const message = { ...baseClient_RoundChecked } as Client_RoundChecked;
    return message;
  },

  toJSON(_: Client_RoundChecked): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundChecked>): Client_RoundChecked {
    const message = { ...baseClient_RoundChecked } as Client_RoundChecked;
    return message;
  },
};

const baseClient_RoundDisclosed: object = {};

export const Client_RoundDisclosed = {
  encode(_: Client_RoundDisclosed, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundDisclosed {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundDisclosed } as Client_RoundDisclosed;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundDisclosed {
    const message = { ...baseClient_RoundDisclosed } as Client_RoundDisclosed;
    return message;
  },

  toJSON(_: Client_RoundDisclosed): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundDisclosed>): Client_RoundDisclosed {
    const message = { ...baseClient_RoundDisclosed } as Client_RoundDisclosed;
    return message;
  },
};

const baseClient_RoundEnded: object = {};

export const Client_RoundEnded = {
  encode(_: Client_RoundEnded, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundEnded {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundEnded } as Client_RoundEnded;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundEnded {
    const message = { ...baseClient_RoundEnded } as Client_RoundEnded;
    return message;
  },

  toJSON(_: Client_RoundEnded): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundEnded>): Client_RoundEnded {
    const message = { ...baseClient_RoundEnded } as Client_RoundEnded;
    return message;
  },
};

const baseClient_RoundAllIn: object = {};

export const Client_RoundAllIn = {
  encode(_: Client_RoundAllIn, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_RoundAllIn {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_RoundAllIn } as Client_RoundAllIn;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Client_RoundAllIn {
    const message = { ...baseClient_RoundAllIn } as Client_RoundAllIn;
    return message;
  },

  toJSON(_: Client_RoundAllIn): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Client_RoundAllIn>): Client_RoundAllIn {
    const message = { ...baseClient_RoundAllIn } as Client_RoundAllIn;
    return message;
  },
};

const baseClient_IncreasedBlinds: object = { smallBlind: 0 };

export const Client_IncreasedBlinds = {
  encode(
    message: Client_IncreasedBlinds,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.smallBlind !== 0) {
      writer.uint32(16).uint32(message.smallBlind);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Client_IncreasedBlinds {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClient_IncreasedBlinds } as Client_IncreasedBlinds;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.smallBlind = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Client_IncreasedBlinds {
    const message = { ...baseClient_IncreasedBlinds } as Client_IncreasedBlinds;
    if (object.smallBlind !== undefined && object.smallBlind !== null) {
      message.smallBlind = Number(object.smallBlind);
    }
    return message;
  },

  toJSON(message: Client_IncreasedBlinds): unknown {
    const obj: any = {};
    message.smallBlind !== undefined && (obj.smallBlind = message.smallBlind);
    return obj;
  },

  fromPartial(
    object: DeepPartial<Client_IncreasedBlinds>
  ): Client_IncreasedBlinds {
    const message = { ...baseClient_IncreasedBlinds } as Client_IncreasedBlinds;
    if (object.smallBlind !== undefined && object.smallBlind !== null) {
      message.smallBlind = object.smallBlind;
    }
    return message;
  },
};

const baseServer: object = {};

export const Server = {
  encode(message: Server, writer: Writer = Writer.create()): Writer {
    if (message.message?.$case === "playerUpdated") {
      Server_PlayerUpdated.encode(
        message.message.playerUpdated,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.message?.$case === "gameUpdated") {
      Server_GameUpdated.encode(
        message.message.gameUpdated,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.message?.$case === "selfUpdated") {
      Server_SelfUpdated.encode(
        message.message.selfUpdated,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.message?.$case === "playerEntered") {
      Server_PlayerEntered.encode(
        message.message.playerEntered,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.message?.$case === "playerLostConn") {
      Server_PlayerLostConn.encode(
        message.message.playerLostConn,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.message?.$case === "gameDeclined") {
      Server_GameDeclined.encode(
        message.message.gameDeclined,
        writer.uint32(58).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer } as Server;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.message = {
            $case: "playerUpdated",
            playerUpdated: Server_PlayerUpdated.decode(reader, reader.uint32()),
          };
          break;
        case 2:
          message.message = {
            $case: "gameUpdated",
            gameUpdated: Server_GameUpdated.decode(reader, reader.uint32()),
          };
          break;
        case 3:
          message.message = {
            $case: "selfUpdated",
            selfUpdated: Server_SelfUpdated.decode(reader, reader.uint32()),
          };
          break;
        case 4:
          message.message = {
            $case: "playerEntered",
            playerEntered: Server_PlayerEntered.decode(reader, reader.uint32()),
          };
          break;
        case 5:
          message.message = {
            $case: "playerLostConn",
            playerLostConn: Server_PlayerLostConn.decode(
              reader,
              reader.uint32()
            ),
          };
          break;
        case 7:
          message.message = {
            $case: "gameDeclined",
            gameDeclined: Server_GameDeclined.decode(reader, reader.uint32()),
          };
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server {
    const message = { ...baseServer } as Server;
    if (object.playerUpdated !== undefined && object.playerUpdated !== null) {
      message.message = {
        $case: "playerUpdated",
        playerUpdated: Server_PlayerUpdated.fromJSON(object.playerUpdated),
      };
    }
    if (object.gameUpdated !== undefined && object.gameUpdated !== null) {
      message.message = {
        $case: "gameUpdated",
        gameUpdated: Server_GameUpdated.fromJSON(object.gameUpdated),
      };
    }
    if (object.selfUpdated !== undefined && object.selfUpdated !== null) {
      message.message = {
        $case: "selfUpdated",
        selfUpdated: Server_SelfUpdated.fromJSON(object.selfUpdated),
      };
    }
    if (object.playerEntered !== undefined && object.playerEntered !== null) {
      message.message = {
        $case: "playerEntered",
        playerEntered: Server_PlayerEntered.fromJSON(object.playerEntered),
      };
    }
    if (object.playerLostConn !== undefined && object.playerLostConn !== null) {
      message.message = {
        $case: "playerLostConn",
        playerLostConn: Server_PlayerLostConn.fromJSON(object.playerLostConn),
      };
    }
    if (object.gameDeclined !== undefined && object.gameDeclined !== null) {
      message.message = {
        $case: "gameDeclined",
        gameDeclined: Server_GameDeclined.fromJSON(object.gameDeclined),
      };
    }
    return message;
  },

  toJSON(message: Server): unknown {
    const obj: any = {};
    message.message?.$case === "playerUpdated" &&
      (obj.playerUpdated = message.message?.playerUpdated
        ? Server_PlayerUpdated.toJSON(message.message?.playerUpdated)
        : undefined);
    message.message?.$case === "gameUpdated" &&
      (obj.gameUpdated = message.message?.gameUpdated
        ? Server_GameUpdated.toJSON(message.message?.gameUpdated)
        : undefined);
    message.message?.$case === "selfUpdated" &&
      (obj.selfUpdated = message.message?.selfUpdated
        ? Server_SelfUpdated.toJSON(message.message?.selfUpdated)
        : undefined);
    message.message?.$case === "playerEntered" &&
      (obj.playerEntered = message.message?.playerEntered
        ? Server_PlayerEntered.toJSON(message.message?.playerEntered)
        : undefined);
    message.message?.$case === "playerLostConn" &&
      (obj.playerLostConn = message.message?.playerLostConn
        ? Server_PlayerLostConn.toJSON(message.message?.playerLostConn)
        : undefined);
    message.message?.$case === "gameDeclined" &&
      (obj.gameDeclined = message.message?.gameDeclined
        ? Server_GameDeclined.toJSON(message.message?.gameDeclined)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server>): Server {
    const message = { ...baseServer } as Server;
    if (
      object.message?.$case === "playerUpdated" &&
      object.message?.playerUpdated !== undefined &&
      object.message?.playerUpdated !== null
    ) {
      message.message = {
        $case: "playerUpdated",
        playerUpdated: Server_PlayerUpdated.fromPartial(
          object.message.playerUpdated
        ),
      };
    }
    if (
      object.message?.$case === "gameUpdated" &&
      object.message?.gameUpdated !== undefined &&
      object.message?.gameUpdated !== null
    ) {
      message.message = {
        $case: "gameUpdated",
        gameUpdated: Server_GameUpdated.fromPartial(object.message.gameUpdated),
      };
    }
    if (
      object.message?.$case === "selfUpdated" &&
      object.message?.selfUpdated !== undefined &&
      object.message?.selfUpdated !== null
    ) {
      message.message = {
        $case: "selfUpdated",
        selfUpdated: Server_SelfUpdated.fromPartial(object.message.selfUpdated),
      };
    }
    if (
      object.message?.$case === "playerEntered" &&
      object.message?.playerEntered !== undefined &&
      object.message?.playerEntered !== null
    ) {
      message.message = {
        $case: "playerEntered",
        playerEntered: Server_PlayerEntered.fromPartial(
          object.message.playerEntered
        ),
      };
    }
    if (
      object.message?.$case === "playerLostConn" &&
      object.message?.playerLostConn !== undefined &&
      object.message?.playerLostConn !== null
    ) {
      message.message = {
        $case: "playerLostConn",
        playerLostConn: Server_PlayerLostConn.fromPartial(
          object.message.playerLostConn
        ),
      };
    }
    if (
      object.message?.$case === "gameDeclined" &&
      object.message?.gameDeclined !== undefined &&
      object.message?.gameDeclined !== null
    ) {
      message.message = {
        $case: "gameDeclined",
        gameDeclined: Server_GameDeclined.fromPartial(
          object.message.gameDeclined
        ),
      };
    }
    return message;
  },
};

const baseServer_PlayerUpdated: object = {};

export const Server_PlayerUpdated = {
  encode(
    message: Server_PlayerUpdated,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.player !== undefined) {
      Player.encode(message.player, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_PlayerUpdated {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_PlayerUpdated } as Server_PlayerUpdated;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.player = Player.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_PlayerUpdated {
    const message = { ...baseServer_PlayerUpdated } as Server_PlayerUpdated;
    if (object.player !== undefined && object.player !== null) {
      message.player = Player.fromJSON(object.player);
    }
    return message;
  },

  toJSON(message: Server_PlayerUpdated): unknown {
    const obj: any = {};
    message.player !== undefined &&
      (obj.player = message.player ? Player.toJSON(message.player) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server_PlayerUpdated>): Server_PlayerUpdated {
    const message = { ...baseServer_PlayerUpdated } as Server_PlayerUpdated;
    if (object.player !== undefined && object.player !== null) {
      message.player = Player.fromPartial(object.player);
    }
    return message;
  },
};

const baseServer_SelfUpdated: object = {};

export const Server_SelfUpdated = {
  encode(
    message: Server_SelfUpdated,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.player !== undefined) {
      OwnPlayer.encode(message.player, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_SelfUpdated {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_SelfUpdated } as Server_SelfUpdated;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.player = OwnPlayer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_SelfUpdated {
    const message = { ...baseServer_SelfUpdated } as Server_SelfUpdated;
    if (object.player !== undefined && object.player !== null) {
      message.player = OwnPlayer.fromJSON(object.player);
    }
    return message;
  },

  toJSON(message: Server_SelfUpdated): unknown {
    const obj: any = {};
    message.player !== undefined &&
      (obj.player = message.player
        ? OwnPlayer.toJSON(message.player)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server_SelfUpdated>): Server_SelfUpdated {
    const message = { ...baseServer_SelfUpdated } as Server_SelfUpdated;
    if (object.player !== undefined && object.player !== null) {
      message.player = OwnPlayer.fromPartial(object.player);
    }
    return message;
  },
};

const baseServer_GameUpdated: object = {};

export const Server_GameUpdated = {
  encode(
    message: Server_GameUpdated,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.game !== undefined) {
      Game.encode(message.game, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_GameUpdated {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_GameUpdated } as Server_GameUpdated;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.game = Game.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_GameUpdated {
    const message = { ...baseServer_GameUpdated } as Server_GameUpdated;
    if (object.game !== undefined && object.game !== null) {
      message.game = Game.fromJSON(object.game);
    }
    return message;
  },

  toJSON(message: Server_GameUpdated): unknown {
    const obj: any = {};
    message.game !== undefined &&
      (obj.game = message.game ? Game.toJSON(message.game) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server_GameUpdated>): Server_GameUpdated {
    const message = { ...baseServer_GameUpdated } as Server_GameUpdated;
    if (object.game !== undefined && object.game !== null) {
      message.game = Game.fromPartial(object.game);
    }
    return message;
  },
};

const baseServer_PlayerEntered: object = {};

export const Server_PlayerEntered = {
  encode(
    message: Server_PlayerEntered,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.player !== undefined) {
      Player.encode(message.player, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_PlayerEntered {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_PlayerEntered } as Server_PlayerEntered;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.player = Player.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_PlayerEntered {
    const message = { ...baseServer_PlayerEntered } as Server_PlayerEntered;
    if (object.player !== undefined && object.player !== null) {
      message.player = Player.fromJSON(object.player);
    }
    return message;
  },

  toJSON(message: Server_PlayerEntered): unknown {
    const obj: any = {};
    message.player !== undefined &&
      (obj.player = message.player ? Player.toJSON(message.player) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server_PlayerEntered>): Server_PlayerEntered {
    const message = { ...baseServer_PlayerEntered } as Server_PlayerEntered;
    if (object.player !== undefined && object.player !== null) {
      message.player = Player.fromPartial(object.player);
    }
    return message;
  },
};

const baseServer_PlayerLostConn: object = { playerId: "" };

export const Server_PlayerLostConn = {
  encode(
    message: Server_PlayerLostConn,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.playerId !== "") {
      writer.uint32(10).string(message.playerId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_PlayerLostConn {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_PlayerLostConn } as Server_PlayerLostConn;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.playerId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_PlayerLostConn {
    const message = { ...baseServer_PlayerLostConn } as Server_PlayerLostConn;
    if (object.playerId !== undefined && object.playerId !== null) {
      message.playerId = String(object.playerId);
    }
    return message;
  },

  toJSON(message: Server_PlayerLostConn): unknown {
    const obj: any = {};
    message.playerId !== undefined && (obj.playerId = message.playerId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<Server_PlayerLostConn>
  ): Server_PlayerLostConn {
    const message = { ...baseServer_PlayerLostConn } as Server_PlayerLostConn;
    if (object.playerId !== undefined && object.playerId !== null) {
      message.playerId = object.playerId;
    }
    return message;
  },
};

const baseServer_GameDeclined: object = {};

export const Server_GameDeclined = {
  encode(_: Server_GameDeclined, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_GameDeclined {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_GameDeclined } as Server_GameDeclined;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Server_GameDeclined {
    const message = { ...baseServer_GameDeclined } as Server_GameDeclined;
    return message;
  },

  toJSON(_: Server_GameDeclined): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Server_GameDeclined>): Server_GameDeclined {
    const message = { ...baseServer_GameDeclined } as Server_GameDeclined;
    return message;
  },
};

const baseServer_CardReceived: object = {};

export const Server_CardReceived = {
  encode(
    message: Server_CardReceived,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.card !== undefined) {
      Card.encode(message.card, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Server_CardReceived {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServer_CardReceived } as Server_CardReceived;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.card = Card.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Server_CardReceived {
    const message = { ...baseServer_CardReceived } as Server_CardReceived;
    if (object.card !== undefined && object.card !== null) {
      message.card = Card.fromJSON(object.card);
    }
    return message;
  },

  toJSON(message: Server_CardReceived): unknown {
    const obj: any = {};
    message.card !== undefined &&
      (obj.card = message.card ? Card.toJSON(message.card) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Server_CardReceived>): Server_CardReceived {
    const message = { ...baseServer_CardReceived } as Server_CardReceived;
    if (object.card !== undefined && object.card !== null) {
      message.card = Card.fromPartial(object.card);
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
