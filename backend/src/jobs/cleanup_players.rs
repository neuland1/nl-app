use crate::{model::Player, server::app_context::AppContext};
use chrono::{Duration, Utc};
use log::{debug, error, info};

/// Removes inactive players from a game if they are inactive for more than five minutes.
pub fn cleanup_players(ctx: &'static AppContext) -> impl Fn() {
    move || {
        tokio::spawn(async move {
            if let Err(err) = execute_cleanup_players(ctx, Duration::minutes(5)).await {
                error!("Cleanup of players has failed: {}", &err);
            }
        });
    }
}

// Player is without an active connection
fn is_inactive_player(duration: Duration) -> impl Fn(&Player) -> bool {
    let threshold = Utc::now().checked_sub_signed(duration).unwrap();

    move |player: &Player| {
        player.last_active_time().is_some() && player.last_active_time().unwrap().lt(&threshold)
    }
}

async fn execute_cleanup_players(ctx: &AppContext, duration: Duration) -> Result<bool, String> {
    let inactive_players = ctx
        .db()
        .players()
        .scan(Box::new(is_inactive_player(duration)))
        .await?;
    let inactive_count = inactive_players.len();

    // remove players from maybe existing game
    for id in inactive_players.clone() {
        if let Some(player) = ctx.db().players().get(&id).await? {
            if let Some(mut game) = ctx.db().games().get(player.game_token()).await? {
                game.remove_player(&id);
                ctx.db().games().persist(&game).await?;
            }
        }
    }

    // remove players
    if inactive_count > 0 {
        ctx.db().players().remove_batch(&inactive_players).await?;
        info!("Removed {} inactive players", inactive_count);
        Ok(true)
    } else {
        debug!("Removed no inactive players");
        Ok(false)
    }
}

#[cfg(test)]
mod tests {
    use super::execute_cleanup_players;
    use crate::{
        model::{Game, GameState, Player},
        server::app_context::AppContext,
    };
    use chrono::Duration;

    #[tokio::test]
    async fn should_cleanup_player() {
        let ctx = AppContext::init();
        let mut player = Player::new("GAME");
        player.set_inactive();
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player failed");
        let game = Game::new(player.id(), "GAME");
        ctx.db()
            .games()
            .persist(&game)
            .await
            .expect("Persisting game failed");

        let res = execute_cleanup_players(&ctx, Duration::nanoseconds(1)).await;
        assert!(res.is_ok());
        assert!(res.unwrap());

        assert!(ctx.db().players().get(player.id()).await.unwrap().is_none());
        assert!(ctx
            .db()
            .games()
            .get(player.game_token())
            .await
            .unwrap()
            .expect("Game should still exist")
            .admin_id()
            .is_none());
        assert_eq!(
            ctx.db()
                .games()
                .get(player.game_token())
                .await
                .unwrap()
                .expect("Game should still exist")
                .state(),
            &GameState::Abandoned
        );
    }

    #[tokio::test]
    async fn should_not_cleanup_player() {
        let ctx = AppContext::init();
        let player = Player::new("GAME");
        ctx.db()
            .players()
            .persist(&player)
            .await
            .expect("Persisting player failed");

        let res = execute_cleanup_players(&ctx, Duration::minutes(1)).await;
        assert!(res.is_ok());
        assert!(!res.unwrap());
        assert!(ctx.db().players().get(player.id()).await.unwrap().is_some());
    }
}
