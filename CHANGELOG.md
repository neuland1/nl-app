# Changelog

## Frontend

See [./frontend/CHANGELOG.md](./frontend/CHANGELOG.md)

## Backend

See [./backend/CHANGELOG.md](./backend/CHANGELOG.md)
