use super::proto::{self};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Action {
    Settings,
    StartRound,
    Call,
    Fold,
    Raise,
    Check,
    Disclose,
    EndRound,
    AllIn,
    IncreaseBlinds,
}

impl From<proto::action::Action> for Action {
    fn from(proto_action: proto::action::Action) -> Self {
        match proto_action.definition {
            Some(def) => match def {
                proto::action::Action_oneof_definition::settings(..) => Action::Settings,
                proto::action::Action_oneof_definition::call(..) => Action::Call,
                proto::action::Action_oneof_definition::fold(..) => Action::Fold,
                proto::action::Action_oneof_definition::raise(..) => Action::Raise,
                proto::action::Action_oneof_definition::check(..) => Action::Check,
                proto::action::Action_oneof_definition::start_round(..) => Action::StartRound,
                proto::action::Action_oneof_definition::disclose(..) => Action::Disclose,
                proto::action::Action_oneof_definition::end_round(..) => Action::EndRound,
                proto::action::Action_oneof_definition::all_in(..) => Action::AllIn,
                proto::action::Action_oneof_definition::increase_blinds(..) => {
                    Action::IncreaseBlinds
                }
            },
            None => Action::Settings,
        }
    }
}
impl Into<proto::action::Action> for Action {
    fn into(self) -> proto::action::Action {
        let mut action = proto::action::Action::new();
        match self {
            Action::Settings => action.set_settings(proto::action::Action_Settings::new()),
            Action::Call => action.set_call(proto::action::Action_Call::new()),
            Action::Fold => action.set_fold(proto::action::Action_Fold::new()),
            Action::Raise => action.set_raise(proto::action::Action_Raise::new()),
            Action::Check => action.set_check(proto::action::Action_Check::new()),
            Action::StartRound => action.set_start_round(proto::action::Action_StartRound::new()),
            Action::Disclose => action.set_disclose(proto::action::Action_Disclose::new()),
            Action::EndRound => action.set_end_round(proto::action::Action_EndRound::new()),
            Action::AllIn => action.set_all_in(proto::action::Action_AllIn::new()),
            Action::IncreaseBlinds => {
                action.set_increase_blinds(proto::action::Action_IncreaseBlinds::new())
            }
        }
        action
    }
}

#[cfg(test)]
mod tests {
    use super::Action;
    use crate::model::proto::{self};

    #[test]
    fn should_serialized_and_deserialize_settings() {
        let action = Action::Settings;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_call() {
        let action = Action::Call;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_fold() {
        let action = Action::Fold;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_raise() {
        let action = Action::Raise;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_check() {
        let action = Action::Check;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_start_round() {
        let action = Action::StartRound;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_disclose() {
        let action = Action::Disclose;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_end_round() {
        let action = Action::EndRound;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_all_in() {
        let action = Action::AllIn;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }

    #[test]
    fn should_serialized_and_deserialize_increase_blinds() {
        let action = Action::IncreaseBlinds;
        let serialized: proto::action::Action = action.clone().into();

        assert_eq!(Action::from(serialized), action);
    }
}
