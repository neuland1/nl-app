use super::move_to_next_player;
use crate::{
    model::{Game, Player},
    server::app_context::AppContext,
};

pub async fn handle_round_all_in(
    player: &mut Player,
    game: &mut Game,
    ctx: &AppContext,
) -> Result<(), String> {
    let round = game
        .round_mut()
        .ok_or_else(|| String::from("Round missing"))?;

    let remaining_credits = player.credits();

    round.all_in(player.id(), remaining_credits);

    player.substract_credits(remaining_credits);

    move_to_next_player(ctx, game, player).await?;

    Ok(())
}
