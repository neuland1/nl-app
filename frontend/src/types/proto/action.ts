/* eslint-disable */
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import * as Long from "long";

export const protobufPackage = "";

export interface Action {
  definition?:
    | { $case: "settings"; settings: Action_Settings }
    | { $case: "call"; call: Action_Call }
    | { $case: "fold"; fold: Action_Fold }
    | { $case: "raise"; raise: Action_Raise }
    | { $case: "startRound"; startRound: Action_StartRound }
    | { $case: "check"; check: Action_Check }
    | { $case: "disclose"; disclose: Action_Disclose }
    | { $case: "endRound"; endRound: Action_EndRound }
    | { $case: "allIn"; allIn: Action_AllIn }
    | { $case: "increaseBlinds"; increaseBlinds: Action_IncreaseBlinds };
}

export interface Action_Settings {}

export interface Action_Call {}

export interface Action_Fold {}

export interface Action_Raise {}

export interface Action_StartRound {}

export interface Action_Check {}

export interface Action_Disclose {}

export interface Action_EndRound {}

export interface Action_AllIn {}

export interface Action_IncreaseBlinds {}

const baseAction: object = {};

export const Action = {
  encode(message: Action, writer: Writer = Writer.create()): Writer {
    if (message.definition?.$case === "settings") {
      Action_Settings.encode(
        message.definition.settings,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "call") {
      Action_Call.encode(
        message.definition.call,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "fold") {
      Action_Fold.encode(
        message.definition.fold,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "raise") {
      Action_Raise.encode(
        message.definition.raise,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "startRound") {
      Action_StartRound.encode(
        message.definition.startRound,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "check") {
      Action_Check.encode(
        message.definition.check,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "disclose") {
      Action_Disclose.encode(
        message.definition.disclose,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "endRound") {
      Action_EndRound.encode(
        message.definition.endRound,
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "allIn") {
      Action_AllIn.encode(
        message.definition.allIn,
        writer.uint32(74).fork()
      ).ldelim();
    }
    if (message.definition?.$case === "increaseBlinds") {
      Action_IncreaseBlinds.encode(
        message.definition.increaseBlinds,
        writer.uint32(82).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction } as Action;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.definition = {
            $case: "settings",
            settings: Action_Settings.decode(reader, reader.uint32()),
          };
          break;
        case 2:
          message.definition = {
            $case: "call",
            call: Action_Call.decode(reader, reader.uint32()),
          };
          break;
        case 3:
          message.definition = {
            $case: "fold",
            fold: Action_Fold.decode(reader, reader.uint32()),
          };
          break;
        case 4:
          message.definition = {
            $case: "raise",
            raise: Action_Raise.decode(reader, reader.uint32()),
          };
          break;
        case 5:
          message.definition = {
            $case: "startRound",
            startRound: Action_StartRound.decode(reader, reader.uint32()),
          };
          break;
        case 6:
          message.definition = {
            $case: "check",
            check: Action_Check.decode(reader, reader.uint32()),
          };
          break;
        case 7:
          message.definition = {
            $case: "disclose",
            disclose: Action_Disclose.decode(reader, reader.uint32()),
          };
          break;
        case 8:
          message.definition = {
            $case: "endRound",
            endRound: Action_EndRound.decode(reader, reader.uint32()),
          };
          break;
        case 9:
          message.definition = {
            $case: "allIn",
            allIn: Action_AllIn.decode(reader, reader.uint32()),
          };
          break;
        case 10:
          message.definition = {
            $case: "increaseBlinds",
            increaseBlinds: Action_IncreaseBlinds.decode(
              reader,
              reader.uint32()
            ),
          };
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Action {
    const message = { ...baseAction } as Action;
    if (object.settings !== undefined && object.settings !== null) {
      message.definition = {
        $case: "settings",
        settings: Action_Settings.fromJSON(object.settings),
      };
    }
    if (object.call !== undefined && object.call !== null) {
      message.definition = {
        $case: "call",
        call: Action_Call.fromJSON(object.call),
      };
    }
    if (object.fold !== undefined && object.fold !== null) {
      message.definition = {
        $case: "fold",
        fold: Action_Fold.fromJSON(object.fold),
      };
    }
    if (object.raise !== undefined && object.raise !== null) {
      message.definition = {
        $case: "raise",
        raise: Action_Raise.fromJSON(object.raise),
      };
    }
    if (object.startRound !== undefined && object.startRound !== null) {
      message.definition = {
        $case: "startRound",
        startRound: Action_StartRound.fromJSON(object.startRound),
      };
    }
    if (object.check !== undefined && object.check !== null) {
      message.definition = {
        $case: "check",
        check: Action_Check.fromJSON(object.check),
      };
    }
    if (object.disclose !== undefined && object.disclose !== null) {
      message.definition = {
        $case: "disclose",
        disclose: Action_Disclose.fromJSON(object.disclose),
      };
    }
    if (object.endRound !== undefined && object.endRound !== null) {
      message.definition = {
        $case: "endRound",
        endRound: Action_EndRound.fromJSON(object.endRound),
      };
    }
    if (object.allIn !== undefined && object.allIn !== null) {
      message.definition = {
        $case: "allIn",
        allIn: Action_AllIn.fromJSON(object.allIn),
      };
    }
    if (object.increaseBlinds !== undefined && object.increaseBlinds !== null) {
      message.definition = {
        $case: "increaseBlinds",
        increaseBlinds: Action_IncreaseBlinds.fromJSON(object.increaseBlinds),
      };
    }
    return message;
  },

  toJSON(message: Action): unknown {
    const obj: any = {};
    message.definition?.$case === "settings" &&
      (obj.settings = message.definition?.settings
        ? Action_Settings.toJSON(message.definition?.settings)
        : undefined);
    message.definition?.$case === "call" &&
      (obj.call = message.definition?.call
        ? Action_Call.toJSON(message.definition?.call)
        : undefined);
    message.definition?.$case === "fold" &&
      (obj.fold = message.definition?.fold
        ? Action_Fold.toJSON(message.definition?.fold)
        : undefined);
    message.definition?.$case === "raise" &&
      (obj.raise = message.definition?.raise
        ? Action_Raise.toJSON(message.definition?.raise)
        : undefined);
    message.definition?.$case === "startRound" &&
      (obj.startRound = message.definition?.startRound
        ? Action_StartRound.toJSON(message.definition?.startRound)
        : undefined);
    message.definition?.$case === "check" &&
      (obj.check = message.definition?.check
        ? Action_Check.toJSON(message.definition?.check)
        : undefined);
    message.definition?.$case === "disclose" &&
      (obj.disclose = message.definition?.disclose
        ? Action_Disclose.toJSON(message.definition?.disclose)
        : undefined);
    message.definition?.$case === "endRound" &&
      (obj.endRound = message.definition?.endRound
        ? Action_EndRound.toJSON(message.definition?.endRound)
        : undefined);
    message.definition?.$case === "allIn" &&
      (obj.allIn = message.definition?.allIn
        ? Action_AllIn.toJSON(message.definition?.allIn)
        : undefined);
    message.definition?.$case === "increaseBlinds" &&
      (obj.increaseBlinds = message.definition?.increaseBlinds
        ? Action_IncreaseBlinds.toJSON(message.definition?.increaseBlinds)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<Action>): Action {
    const message = { ...baseAction } as Action;
    if (
      object.definition?.$case === "settings" &&
      object.definition?.settings !== undefined &&
      object.definition?.settings !== null
    ) {
      message.definition = {
        $case: "settings",
        settings: Action_Settings.fromPartial(object.definition.settings),
      };
    }
    if (
      object.definition?.$case === "call" &&
      object.definition?.call !== undefined &&
      object.definition?.call !== null
    ) {
      message.definition = {
        $case: "call",
        call: Action_Call.fromPartial(object.definition.call),
      };
    }
    if (
      object.definition?.$case === "fold" &&
      object.definition?.fold !== undefined &&
      object.definition?.fold !== null
    ) {
      message.definition = {
        $case: "fold",
        fold: Action_Fold.fromPartial(object.definition.fold),
      };
    }
    if (
      object.definition?.$case === "raise" &&
      object.definition?.raise !== undefined &&
      object.definition?.raise !== null
    ) {
      message.definition = {
        $case: "raise",
        raise: Action_Raise.fromPartial(object.definition.raise),
      };
    }
    if (
      object.definition?.$case === "startRound" &&
      object.definition?.startRound !== undefined &&
      object.definition?.startRound !== null
    ) {
      message.definition = {
        $case: "startRound",
        startRound: Action_StartRound.fromPartial(object.definition.startRound),
      };
    }
    if (
      object.definition?.$case === "check" &&
      object.definition?.check !== undefined &&
      object.definition?.check !== null
    ) {
      message.definition = {
        $case: "check",
        check: Action_Check.fromPartial(object.definition.check),
      };
    }
    if (
      object.definition?.$case === "disclose" &&
      object.definition?.disclose !== undefined &&
      object.definition?.disclose !== null
    ) {
      message.definition = {
        $case: "disclose",
        disclose: Action_Disclose.fromPartial(object.definition.disclose),
      };
    }
    if (
      object.definition?.$case === "endRound" &&
      object.definition?.endRound !== undefined &&
      object.definition?.endRound !== null
    ) {
      message.definition = {
        $case: "endRound",
        endRound: Action_EndRound.fromPartial(object.definition.endRound),
      };
    }
    if (
      object.definition?.$case === "allIn" &&
      object.definition?.allIn !== undefined &&
      object.definition?.allIn !== null
    ) {
      message.definition = {
        $case: "allIn",
        allIn: Action_AllIn.fromPartial(object.definition.allIn),
      };
    }
    if (
      object.definition?.$case === "increaseBlinds" &&
      object.definition?.increaseBlinds !== undefined &&
      object.definition?.increaseBlinds !== null
    ) {
      message.definition = {
        $case: "increaseBlinds",
        increaseBlinds: Action_IncreaseBlinds.fromPartial(
          object.definition.increaseBlinds
        ),
      };
    }
    return message;
  },
};

const baseAction_Settings: object = {};

export const Action_Settings = {
  encode(_: Action_Settings, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Settings {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Settings } as Action_Settings;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Settings {
    const message = { ...baseAction_Settings } as Action_Settings;
    return message;
  },

  toJSON(_: Action_Settings): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Settings>): Action_Settings {
    const message = { ...baseAction_Settings } as Action_Settings;
    return message;
  },
};

const baseAction_Call: object = {};

export const Action_Call = {
  encode(_: Action_Call, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Call {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Call } as Action_Call;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Call {
    const message = { ...baseAction_Call } as Action_Call;
    return message;
  },

  toJSON(_: Action_Call): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Call>): Action_Call {
    const message = { ...baseAction_Call } as Action_Call;
    return message;
  },
};

const baseAction_Fold: object = {};

export const Action_Fold = {
  encode(_: Action_Fold, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Fold {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Fold } as Action_Fold;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Fold {
    const message = { ...baseAction_Fold } as Action_Fold;
    return message;
  },

  toJSON(_: Action_Fold): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Fold>): Action_Fold {
    const message = { ...baseAction_Fold } as Action_Fold;
    return message;
  },
};

const baseAction_Raise: object = {};

export const Action_Raise = {
  encode(_: Action_Raise, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Raise {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Raise } as Action_Raise;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Raise {
    const message = { ...baseAction_Raise } as Action_Raise;
    return message;
  },

  toJSON(_: Action_Raise): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Raise>): Action_Raise {
    const message = { ...baseAction_Raise } as Action_Raise;
    return message;
  },
};

const baseAction_StartRound: object = {};

export const Action_StartRound = {
  encode(_: Action_StartRound, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_StartRound {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_StartRound } as Action_StartRound;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_StartRound {
    const message = { ...baseAction_StartRound } as Action_StartRound;
    return message;
  },

  toJSON(_: Action_StartRound): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_StartRound>): Action_StartRound {
    const message = { ...baseAction_StartRound } as Action_StartRound;
    return message;
  },
};

const baseAction_Check: object = {};

export const Action_Check = {
  encode(_: Action_Check, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Check {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Check } as Action_Check;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Check {
    const message = { ...baseAction_Check } as Action_Check;
    return message;
  },

  toJSON(_: Action_Check): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Check>): Action_Check {
    const message = { ...baseAction_Check } as Action_Check;
    return message;
  },
};

const baseAction_Disclose: object = {};

export const Action_Disclose = {
  encode(_: Action_Disclose, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_Disclose {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_Disclose } as Action_Disclose;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_Disclose {
    const message = { ...baseAction_Disclose } as Action_Disclose;
    return message;
  },

  toJSON(_: Action_Disclose): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_Disclose>): Action_Disclose {
    const message = { ...baseAction_Disclose } as Action_Disclose;
    return message;
  },
};

const baseAction_EndRound: object = {};

export const Action_EndRound = {
  encode(_: Action_EndRound, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_EndRound {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_EndRound } as Action_EndRound;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_EndRound {
    const message = { ...baseAction_EndRound } as Action_EndRound;
    return message;
  },

  toJSON(_: Action_EndRound): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_EndRound>): Action_EndRound {
    const message = { ...baseAction_EndRound } as Action_EndRound;
    return message;
  },
};

const baseAction_AllIn: object = {};

export const Action_AllIn = {
  encode(_: Action_AllIn, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_AllIn {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_AllIn } as Action_AllIn;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_AllIn {
    const message = { ...baseAction_AllIn } as Action_AllIn;
    return message;
  },

  toJSON(_: Action_AllIn): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_AllIn>): Action_AllIn {
    const message = { ...baseAction_AllIn } as Action_AllIn;
    return message;
  },
};

const baseAction_IncreaseBlinds: object = {};

export const Action_IncreaseBlinds = {
  encode(_: Action_IncreaseBlinds, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Action_IncreaseBlinds {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAction_IncreaseBlinds } as Action_IncreaseBlinds;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): Action_IncreaseBlinds {
    const message = { ...baseAction_IncreaseBlinds } as Action_IncreaseBlinds;
    return message;
  },

  toJSON(_: Action_IncreaseBlinds): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<Action_IncreaseBlinds>): Action_IncreaseBlinds {
    const message = { ...baseAction_IncreaseBlinds } as Action_IncreaseBlinds;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}
