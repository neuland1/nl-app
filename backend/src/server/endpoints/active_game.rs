use super::client_messages::{
    handle_auth_confirmation, handle_game_leave, handle_game_start, handle_increase_blinds,
    handle_name_update, handle_round_all_in, handle_round_call, handle_round_checked,
    handle_round_disclosed, handle_round_ended, handle_round_fold, handle_round_raised,
    handle_round_started,
};
use crate::{
    model::{
        proto::{self},
        Action, Game, Player,
    },
    server::app_context::AppContext,
};
use futures::{stream::SplitSink, StreamExt};
use log::{debug, error, info};
use nanoid::nanoid;
use protobuf::Message;
use warp::{
    ws::{Message as WsMessage, WebSocket},
    Error,
};

pub fn handle_ws_filter(ws: warp::ws::Ws, ctx: &'static AppContext) -> impl warp::Reply {
    ws.on_upgrade(move |socket| async move {
        on_upgrade(socket, ctx).await;
    })
}

async fn on_upgrade(socket: WebSocket, ctx: &AppContext) {
    let (sender, mut receiver) = socket.split();

    match prepare_new_connection(ctx, sender).await {
        Ok(peer_id) => {
            // incoming message loop
            while let Some(msg) = receiver.next().await {
                if let Err(err) = process_incoming_message(msg, &peer_id, ctx).await {
                    error!("Processing message failed: {}", &err);
                }
            }

            if let Err(err) = inform_about_leave(&peer_id, ctx).await {
                error!(
                    "Informing other players about left player with peer id {} failed: {}",
                    &peer_id, &err
                );
            }
        }
        Err(err) => {
            error!("Preparing new connection has failed: {}", &err);
        }
    }
}

async fn inform_about_leave(peer_id: &str, ctx: &AppContext) -> Result<(), String> {
    // inform other players about left player
    if let Some(player_id) = ctx.ws().get_authenticated_player_for_peer(peer_id).await {
        if let Some(mut player) = ctx.db().players().get(&player_id).await? {
            player.set_inactive();
            ctx.db().players().persist(&player).await?;

            let game = ctx.db().games().get(player.game_token()).await.unwrap();
            ctx.ws().remove_connection(peer_id, &game).await?;

            info!("Player {} has closed its connection", &player_id);
        }
    } else {
        ctx.ws().remove_connection(peer_id, &None).await?;
    }

    Ok(())
}

async fn prepare_new_connection(
    ctx: &AppContext,
    sender: SplitSink<WebSocket, WsMessage>,
) -> Result<String, String> {
    let peer_id = nanoid!();

    ctx.ws().add_connection(&peer_id, sender).await?;

    info!("Registered new player {}", &peer_id);

    Ok(peer_id)
}

async fn process_incoming_message(
    msg: Result<WsMessage, Error>,
    peer_id: &str,
    ctx: &AppContext,
) -> Result<(), String> {
    match msg {
        Ok(msg) => {
            if msg.is_close() {
                debug!("Connection to {} closed", &peer_id);
                return Ok(());
            } else if msg.is_binary() {
                return match proto::message::Client::parse_from_bytes(&msg.into_bytes()) {
                    Ok(res) => match res.message {
                        Some(x) => handle_incoming_message(x, ctx, &peer_id).await,
                        None => Err(String::from("Message was empty")),
                    },
                    Err(err) => Err(format!("Reading incoming message failed: {err}", err = err)),
                };
            } else if msg.is_text() {
                return Err(String::from(
                    "Textual protobuf message ignored, only binary format supported",
                ));
            } else if msg.is_ping() || msg.is_pong() {
                return Ok(());
            }

            Err(String::from("Received unexpected message type"))
        }
        Err(err) => Err(format!("Receiving message has failed: {err}", err = err,)),
    }
}

async fn handle_incoming_message(
    message: proto::message::Client_oneof_message,
    ctx: &AppContext,
    peer_id: &str,
) -> Result<(), String> {
    match message {
        proto::message::Client_oneof_message::auth_confirmed(..) => {
            info!("Received message from {}: authConfirmation", peer_id);
        }
        _ => {
            info!("Received message from {}: {:?}", peer_id, message);
        }
    }

    let player = get_corresponding_player(peer_id, ctx).await;
    match message {
        proto::message::Client_oneof_message::auth_confirmed(
            proto::message::Client_AuthConfirmed { token, .. },
        ) => handle_auth_confirmation(&token, peer_id, ctx).await,
        proto::message::Client_oneof_message::name_updated(ev) => {
            handle_name_update(&ev.name, player, ctx).await
        }
        proto::message::Client_oneof_message::game_started(_) => {
            handle_game_start(player, ctx).await
        }
        proto::message::Client_oneof_message::game_left(_) => handle_game_leave(player, ctx).await,
        proto::message::Client_oneof_message::round_called(_) => {
            let (mut game, mut player) = check_assigned_action(&player, Action::Call, ctx).await?;
            handle_round_call(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_fold(_) => {
            let (mut game, mut player) = check_assigned_action(&player, Action::Fold, ctx).await?;
            handle_round_fold(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_raised(
            proto::message::Client_RoundRaised { amount, .. },
        ) => {
            let (mut game, mut player) = check_assigned_action(&player, Action::Raise, ctx).await?;
            handle_round_raised(amount, &mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_checked(_) => {
            let (mut game, mut player) = check_assigned_action(&player, Action::Check, ctx).await?;
            handle_round_checked(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_started(_) => {
            let (mut game, mut player) =
                check_assigned_action(&player, Action::StartRound, ctx).await?;
            handle_round_started(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_disclosed(_) => {
            let (mut game, mut player) =
                check_assigned_action(&player, Action::Disclose, ctx).await?;
            handle_round_disclosed(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_ended(_) => {
            let (mut game, mut player) =
                check_assigned_action(&player, Action::EndRound, ctx).await?;
            handle_round_ended(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::round_all_in(_) => {
            let (mut game, mut player) = check_assigned_action(&player, Action::AllIn, ctx).await?;
            handle_round_all_in(&mut player, &mut game, ctx).await
        }
        proto::message::Client_oneof_message::increased_blinds(
            proto::message::Client_IncreasedBlinds { small_blind, .. },
        ) => {
            let (mut game, mut player) =
                check_assigned_action(&player, Action::IncreaseBlinds, ctx).await?;
            handle_increase_blinds(small_blind, &mut player, &mut game, ctx).await
        }
    }
}

async fn check_assigned_action(
    player: &Option<Player>,
    action: Action,
    ctx: &AppContext,
) -> Result<(Game, Player), String> {
    let player = player
        .clone()
        .ok_or_else(|| String::from("Player is not authenticated"))?;

    if player.actions().iter().find(|t| *t == &action).is_none() {
        return Err(String::from("Player doesn't have action"));
    }

    ctx.db()
        .games()
        .get(player.game_token())
        .await?
        .ok_or_else(|| String::from("Game not found"))
        .map(|game| (game, player))
}

async fn get_corresponding_player(peer_id: &str, ctx: &AppContext) -> Option<Player> {
    let player_id = ctx.ws().get_authenticated_player_for_peer(peer_id).await?;

    return ctx
        .db()
        .players()
        .get(&player_id)
        .await
        .ok()
        .and_then(|p| p);
}

#[cfg(test)]
mod tests {
    use super::{check_assigned_action, process_incoming_message};
    use crate::{
        model::{
            proto::{self},
            Action, Game, Player,
        },
        server::app_context::AppContext,
    };
    use itertools::Itertools;
    use protobuf::Message;
    use warp::ws::Message as WsMessage;

    #[tokio::test]
    async fn should_process_incoming_message() {
        let ctx = AppContext::init();
        let mut msg = proto::message::Client::new();
        msg.set_game_started(proto::message::Client_GameStarted::new());
        let binary_msg = WsMessage::binary(msg.write_to_bytes().unwrap());

        let result = process_incoming_message(Ok(binary_msg), "peer", &ctx).await;
        assert!(result.is_ok());
    }

    #[tokio::test]
    async fn should_process_incoming_close() {
        let ctx = AppContext::init();
        let binary_msg = WsMessage::close();

        let result = process_incoming_message(Ok(binary_msg), "peer", &ctx).await;
        assert!(result.is_ok());
    }

    #[tokio::test]
    async fn should_process_incoming_message_with_only_first_five_bytes() {
        let ctx = AppContext::init();
        let mut msg = proto::message::Client::new();
        msg.set_game_started(proto::message::Client_GameStarted::new());
        let invalid_msg = WsMessage::binary(
            msg.write_to_bytes()
                .unwrap()
                .iter()
                .skip(1)
                .cloned()
                .collect_vec(),
        );

        let result = process_incoming_message(Ok(invalid_msg), "peer", &ctx).await;
        assert!(result.is_err());
    }

    #[tokio::test]
    async fn should_process_incoming_message_without_content() {
        let ctx = AppContext::init();
        let msg = proto::message::Client::new();
        let binary_msg = WsMessage::binary(msg.write_to_bytes().unwrap());

        let result = process_incoming_message(Ok(binary_msg), "peer", &ctx).await;
        assert!(result.is_err());
    }

    #[tokio::test]
    async fn should_process_incoming_message_with_text_content() {
        let ctx = AppContext::init();
        let binary_msg = WsMessage::text("Abc");

        let result = process_incoming_message(Ok(binary_msg), "peer", &ctx).await;
        assert!(result.is_err());
    }

    #[tokio::test]
    async fn should_process_incoming_message_with_ping() {
        let ctx = AppContext::init();
        let binary_msg = WsMessage::ping(vec![]);

        let result = process_incoming_message(Ok(binary_msg), "peer", &ctx).await;
        assert!(result.is_ok());
    }

    #[tokio::test]
    async fn should_check_assigned_action() {
        let ctx = AppContext::init();

        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Check);
        ctx.db().players().persist(&player).await.unwrap();

        let game = Game::new(player.id(), "TOKEN");
        ctx.db().games().persist(&game).await.unwrap();

        let result = check_assigned_action(&Some(player), Action::Check, &ctx).await;
        assert!(result.is_ok());

        let (game, player) = result.unwrap();
        assert_eq!(game.token(), "TOKEN");
        assert_eq!(player.id(), player.id());
    }

    #[tokio::test]
    async fn should_block_missing_action() {
        let ctx = AppContext::init();

        let mut player = Player::new("TOKEN");
        player.assign_action(Action::Raise);
        ctx.db().players().persist(&player).await.unwrap();

        let game = Game::new(player.id(), "TOKEN");
        ctx.db().games().persist(&game).await.unwrap();

        let result = check_assigned_action(&Some(player), Action::Check, &ctx).await;
        assert!(result.is_err());
    }
}
