import jwtDecode from "jwt-decode";

const ACCESS_TOKEN = "ACCESS_TOKEN";

export const getToken = (): string | null =>
  window.localStorage.getItem(ACCESS_TOKEN) || null;

export const saveToken = (token: string): void =>
  window.localStorage.setItem(ACCESS_TOKEN, token);

export const clearToken = (): void =>
  window.localStorage.removeItem(ACCESS_TOKEN);

export type Claims = { sub: string; name: string; game: string };

// TODO move to store!
export const getClaims = (): Claims | null => {
  const token = getToken();
  if (!token) {
    return null;
  }

  return jwtDecode<Claims>(token);
};
